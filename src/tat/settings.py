# Copyright (C) 2016 Andres Angelini
#
# This file is part of The Alchemist's Tower.
#
# The Achemist's Tower is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Alchemist's Tower is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Website.  If not, see <http://www.gnu.org/licenses/>.


"""
Django settings for tat (aka "thealchemisttower") project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os, secret_settings
# This was missing in the original file. It's needed for TEMPLATE_DIRS.
from os.path import join
# Make the folder where this file lives the base URL to use relative paths.
settings_dir = os.path.dirname(__file__)
BASE_DIR = os.path.abspath(os.path.dirname(settings_dir))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = secret_settings.SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
# When turned off, Django will refuse to serve static files as a security
# measure. They must be served by an actual web server instead.
DEBUG = False

TEMPLATE_DEBUG = False

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
            join(BASE_DIR, 'tpl/')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# These must be added when DEBUG is False.
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '159.203.28.148', 'thealchemisttower.com', 'www.thealchemisttower.com']

# Only use this settings for development. Run this command in a terminal:
# python -m smtpd -n -c DebuggingServer localhost:1025
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# E-mail settings for production.
#EMAIL_HOST = secret_settings.EMAIL_HOST
#EMAIL_HOST_USER = secret_settings.EMAIL_HOST_USER
#EMAIL_HOST_PASSWORD = secret_settings.EMAIL_HOST_PASSWORD
#EMAIL_PORT = secret_settings.EMAIL_PORT
#EMAIL_USE_TLS = secret_settings.EMAIL_USE_TLS

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'itm',
    'paypal.standard.ipn',
)

PAYPAL_RECEIVER_EMAIL = secret_settings.PAYPAL_RECEIVER_EMAIL
PAYPAL_TEST = True
PAYPAL_PRIVATE_CERT = os.path.join(BASE_DIR, "crt/paypal.pem")
PAYPAL_PUBLIC_CERT = os.path.join(BASE_DIR, "crt/pubpaypal.pem")
PAYPAL_CERT = os.path.join(BASE_DIR, "crt/paypal_cert.pem")
PAYPAL_CERT_ID = secret_settings.PAYPAL_CERT_ID

# For development with DEBUG = True.
#PAYPAL_NOTIFY_URL = 'http://localhost:8000/ipn/'
#PAYPAL_RETURN_URL = 'http://localhost:8000/return/'
#PAYPAL_CANCEL_URL = 'http://localhost:8000/cancel/'

# For development with DEBUG = False.
PAYPAL_NOTIFY_URL = 'http://localhost/ipn/'
PAYPAL_RETURN_URL = 'http://localhost/return/'
PAYPAL_CANCEL_URL = 'http://localhost/cancel/'

# For production.
#PAYPAL_NOTIFY_URL = 'http://thealchemisttower.com/ipn/'
#PAYPAL_RETURN_URL = 'http://thealchemisttower.com/return/'
#PAYPAL_CANCEL_URL = 'http://thealchemisttower.com/cancel/'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# User must be logged out when they close the browser.
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

ROOT_URLCONF = 'tat.urls'

WSGI_APPLICATION = 'tat.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'                         # How it will appear in the URL.
STATIC_ROOT = os.path.join(BASE_DIR, "static")  # For production.
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "../../static"),              # For development.
)
# Leave django and django.db.backends loggers commented out and null handler
# only when working on localhost.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
#        'null': {
#            'level':'DEBUG',
#            'class':'django.utils.log.NullHandler',
#        },
        'logfile': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': BASE_DIR + "/logfile",
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
#        'django': {
#            'handlers':['console'],
#            'propagate': True,
#            'level':'WARN',
#        },
#        'django.db.backends': {
#            'handlers': ['console'],
#            'level': 'DEBUG',
#            'propagate': False,
#        },
        'ipn': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
        'tat': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
        'itm': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
    }
}
