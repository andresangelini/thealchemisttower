# Copyright (C) 2016 Andres Angelini
#
# This file is part of The Alchemist's Tower.
#
# The Achemist's Tower is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Alchemist's Tower is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Website.  If not, see <http://www.gnu.org/licenses/>.

from django import forms

# We'll use a django's form without rendering it onto the template just to take
# advantage of its validation method.
class ContactForm(forms.Form):
    subject = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField()

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()

class RecoveryForm(forms.Form):
    email = forms.EmailField()

class ConfirmationForm(forms.Form):
    recovery_code = forms.CharField()

class ResetPasswordForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()
    re_password = forms.CharField()

class ResetEmailForm(forms.Form):
    email = forms.EmailField()

class RegistrationForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField()
    re_password = forms.CharField()
