# Copyright (C) 2016 Andres Angelini
#
# This file is part of The Alchemist's Tower.
#
# The Achemist's Tower is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Alchemist's Tower is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Website.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import ensure_csrf_cookie
from tat.forms import ContactForm, LoginForm, RecoveryForm, ConfirmationForm
from tat.forms import ResetPasswordForm, ResetEmailForm, RegistrationForm
from django.core.mail import send_mail
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from itm.models import Item, Library
from paypal.standard.forms import PayPalEncryptedPaymentsForm
import string, random
import settings
import logging

log = logging.getLogger(__name__)

try:
    import json
except ImportError:
    import simplejson

@ensure_csrf_cookie
def run_site(request):
    
    # Function for generating random code.
    def generate_code(length=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for i in range(length))
    # ON EACH AJAX CALL:
    if request.is_ajax():
        log.info('Ajax call: ' +  request.POST.get('id', ''))
        # Respond according to the identity of the request.
        # MESSAGE
        if request.POST.get('id', '') == 'message':
            # Never trust user's input! Use Django's form to validate them.
            contact_form = ContactForm({
                'subject': request.POST.get('subject', ''),
                'email': request.POST.get('email', ''),
                'message': request.POST.get('message', '')
            })
            # Although validation is done client-side in javascript, we still
            # have to validate from the server side of things.
            if contact_form.is_valid():
                # Clean the form so its input values become more readable.
                cleaned_contact_form = contact_form.cleaned_data
                send_mail(
                    cleaned_contact_form['subject'],
                    cleaned_contact_form['message'],
                    cleaned_contact_form.get('email', 'noreply@example.com'),
                    ['siteowner@example.com'],
                )
            return HttpResponse(json.dumps({'errors' :
                                            contact_form.errors},
                                ensure_ascii=False),
                                content_type='application/javascript')
        # LOGIN
        if request.POST.get('id', '') == 'login':
            # Never trust user's input! Use Django's form to validate them.
            login_form = LoginForm({
                'username': request.POST.get('username', ''),
                'password': request.POST.get('password', '')
            })
            if login_form.is_valid():
                # Clean the form so its input values become more readable.
                cleaned_login_form = login_form.cleaned_data
                username = cleaned_login_form['username']
                password = cleaned_login_form['password']
                user = auth.authenticate(username=username, password=password)
                if user is not None:
                    # The password verified for the user: the user is valid,
                    # active and authenticated.
                    if user.is_active:
                        log.info('Logged in: ' + \
                            str(request.user.is_authenticated()))
                        auth.login(request, user)
                        log.info('Logged in: ' + \
                            str(request.user.is_authenticated()))
                        user_library = Library.objects.get(owner=user.username)
                        # Serialize user's library in order to send it through
                        # a json file to the client.
                        serialized_user_library = {}
                        for item in user_library.items.all():
                            serialized_item = {
                                'name': item.name,
                                'type': item.type,
                                'fileUrl': item.file_url,
                                'license': item.license,
                                'price': item.price
                            }
                            serialized_user_library[item.name.lower()] =\
                                serialized_item
                        if request.user.is_authenticated():
                            return HttpResponse(json.dumps(
                                {'userStatus': 'active', 'email': user.email,
                                'userLibrary': serialized_user_library},
                                ensure_ascii=False),
                                content_type='application/javascript')
                        else:
                            log.error('Unable to log in even though ' + \
                                'user is valid, active and authenticated. ' + \
                                'is the CSRF token being updated properly?')
                            return HttpResponse(json.dumps(
                                {'userStatus': 'error'},
                                ensure_ascii=False),
                                content_type='application/javascript')
                    else:
                    # The password is valid, but the account has been disabled!
                        return HttpResponse(json.dumps(
                                    {'userStatus': 'disabled'},
                                    ensure_ascii=False),
                                    content_type='application/javascript')
                else:
                    # The authentication system was unable to verify the
                    # username and password.
                    return HttpResponse(json.dumps({'userStatus': 'invalid'},
                                    ensure_ascii=False),
                                    content_type='application/javascript')
            else:
                # User's input is invalid.
                return HttpResponse(json.dumps({'userStatus': 'invalid'},
                                    ensure_ascii=False),
                                    content_type='application/javascript')
        # LOGOUT
        if request.POST.get('id', '') == 'logout':
            if request.user.is_authenticated() == True:
                log.info('Logged in: ' + str(request.user.is_authenticated()))
                auth.logout(request)
                log.info('Logged in: ' + str(request.user.is_authenticated()))
                if request.user.is_authenticated() == False:
                    # User has logged out successfully.
                    return HttpResponse(json.dumps({'userStatus': 'logged out'},
                        ensure_ascii=False),
                        content_type='application/javascript')
                else:
                    log.error('Unable to log out. Is the CSRF token ' + \
                        'being updated properly?')
                    return HttpResponse(json.dumps({'userStatus': 'error'},
                        ensure_ascii=False),
                        content_type='application/javascript')
            else:
                # User is already logged out.
                log.info('Logged in: ' + str(request.user.is_authenticated()))
                log.info('User is already logged out!')
                log.info('Logged in: ' + str(request.user.is_authenticated()))
                return HttpResponse(json.dumps({'userStatus':
                    'already logged out'},
                    ensure_ascii=False),
                    content_type='application/javascript')
        # ACCOUNT RECOVERY REQUEST
        if request.POST.get('id', '') == 'accountRecoveryRequest':
            # Never trust user's input! Use Django's form to validate them.
            recovery_form = RecoveryForm({'email': request.POST.get('email')})
            if recovery_form.is_valid():
                # Clean the form so its input values become more readable.
                cleaned_recovery_form = recovery_form.cleaned_data
                email = cleaned_recovery_form['email']
                # Check if a user associated to the stated e-mail address actually
                # exists. If it does, then send the e-mail. Otherwise just reply
                # with a warning.
                try:
                    User.objects.get(email=email)
                    username = User.objects.get(email=email).username
                    request.session['recovery_code'] = generate_code()
                    # Notify the user by e-mail about the request together with
                    # the recovery code.
                    send_mail(
                        "The Alchemist's Tower: Your Account Recovery",
                        "Dear %s," % username + "\n\n" +
                        "We have received a request to recover your account " +
                        "at The Alchemist's Tower." + "\n\n" +
                        "To proceed with the process, please enter the " +
                        "following Recovery Code into the recovery window " +
                        "you have left open at our website: " +
                        request.session['recovery_code'] + "\n\n" +
                        "If you did not make this request, you don't need to " +
                        "worry; your information will not change. Just " +
                        "ignore this message." + "\n\n" +
                        "This notification was sent to this e-mail address " +
                        "because it is associated with your account at The " +
                        "Alchemist Tower. For more information please read " +
                        "our Privacy Policy located in the F.A.Q. section: " +
                        "http://www.thealchemisttower.com" + "\n\n\n"
                        "Sincerely," + "\n"
                        "Your friends at The Alchemist's Tower" + "\n"
                        "http://www.thealchemisttower.com",
                        cleaned_recovery_form.get('siteowner@example.com',
                        'noreply@example.com'),
                        [email],
                    )
                    return HttpResponse(json.dumps({'requestStatus':
                        'valid'},
                        ensure_ascii=False),
                        content_type='application/javascript')
                except ObjectDoesNotExist:
                    return HttpResponse(json.dumps({'requestStatus':
                            'invalid'},
                            ensure_ascii=False),
                            content_type='application/javascript')
            else:
                return HttpResponse(json.dumps({'requestStatus':
                    'invalid'},
                    ensure_ascii=False),
                    content_type='application/javascript')
        # ACCOUNT RECOVERY CONFIRMATION
        if request.POST.get('id', '') == 'accountRecoveryConfirmation':
            # Never trust user's input! Use Django's form to validate them.
            confirmation_form = ConfirmationForm({
                'recovery_code': request.POST.get('recoveryCode', '') 
            })
            if confirmation_form.is_valid():
                recovery_code = request.session['recovery_code']
                # Clean the form so its input values become more readable.
                cleaned_confirmation_form = confirmation_form.cleaned_data
                if cleaned_confirmation_form['recovery_code'] == recovery_code:
                    log.info('Account ownership confirmed!')
                    # Delete recovery code as it shouldn't be used again.
                    del request.session['recovery_code']
                    return HttpResponse(json.dumps({'codeStatus':
                        'valid'},
                        ensure_ascii=False),
                        content_type='application/javascript')
                else:
                    log.error('Recovery code does not match!')
                    return HttpResponse(json.dumps({'codeStatus':
                        'invalid'},
                        ensure_ascii=False),
                        content_type='application/javascript')
        # CHANGE PASSWORD (FROM "ACCOUNT RECOVERY REQUEST")
        if request.POST.get('id', '') == 'accountRecoveryReset':
            # Get the recovery email the user input and prepare it to be
            # cleaned. We are grabbing it again from the input to avoid storing
            # user information in cookies.
            # Never trust user's input! Use Django's form to validate them.
            reset_password_form = ResetPasswordForm({
                'email': request.POST.get('email', ''),
                'password': request.POST.get('password', ''),
                're_password': request.POST.get('rePassword', '')
            })
            if reset_password_form.is_valid():
                # Clean the form so its input values become more readable.
                cleaned_reset_password_form = reset_password_form.cleaned_data
                # Make shortcuts for those values.
                email = cleaned_reset_password_form['email']
                password = cleaned_reset_password_form['password']
                re_password = cleaned_reset_password_form['re_password']
                # Get the username from the e-mail and and then get the user
                # using that username.
                username = User.objects.get(email=email)
                user = User.objects.get(username=username)
                if password == re_password:
                    user.set_password(password)
                    user.save()
                    log.info("User's password has been changed successfully!")
                    # Notify the user by e-mail about his account's password
                    # being changed.
                    send_mail(
                        "The Alchemist's Tower: Password has been changed",
                        "Dear %s," % username + "\n\n" +
                        "This is a short message to let you know that your " +
                        "account's password has been changed." + "\n\n" +
                        "if you made this password change, please ignore " +
                        "this notification." + "\n\n" +
                        "However, if you did NOT changed your password, " +
                        "we highly recommend you contact us through our " +
                        "site's Contact section as soon as possible." + "\n\n" +
                        "This notification was sent to this e-mail address " +
                        "because it is associated with your account at The " +
                        "Alchemist Tower. For more information please read " +
                        "our Privacy Policy located in the F.A.Q. section: " +
                        "http://www.thealchemisttower.com" + "\n\n\n" +
                        "Sincerely," + "\n" +
                        "Your friends at The Alchemist's Tower" + "\n" +
                        "http://www.thealchemisttower.com",
                        cleaned_reset_password_form.get('siteowner@example.com',
                        'noreply@example.com'),
                        [email],
                    )
                    return HttpResponse(json.dumps({'newPasswordStatus':
                        'valid'},
                        ensure_ascii=False),
                        content_type='application/javascript')
                # Passwords do not match!
                else:
                    log.error('Passwords do not match!')
                    return HttpResponse(json.dumps({'newPasswordStatus':
                        'passwordsDoNotMatch'},
                        ensure_ascii=False),
                        content_type='application/javascript')
            else:
                # The input is invalid.
                return HttpResponse(json.dumps({'newPasswordStatus':
                        'invalid'},
                        ensure_ascii=False),
                        content_type='application/javascript')
        # CHANGE PASSWORD (FROM "ACCOUNT" BOARD)
        if request.POST.get('id', '') == 'changePasswordFromAccountBoard':
            if request.user.is_authenticated():
                # Never trust user's input! Use Django's form to validate them.
                reset_password_form = ResetPasswordForm({
                    'email': request.user.email,
                    'password': request.POST.get('password', ''),
                    're_password': request.POST.get('rePassword', '')
                })
                if reset_password_form.is_valid():
                    # Clean the form so its input values become more readable.
                    cleaned_reset_password_form =\
                        reset_password_form.cleaned_data
                    # Make shortcuts for those values.
                    email = cleaned_reset_password_form['email']
                    password = cleaned_reset_password_form['password']
                    re_password = cleaned_reset_password_form['re_password']
                    # Get the username from the e-mail and and then get the user
                    # using that username.
                    username = User.objects.get(email=email)
                    user = User.objects.get(username=username)
                    if password == re_password:
                        user.set_password(password)
                        user.save()
                        log.info("User's password has been changed " +\
                            "successfully!")
                        # Notify the user by e-mail about his account's password
                        # being changed.
                        send_mail(
                            "The Alchemist's Tower: Password has been changed",
                            "Dear %s," % username + "\n\n" +
                            "This is a short message to let you know that " +
                            "your account's password has been changed." +
                            "\n\n" +
                            "if you made this password change, please ignore " +
                            "this notification." + "\n\n" +
                            "However, if you did NOT changed your password, " +
                            "we highly recommend you contact us through our " +
                            "site's Contact section as soon as possible." +
                            "\n\n" +
                            "This notification was sent to this e-mail " +
                            "address because it is associated with your " +
                            "account at The Alchemist Tower. For more " +
                            "information please read our Privacy Policy " +
                            "located in the F.A.Q. section: " +
                            "http://www.thealchemisttower.com" + "\n\n\n" +
                            "Sincerely," + "\n" +
                            "Your friends at The Alchemist's Tower" + "\n" +
                            "http://www.thealchemisttower.com",
                            cleaned_reset_password_form.get(
                            'siteowner@example.com',
                            'noreply@example.com'),
                            [email],
                        )
                        return HttpResponse(json.dumps({'newPasswordStatus':
                            'valid'},
                            ensure_ascii=False),
                            content_type='application/javascript')
                    # Passwords do not match!
                    else:
                        log.error('Passwords do not match!')
                        return HttpResponse(json.dumps({'newPasswordStatus':
                            'passwordsDoNotMatch'},
                            ensure_ascii=False),
                            content_type='application/javascript')
                else:
                    # The input is invalid.
                    return HttpResponse(json.dumps({'newPasswordStatus':
                            'invalid'},
                            ensure_ascii=False),
                            content_type='application/javascript')
            else:
                # User is not logged in!
                return HttpResponse(json.dumps({'newPasswordStatus':
                            'userIsNotLoggedIn'},
                            ensure_ascii=False),
                            content_type='application/javascript')
        # CHANGE E-MAIL
        if request.POST.get('id', '') == 'changeEmail':
            if request.user.is_authenticated():
                # Never trust user's input! Use Django's form to validate them.
                reset_email_form = ResetEmailForm({
                    'email': request.POST.get('email', '')
                })
                if reset_email_form.is_valid():
                    old_email = request.user.email
                    username = request.user.username
                    user = User.objects.get(username=username)
                    # Clean the form so its input values become more readable.
                    cleaned_reset_email_form = reset_email_form.cleaned_data
                    new_email = cleaned_reset_email_form['email']
                    user.email = new_email
                    user.save()
                    log.info("User's e-mail has been changed successfully!")
                    # Notify the user by e-mail about his account's e-mail
                    # address being changed.
                    send_mail(
                        "The Alchemist's Tower: E-mail address has been " +
                        "changed",
                        "Dear %s," % username + "\n\n" +
                        "This is a short message to let you know that " +
                        "the e-mail address associated with your account has " +
                        "been changed to: " + new_email + "\n\n" +
                        "From now on you will no longer receive any e-mails " +
                        "from us to this e-mail address, but you can revert " +
                        "this by logging in to your account at any time." +
                        "\n\n" +
                        "If you did NOT changed your e-mail address, " +
                        "we highly recommend you contact us through our " +
                        "site's Contact section as soon as possible." +
                        "\n\n" +
                        "This notification was sent to this e-mail " +
                        "address because it was associated with your " +
                        "account at The Alchemist Tower. For more " +
                        "information please read our Privacy Policy " +
                        "located in the F.A.Q. section: " +
                        "http://www.thealchemisttower.com" + "\n\n\n" +
                        "Sincerely," + "\n" +
                        "Your friends at The Alchemist's Tower" + "\n" +
                        "http://www.thealchemisttower.com",
                        cleaned_reset_email_form.get(
                        'siteowner@example.com',
                        'noreply@example.com'),
                        [old_email],
                    )
                    send_mail(
                        "The Alchemist's Tower: E-mail address has been " +
                        "changed",
                        "Dear %s," % username + "\n\n" +
                        "This is a short message to let you know that from " +
                        "on your account at The Alchemist Tower is associated" +
                        "with this e-mail address." + "\n\n" +
                        "If you did NOT changed your e-mail address, " +
                        "we highly recommend you contact us through our " +
                        "site's Contact section as soon as possible." +
                        "\n\n" +
                        "This notification was sent to this e-mail " +
                        "address because it was associated with your " +
                        "account at The Alchemist Tower. For more " +
                        "information please read our Privacy Policy " +
                        "located in the F.A.Q. section: " +
                        "http://www.thealchemisttower.com" + "\n\n\n" +
                        "Sincerely," + "\n" +
                        "Your friends at The Alchemist's Tower" + "\n" +
                        "http://www.thealchemisttower.com",
                        cleaned_reset_email_form.get(
                        'siteowner@example.com',
                        'noreply@example.com'),
                        [old_email],
                    )
                    return HttpResponse(json.dumps({'newEmailStatus':
                            'valid'},
                            ensure_ascii=False),
                            content_type='application/javascript')
                else:
                    # The input is invalid.
                    return HttpResponse(json.dumps({'newEmailStatus':
                            'invalid'},
                            ensure_ascii=False),
                            content_type='application/javascript')
            else:
                # User is not logged in!
                return HttpResponse(json.dumps({'newEmaildStatus':
                            'userIsNotLoggedIn'},
                            ensure_ascii=False),
                            content_type='application/javascript')
        # REGISTRATION
        if request.POST.get('id', '') == 'registration':
            # Never trust user's input! Use Django's form to validate them.
            registration_form = RegistrationForm({
                'username': request.POST.get('username', ''),
                'email': request.POST.get('email', ''),
                'password': request.POST.get('password', ''),
                're_password': request.POST.get('rePassword', '')
            })
            if registration_form.is_valid():
                # Clean the form so its input values become more readable.
                cleaned_registration_form = registration_form.cleaned_data
                # Make shortcuts for those values.
                username = cleaned_registration_form['username']
                email = cleaned_registration_form['email']
                password = cleaned_registration_form['password']
                re_password = cleaned_registration_form['re_password']
                # Only if passwords match.
                if password == re_password:
                    # Check whether the username is already taken.
                    try:
                        User.objects.get(username=username)
                        log.error('That username already exist!')
                        return HttpResponse(json.dumps({'registrationStatus':
                            'taken'},
                            ensure_ascii=False),
                            content_type='application/javascript')
                    except ObjectDoesNotExist:
                        log.info('The username does not exist yet. Creating...')
                        # Create the new user.
                        new_user = User.objects.create_user(username, email,
                            password)
                        new_user.save()
                        # Log in the user. There is no need to check if the user
                        # exist or if it is valid since we have just created it.
                        user = auth.authenticate(username=username,
                            password=password)
                        auth.login(request, user)
                        # Notify the user by e-mail about his new account being
                        # just created.
                        send_mail(
                            "The Alchemist's Tower: Your new account",
                            "Dear %s," % username + "\n\n" +
                            "Thank you for registering at The Alchemist's " +
                            "Tower. We hope to see you around very often." +
                            "\n\n" +
                            "This notification was sent to this e-mail " +
                            "address because it is associated with your " +
                            "your account at The Alchemist Tower. For more " +
                            "information please read our Privacy Policy " +
                            "located in the F.A.Q. section: " +
                            "http://www.thealchemisttower.com" + "\n\n\n" +
                            "Sincerely," + "\n" +
                            "Your friends at The Alchemist's Tower" + "\n" +
                            "http://www.thealchemisttower.com",
                            cleaned_registration_form.get('siteowner@example.com',
                            'noreply@example.com'),
                            [email],
                        )
                         # Create user's library and fill it with free items.
                        user_library = Library(name=username + 'Lib',
                                               owner=username)
                        # Don't forget to save it!
                        user_library.save()
                        free_items = Item.objects.filter(price=0)
                        for item in free_items:
                            user_library.items.add(item)
                        # Serialize user's library in order to send it through
                        # a json file to the client.
                        serialized_user_library = {}
                        for item in user_library.items.all():
                            serialized_item = {
                                'name': item.name,
                                'type': item.type,
                                'fileUrl': item.file_url,
                                'license': item.license,
                                'price': item.price
                            }
                            serialized_user_library[item.name.lower()] =\
                                serialized_item
                        # Laslty, tell the client the registration is valid and
                        # send it the user's library information.
                        return HttpResponse(json.dumps({'registrationStatus':
                            'valid', 'userLibrary': serialized_user_library,
                            'email': user.email},
                            ensure_ascii=False),
                            content_type='application/javascript')
                # Passwords do not match!
                else:
                    log.error('Passwords do not match!')
                    return HttpResponse(json.dumps({'registrationStatus':
                        'passwordsDoNotMatch'},
                        ensure_ascii=False),
                        content_type='application/javascript')
            else:
                # The input is invalid.
                return HttpResponse(json.dumps({'registrationStatus':
                        'invalid'},
                        ensure_ascii=False),
                        content_type='application/javascript')
        # AUTHENTICATION CHECK
        if request.POST.get('id', '') == 'authenticationCheck':
            if request.user.is_authenticated():
                return HttpResponse(json.dumps({'isAuthenticated':
                            'true'},
                            ensure_ascii=False),
                            content_type='application/javascript')
            else:
                return HttpResponse(json.dumps({'isAuthenticated':
                            'false'},
                            ensure_ascii=False),
                            content_type='application/javascript')
        # CHECK IF CHARACTER IS FREE
        if request.POST.get('id', '') == 'isItFree':
            character_name = request.POST.get('characterName', '')
            character = Item.objects.get(name=character_name)
            is_it_free = 'false'
            is_user_logged = 'false'
            has_item = 'false'
            paypal_form = '<input type="hidden" name="cmd" ' +\
            'value="_s-xclick"><input type="hidden" name="hosted_button_id" ' +\
            'value="">'
            if character.price == 0:
                is_it_free = 'true'
            else:
                is_it_free = 'false'
                # Check then if the user is logged and if already has this item
                # so to avoid prompting him/her to buy it again.
                if request.user.is_authenticated():
                    user_library = Library.objects.get(owner=request.user)
                    if len(user_library.items.filter(name=character_name)) == 0:
                        has_item = 'false'
                    else:
                        has_item = 'true'
            if request.user.is_authenticated():
                is_user_logged = 'true'
                if is_it_free == 'false':
                    paypal_dict = {
                        "business": settings.PAYPAL_RECEIVER_EMAIL,
                        "amount": character.price,
                        "item_name": character.name,
                        "custom": 'user=' + str(request.user) + '|' +\
                            'item_name=' + character.name,
                        # Invoice must have a unique invoice id or PayPal
                        # business account must allow duplicate invoice number.
                        # This is set in My Account > Profile > Selling
                        # Preferences. Block payments Limit payments, add
                        # instructions and more. Click Update.
                        "invoice": generate_code(length=17),
                        "notify_url": settings.PAYPAL_NOTIFY_URL,
                        "return_url": settings.PAYPAL_RETURN_URL,
                        "cancel_return": settings.PAYPAL_CANCEL_URL,
                    }
                    plain_paypal_form = \
                        PayPalEncryptedPaymentsForm(initial=paypal_dict)
                    paypal_form = plain_paypal_form.as_p()
            else:
                is_user_logged = 'false'
            return HttpResponse(json.dumps({'isItFree': is_it_free,
                            'fileUrl': character.file_url,
                            'isUserLogged': is_user_logged,
                            'hasItem': has_item,
                            'paypalForm': paypal_form},
                            ensure_ascii=False),
                            content_type='application/javascript')
    # RENDER VIEW
    return render(request, 'index.html')
