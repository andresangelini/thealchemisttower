# Copyright (C) 2016 Andres Angelini
#
# This file is part of The Alchemist's Tower.
#
# The Achemist's Tower is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Alchemist's Tower is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Website.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.http import HttpResponse, QueryDict
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from paypal.standard.ipn.forms import PayPalIPNForm
from paypal.standard.ipn.models import PayPalIPN
from paypal.standard.models import DEFAULT_ENCODING
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received
from itm.views import Item, Library
from dateutil import parser
import re, time, pytz
from pytz import timezone


log = logging.getLogger(__name__)


@require_POST
@csrf_exempt
def process_ipn(request, item_check_callable=None):
    """
    PayPal IPN endpoint (notify_url).
    Used by both PayPal Payments Pro and Payments Standard to confirm
    transactions.
    http://tinyurl.com/d9vu9d
    PayPal IPN Simulator:
    https://developer.paypal.com/cgi-bin/devscr?cmd=_ipn-link-session
    """
    # TODO: Clean up code so that we don't need to set None here and have a lot
    #       of if checks just to determine if flag is set.
    flag = None
    ipn_obj = None

    # Clean up the data as PayPal sends some weird values such as "N/A"
    # Also, need to cope with custom encoding, which is stored in the body (!).
    # Assuming the tolerant parsing of QueryDict and an ASCII-like encoding,
    # such as windows-1252, latin1 or UTF8, the following will work:
    encoding = request.POST.get('charset', None)
    
    encoding_missing = encoding is None
    if encoding_missing:
        encoding = DEFAULT_ENCODING
    
    try:
        data = QueryDict(request.body, encoding=encoding).copy()
    except LookupError:
        data = None
        flag = "Invalid form - invalid charset"
    
    if data is not None:
        if hasattr(PayPalIPN._meta, 'get_fields'):
            date_fields = [f.attname for f in PayPalIPN._meta.get_fields() if \
                f.__class__.__name__ == 'DateTimeField']
        else:
            date_fields = [f.attname for f, m in \
                PayPalIPN._meta.get_fields_with_model() if \
                f.__class__.__name__ == 'DateTimeField']
                
        for date_field in date_fields:
            if data.get(date_field) == 'N/A':
                del data[date_field]
        
        # Reformat 'for_auction' field. PayPal uses boolean values while
        # django-paypal expects 0 or 1.
        if request.POST.get('for_auction', ''):
            if request.POST.get('for_auction', 'TRUE'):
                data.__setitem__('for_auction', '1')
            else:
                data.__setitem__('for_auction', '0')
        
        # Django-paypal's fields' maximum length differs from PayPal's.
        if request.POST.get('reason_code', ''):
            reason_code = request.POST.get('reason_code', '')
            if len(reason_code) > 15:
                data.__setitem__('reason_code', reason_code[0:15])
        if request.POST.get('pending_reason', ''):
            pending_reason = request.POST.get('pending_reason', '')
            if len(pending_reason) > 15:
                data.__setitem__('pending_reason', pending_reason[0:14])
        if request.POST.get('payment_status', ''):
            payment_status = request.POST.get('payment_status', '')
            if len(payment_status) > 17:
                data.__setitem__('payment_status', payment_status[0:17])
        
        # Create the PayPalIPNForm with the processed data.        
        form = PayPalIPNForm(data)
        
        # IMPORTANT:
        # Django-paypal expects datetimes to have the format:
        # '%H:%:M:%S %b %d, %Y %z' where 'z' could be either 'PDT' or 'PSD'.
        # It will raise an error if any other format is used. When testing with
        # Poster, PayPal's IPN Simulator or any other tool, please make sure
        # to use the format mentioned above.
        if form.is_valid():
            log.info('IPN form is valid. Commiting to DB...')
            try:
                # When commit = False, object is returned without saving to DB.
                ipn_obj = form.save(commit=False)
                log.info('IPN object commited without saving it to DB.')
            except Exception as e:
                flag = "Exception while processing. (%s)" % e
                log.error(flag)
        else:
            flag = "Invalid form. ({0})".format(", ".join(["{0}: {1}".format(k,\
                ", ".join(v)) for k, v in form.errors.items()]))
            log.error(flag)
    
    if ipn_obj is None:
        ipn_obj = PayPalIPN()
    
    # Set query params and sender's IP address
    ipn_obj.initialize(request)
    
    if flag is not None:
        # We save errors in the flag field
        ipn_obj.set_flag(flag)
    else:
        # Secrets should only be used over SSL.
        if request.is_secure() and 'secret' in request.GET:
            ipn_obj.verify_secret(form, request.GET['secret'])
        else:
            ipn_obj.verify(item_check_callable)
    
    ipn_obj.save()
    ipn_obj.send_signals()
    log.info('IPN object saved and signals sent.')
    
    if encoding_missing:
        # Wait until we have an ID to log warning
        log.warning("No charset passed with PayPalIPN: %s. Guessing %s", \
            ipn_obj.id, encoding)
    
    # If IPN is verified, process it.
    if PayPalIPN.objects.get(id=ipn_obj.id).response == 'VERIFIED':
        
        # Get the payment status.
        payment_status = PayPalIPN.objects.get(id=ipn_obj.id).payment_status

        # Get the custom field content and turn into a dictionary to extract the
        # buyer's username as well as other data we may have written.
        raw_custom_field = PayPalIPN.objects.get(id=ipn_obj.id).custom
        custom_dict = {}
        # Check if 'user' and 'item_name' fields exist inside the 'custom' field
        # before adding them to the dictionary.
        if 'user' in raw_custom_field and 'item_name' in raw_custom_field:
            for i in re.split(r'\|', raw_custom_field):
                custom_dict[re.split('=', i)[0]] = re.split('=', i)[1]
        else:
            if 'user' not in raw_custom_field:
                log.error("No 'user' field has been found inside " + \
                      "'custom' field.")
            if 'item_name' not in raw_custom_field:
                log.error("No 'item_name' field has been found inside" + \
                      "'custom' field.")
            log.error('process aborted.')
            return False
        
        # DEFINE FUNCTION FOR ADDING ITEM TO USER'S LIBRARY.
        def add_item_to_user_library():
            log.info("Checking if item already exists in user's library...")
            user = custom_dict['user']
            item_name = custom_dict['item_name']
            try:
                item = Item.objects.get(name=item_name)
            except ObjectDoesNotExist:
                log.error('This item does not exist in our library. ' +\
                    'Process aborted.')
                return False
            try:
                user_library = Library.objects.get(owner=user)
            except ObjectDoesNotExist:
                log.error('This user does not exist in our database. ' +\
                    'Process aborted.')
                return False
            if len(user_library.items.filter(name=item_name)) == 0:
                log.info("User does not have this item yet. Adding it to " +\
                    "user's library...")
                user_library.items.add(item)
                log.info("User's library now looks like this:")
                for item in user_library.items.all():
                    log.info(item)
            else:
                log.info('User already has this item. There is no need to ' +\
                    'add it again. No action taken.')

        # DEFINE A FUNCTION FOR REMOVING ITEM FROM USER'S LIBRARY.
        def remove_item_from_user_library():
            # First, check if user has the item.
            user = custom_dict['user']
            item_name = custom_dict['item_name']
            try:
                item = Item.objects.get(name=item_name)
            except ObjectDoesNotExist:
                log.error('This item does not exist in our library. Process ' +\
                    'aborted.')
                return False
            try:
                user_library = Library.objects.get(owner=user)
            except ObjectDoesNotExist:
                log.error('This user does not exist in our database.')
                return False
            if len(user_library.items.filter(name=item_name)) != 0:
                user_library.items = user_library.items.exclude(name=item.name)
                log.info("Item has been removed from user's library. " + \
                    "User's library now looks like this:")
                for item in user_library.items.all():
                    log.info(item)
            else:
                log.info("Item does not exist in user's library. " +\
                    "No action taken.")
        def print_fraud_management_pending_filter_x():
            # Check for fraud management pending filters and print their names
            # only if they exist (there could be more than one).
            log.info('Fraud management pending filter:')
            fmpfx = re.search('fraud_management_pending_filters_' +\
                r'([1-9]*)\d', raw_custom_field)
            if fmpfx is not None:
                # Print the name of each filter (there may be more than one)
                # which caused the payment to be pending.
                for key in custom_dict:
                    if 'fraud_management_pending_filters_' in key:
                        fmpfx = re.search('fraud_management_pending_filters_' +\
                        r'([1-9]*)\d', key)
                        fmpf = fmpfx.group()
                        filter_id = custom_dict[fmpf]
                        if filter_id == '1':
                            log.info('- AVS No Match')
                        if filter_id == '2':
                            log.info('- AVS Partial Match')
                        if filter_id == '3':
                            log.info('- AVS Unavailable/Unsupported')
                        if filter_id == '4':
                            log.info('- Card Security Code (CSC) Mismatch')
                        if filter_id == '5':
                            log.info('- Maximum Transaction Amount')
                        if filter_id == '6':
                            log.info('- Unconfirmed Address')
                        if filter_id == '7':
                            log.info('- Country Monitor')
                        if filter_id == '8':
                            log.info('- Large Order Number')
                        if filter_id == '9':
                            log.info('- Billing/Shipping Address Mismatch')
                        if filter_id == '10':
                            log.info('- Risky ZIP Code')
                        if filter_id == '11':
                            log.info('- Suspected Freight Forwarder Check')
                        if filter_id == '12':
                            log.info('- Total Purchase Price Minimum')
                        if filter_id == '13':
                            log.info('- IP Address Velocity')
                        if filter_id == '14':
                            log.info('- Risky Email Address Domain Check')
                        if filter_id == '15':
                            log.info('- Risky Bank Identification Number ' +\
                                '(BIN) Check')
                        if filter_id == '16':
                            log.info('- Risky IP Address Range')
                        if filter_id == '17':
                            log.info('- PayPal Fraud Model')
                        
                        if int(filter_id) not in range(1, 18):
                            log.info("Could not recognize fraud management " +\
                                "pending filter. Please, check PayPal's " +\
                                "documentation.")
            else:
                log.info('No fraud management pending filter found.')
        def print_reason_code():
            try:
                reason_code = PayPalIPN.objects.get(id=ipn_obj.id).reason_code
                if reason_code == 'adjustment_reve':
                    log.info('Reason code: adjustment_reversal\n' +\
                        'Reversal of an adjustment')
                if reason_code == 'admin_fraud_rev':
                    log.info('Reason code: admin_fraud_reversal\n' +\
                        'The transaction has been reversed due to fraud ' +\
                        'detected by PayPal administrators.')
                if reason_code == 'admin_reversal':
                    log.info('Reason code: admin_reversal\n' +\
                        'The transaction has been reversed by PayPal ' +\
                        'administrators.')
                if reason_code == 'buyer-complaint':
                    log.info('Reason code: buyer-complaint\n' +\
                        'The transaction has been reversed due to a ' +\
                        'complaint from your customer.')
                if reason_code == 'chargeback':
                    log.info('Reason code: chargeback\n' +\
                        'The transaction has been reversed due to a ' +\
                        'chargeback by your customer.')
                if reason_code == 'chargeback_reim':
                    log.info('Reason code: chargeback_reimbursement\n' +\
                        'Reimbursement for a chargeback.')
                if reason_code == 'chargeback_sett':
                    log.info('Reason code: chargeback_settlement\n' +\
                             'Settlement of a chargeback.')
                if reason_code == 'guarantee':
                    log.info('Reason code: guarantee\n' +\
                        'The transaction has been reversed because your ' +\
                        'customer exercised a money-back guarantee.')
                if reason_code == 'other':
                    log.info('Reason code: other\n' + 'Unspecified reason.')
                if reason_code == 'refund':
                    log.info('Reason code: refund\n' +\
                        'The transaction has been reversed because you ' +\
                        'gave the customer a refund.')
                if reason_code == 'regulatory_bloc':
                    log.info('Reason code: regulatory_block\n' +\
                        'PayPal blocked the transaction due to a ' +\
                        'violation of a government regulation. In this ' +\
                        'case, payment_status is Denied.')
                if reason_code == 'regulatory_reje':
                    log.info('Reason code: regulatory_reject\n' +\
                        'PayPal rejected the transaction due to a ' +\
                        'violation of a government regulation and returned ' +\
                        'the funds to the buyer. In this case, ' +\
                        'payment_status is Denied.')
                if reason_code == 'regulatory_revi':
                    log.info('Reason code: regulatory_review_exceeding_sla\n' +\
                        'PayPal did not complete the review for ' +\
                        'compliance with government regulations within 72 ' +\
                        'hours, as required. Consequently, PayPal ' +\
                        'auto-reversed the transaction and returned the ' +\
                        'funds to the buyer. In this case, payment_status ' +\
                        'is Denied. Note that "sla" stand for "service ' +\
                        'level agreement".')
                if reason_code == 'unauthorized_cl':
                    log.info('Reason code: unauthorized_claim\n' +\
                        'The transaction has been reversed because it ' +\
                        'was not authorized by the buyer.')
                if reason_code == 'unauthorized_sp':
                    log.info('Reason code: unauthorized_spoof\n' +\
                        'The transaction has been reversed due to a ' +\
                        'customer dispute in which an unauthorized spoof is ' +\
                        'suspected.')
                # Note: Additional codes may be returned.
                log.info("Note: Addition reason codes may be returned by "+\
                    "PayPal that are not recognized. Please, check " +\
                    "PayPal's documentation.")
            except ObjectDoesNotExist:
                log.info('No reason code found.')
        log.info("PayPal's Instant Payment Notification received. " +\
            "Processing...")
        # PAYMENT STATUS: CANCELED REVERSAL
        if payment_status == 'Canceled_Reversal':
            log.info('Payment status: Canceled_Reversal\n' +\
                "A reversal has been canceled. Funds for a transaction " +\
                "that was reversed during a dispute with the customer have " +\
                "been returned to The Alchemist's Tower and thus, the item " +\
                "must be returned to the customer too.")
            print_reason_code()
            add_item_to_user_library()
        # PAYMENT STATUS: COMPLETED
        if payment_status == 'Completed':
            log.info('Payment status: Completed')
            print_fraud_management_pending_filter_x()
            add_item_to_user_library()
        # PAYMENT STATUS: CREATED
        if payment_status == 'Created':
            print "A German ELV payment has been made using Express " +\
                "Checkout. Treating equally as 'Completed'..."
            add_item_to_user_library()
        # PAYMENT STATUS: DENIED OR DECLINED
        if payment_status == 'Denied' or payment_status == 'Declined':
            log.info('Payment status: Denied\n' +\
                'The payment has been denied. This happened because ' +\
                'the payment was previously pending. No action taken.')
            print_fraud_management_pending_filter_x()
            print_reason_code()
        # PAYMENT STATUS: EXPIRED
        if payment_status == 'Expired':
            log.info('Payment status: Expired\n' +\
                'Payment authorization has expired and cannot be captured.')
            log.info("Removing item from user's library if present.")
            remove_item_from_user_library()
        # PAYMENT STATUS: FAILED
        if payment_status == 'Failed':
            log.info("Payment status: Failed\n" +\
                "The payment has failed. This happens only if the payment " +\
                "was made from the costumer's bank account.")
            log.info("Removing item from user's library if present.")
            remove_item_from_user_library()
        # PAYMENT STATUS: PENDING
        if payment_status == 'Pending':
            log.info('Payment status: Pending')
            try:
                pending_reason = \
                    PayPalIPN.objects.get(id=ipn_obj.id).pending_reason

                if pending_reason == 'address':
                    log.info('Pending reason: address\n' +\
                        'The customer did not include a confirmed ' +\
                        'shipping address and your Payment Receiving ' +\
                        'Preferences is set to allow you to manually accept ' +\
                        'or deny each of these payments. To change your ' +\
                        'preference, go to the Preferences section of your ' +\
                        'Profile.')
                if pending_reason == 'authorization':
                    log.info('Pending reason: authorization\n' +\
                        'You set the payment action to Authorization and ' +\
                        'have not yet captured funds.')
                if pending_reason == 'echeck':
                    log.info('Pending reason: echeck\n' +\
                        'The payment is pending because it was made by an ' +\
                        'eCheck that has not yet cleared.')
                if pending_reason == 'intl':
                    log.info('Pending reason: intl\n' +\
                        'The payment is pending because you hold a ' +\
                        'non-U.S. account and do not have a withdrawal ' +\
                        'mechanism. You must manually accept or deny this ' +\
                        'payment from your Account Overview.')
                if pending_reason == 'multi_currency':
                    log.info("Pending reason: multi_currency\n" +\
                        "You do not have a balance in the currency sent, " +\
                        "and you do not have your profiles's Payment " +\
                        "Receiving Preferences option set to automatically " +\
                        "convert and accept this payment. As a result, you " +\
                        "must manually accept or deny this payment.")
                if pending_reason == 'order':
                    log.info('Pending reason: order\n' +\
                        'You set the payment action to Order and have not ' +\
                        'yet captured funds.')
                if pending_reason == 'paymentreview':
                    log.info('Pending reason: paymentreview\n' +\
                        'The payment is pending while it is reviewed by ' +\
                        'PayPal for risk.')
                if pending_reason == 'regulatory_rev':
                    log.info('Pending reason: regulatory_review\n' +\
                        'The payment is pending because PayPal is ' +\
                        'reviewing it for compliance with government ' +\
                        'regulations. PayPal will complete this review ' +\
                        'within 72 hours. When the the review is complete, ' +\
                        'you will receive a second IPN the message whose ' +\
                        'payment_status/reason code variables indicate the ' +\
                        'result.')
                if pending_reason == 'unilateral':
                    log.info('Pending reason: unilateral\n' +\
                        'The payment is pending because it was made to an ' +\
                        'email address that is not yet registered or ' +\
                        'confirmed.')
                if pending_reason == 'upgrade':
                    log.info('Pending reason: upgrade\n' +\
                        'The payment is pending because it was made via ' +\
                        'credit card and you must upgrade your account to ' +\
                        'Business or Premier status before you can receive ' +\
                        'the funds. Upgrade can also mean that you have ' +\
                        'reached the the monthly limit for transactions on ' +\
                        'your account.')
                if pending_reason == 'verify':
                    log.info('Pending reason: verify\n' +\
                        'The payment is pending because you are not yet ' +\
                        'verified. You must verify your account before you ' +\
                        'can accept this payment.')
                if pending_reason == 'other':
                    log.info('Pending reason: other\n' +\
                        'The payment is pending for a reason other than ' +\
                        'those listed. For more information, contact PayPal ' +\
                        'Customer Service.')
                else:
                    log.warning('Could not recognize pending reason or ' +\
                        'field is empty.')
            except ObjectDoesNotExist:
                log.warning('No pending reason found. Is it the fraud ' +\
                    'managment pending filter perhaps?')
            print_fraud_management_pending_filter_x()
            log.info('No action taken.')
        # PAYMENT STATUS: REFUNDED
        if payment_status == 'Refunded':
            log.info("Payment status: Refunded\n" +\
                "Customer's payment has been refunded. No action taken.")
            print_reason_code()
        # PAYMENT STATUS: REVERSED
        if payment_status == 'Reversed':
            log.info('Payment status: Reversed\n' +\
                'A payment was reversed due to a chargeback or other type ' +\
                'of reversal. The funds have been removed from your account ' +\
                'balance and returned to the buyer. The reason for the ' +\
                'reversal is specified in the ReasonCode element.')
            print_reason_code()
            log.info("Removing item from user's library if present.")
            remove_item_from_user_library()
        # PAYMENT STATUS: PROCESSED
        if payment_status == 'Processed':
            log.info('Payment status: Processed\n' +\
                'A payment has been accepted. Waiting to be completed. No ' +\
                'action taken.')
        # PAYMENT STATUS: VOIDED
        if payment_status == 'Voided':
            log.info('Payment status: Voided\n' +\
                'This authorization has been voided.')
            log.info("Removing item from user's library if present.")
            remove_item_from_user_library()
        # PAYMENT STATUS: IN PROGRESS
        if payment_status == 'In-Progress':
            log.info('Payment status: In-Progress\n' +\
                'There is no documentation on this status. No action taken.')
        if payment_status == 'Partially_Refunde':
            log.info('Payment status: Partially_Refunded\n' +\
                'There is no documentation on this status. No action taken.')
    else:
        log.info('IPN is INVALID')
    return HttpResponse("OKAY")
