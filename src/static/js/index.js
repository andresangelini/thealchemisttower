/**
 *
 * @source: http://www.thealchemisttower.com/static/js/index.js
 *
 * @licstart  The following is the entire license notice for the 
 *  JavaScript code in this page.
 *
 * Copyright (C) 2016  Andres Angelini and Sergio de los Santos
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

/****************************** READ ME FIRST **********************************
The following code uses Object Oriented Programming as a structure. Therefore,
it was designed in blocks. To be specific, there are three major blocks: two of
them holding the functions to make things work ("gui" and "trid"), and a third
one which runs all the functions. In fact, the first two blocks are objects and
as such, everytime you want to call one of the functions (which are properties,
to be precise) they contain, regardless of if you are calling them from inside
or outside the blocks, you need to prepend the name of the block they belong to,
like this: myBlock.myFunction(), otherwise it won't work at all.
*******************************************************************************/

/*******************************************************************************
TRID: initializes inner functions when called from outside. Contains all
the code related to 3d content needed to run canvas.

HOW TO USE: Invoke inner function from outside by adding "trid." (whitout
quotes) in front of function.
*******************************************************************************/
var trid = {
	/*--------------------------------------------------------------------------
	VARIABLE DECLARATION: These variables are manipulated by many different
	functions.
	--------------------------------------------------------------------------*/
	actionOldTime: 0,
	actionTimer: new THREE.Clock(),
	backgroundAmbientLight: null,
	backgroundCamera: null,
	backgroundControls: null,
	clock: new THREE.Clock(),
	cameraTimer: new THREE.Clock(),
	titleFadeInTimer: new THREE.Clock(),
	titleFadeOutTimer: new THREE.Clock(),
	backgroundRenderer: null,
	backgroundScene: null,
	isActionPlaying: false,
	isViewerReady: false,
	modelsToBeLoaded: 0,
	modelsLoaded: 0,
	texturesToBeLoaded: 0,
	texturesLoaded: 0,
	uvsReferenceX: 0,
	uvsReferenceY: 0,
	viewerCamera: null,
	viewerControls: null,
	viewerClock: new THREE.Clock(),
	viewerRenderer: null,
	viewerScene: null,
	/*--------------------------------------------------------------------------
	ARRAYS: Elements are save into these arrays to be referenced by index
	number.
	--------------------------------------------------------------------------*/
	animations: new Array (),
	emptyHelpers: new Array (),
	isLoaded: new Array (),
	locators: new Array (),
	models: new Array (),
	textures: new Array (),
	spotLights: new Array (),
	uvs: new Array (),
	/*--------------------------------------------------------------------------
	LOAD MODEL: Loads model and returns a geometry. It also detects whether it
	has animations or not. If it does, it creates a skinneable mesh with the
	loaded geometry. If it doesn't, it creates a regular mesh. Regardless of the
	type of mesh it creates, the returned object (the mesh or skinneable mesh)
	is saved into a global array for later use.

	HOW TO USE: Just invoke and pass the url path of the JSON file as argument.
	
	Parameters:

	url - Url path to the JSON model file.
	--------------------------------------------------------------------------*/
	loadModel: function (url) {
		// Fill the array where the models will be saved into with the JSON url
		// so as to identify in which slot to save said models once the are
		// loaded.
		trid.models.push(url);
		
		// Fill the animations array also, regardless wether they exist or not,
		// so that the animations can be identified by the model index number,
		// creating an array of sub-arrays in the form of [model][animation]. 
		trid.animations.push(null);
		
		// Create a shortcut variable for loader.
		loader = new THREE.JSONLoader();
		trid.modelsToBeLoaded += 1;
		
		loader.load(url, function (geometry) {
			var mesh;
			
			if (geometry.animation === undefined) {
				mesh = new THREE.Mesh(geometry);
				trid.modelsLoaded += 1;
			}
			else {
				console.log(url);
				mesh = new THREE.SkinnedMesh(geometry);
				trid.modelsLoaded += 1;
			}
			// Identify array slot by the url and save the model in it.
			for (var i = 0; i < trid.models.length; i++) {
				if (trid.models[i] === url) {
					trid.models[i] = mesh;
				}
			}
		}, staticUrl + 'art/img/3d/tex/');
		
	},
	/*--------------------------------------------------------------------------
	ANIMATE FX UVS: Create a function which only calls for the UVs animation
	when the proper character animation is being displayed.
	
	HOW TO USE: Invoke this function at render cycle (where
	'requestAnimationFrame' is called) passing the following parameters as
	arguments.
	
	Parameters:
	
	modelIndex         - Index number of the model whose uvs are to be animated.
	characterIndex     - Index number of the character associated with the
	                     effect.
	actionIndex        - Index number of the action.
	callback           - The actual function responsible for animating the UVS.
	--------------------------------------------------------------------------*/
	animateFxUvs: function (modelIndex, characterIndex, actionIndex,
		callback) {
			var characterAction; // Shortcut for character's animation.
			var currentUvs;      // Shortcut for currently UVs being animated.
			var originalUvs;     // Shortcut for orinial UVs.
			
			// Make shortcuts.
			characterAction = trid.animations[characterIndex][actionIndex];
			currentUvs = trid.models[modelIndex].geometry.faceVertexUvs;
			originalUvs = trid.uvs[modelIndex];
			
			// Animate UVs only when its associated character is being
			// displayed and its animations is playing. Otherwise, hide and
			// return UVs to their original position.
			if (characterAction.isPlaying === true &&
				trid.models[characterIndex].visible === true) {
					callback();
			} else {
				trid.models[modelIndex].visible = false;
				
				for (var i = 0; i < currentUvs[0].length; i++) {
					for (var k = 0; k < currentUvs[0][i].length; k++) {
						currentUvs[0][i][k].x = originalUvs[0][i][k].x;
						currentUvs[0][i][k].y = originalUvs[0][i][k].y;
					}
				}
			}
	},
	/*--------------------------------------------------------------------------
	ANIMATE UVS: Animates models' uv mapping according to determined amount of
	time or indefinitely, and hides model when is not animating.
	
	HOW TO USE: Invoke this function at render cycle (where
	'requestAnimationFrame' is called) passing the fallowing parameters as
	arguments:
	
	Parameters:
	
	modelIndex     - Index number of the model whose uvs are to be animated.
	hTiles         - Number of horizontal tiles in the texture.
	vTiles         - Number of vertical tiles in the texture.
	time           - Time to be used as variable.
	tileTime       - The amount of time each tile is displayed for.
	start          - Frame in which animation starts.
	end            - Frame in which animation ends.
	--------------------------------------------------------------------------*/
	animateUvs: function (modelIndex, hTiles, vTiles, time, tileTime, start,
		end) {
			var currentUvs;  // Shortcut for currently UVs being animated.
			var hLoc;        // UVs horizontal location.
			var originalUvs; // Shortcut for orinial UVs.
			var vLoc;        // UVs vertical location.
		
			currentUvs = trid.models[modelIndex].geometry.faceVertexUvs;
			originalUvs = trid.uvs[modelIndex];
			
			/*------------------------------------------------------------------
			Set uvs x location as a sawtooth function of time. UVs will be
			translated over the horizontal tiles one tile at a time until they
			reach the last one. Then they will be translated back at the
			begining. This behaivor set as a function of time describes a
			sawtooth graph. The general form of a sawtooth function is as
			follows:
			
			f(t) = a((t - transX)/p - floor(transY + (t - transX)/p))
			
			Where:
			
			a      - Amplitud.
			t      - The variable (in this case, the time).
			transX - Translation over the x axis.
			transY - Translation over the y axis.
			p      - Period.
			
			
			  f(t) -
			       -
			       -
			       -
			      ---       .        .        .        .        .
			       -       .        .        .        .        .
			       -      .        .        .        .        .        .
			       -     .        .        .        .        .        .
			     a -    .        .        .        .        .        .
			       -   .        .        .        .        .        .
			       -  .        .        .        .        .        .
			       - .        .        .        .        .        .
			      ---        .        .        .        .        .
			       -
			transY -
			       -         -       -
			      ------------------------------------------------------
			       -    p    -       -                                 t
			              transX
			
			We just want integers as output so we place it into a floor
			function, resulting in a sawtooth-step hybrid function. This
			way each tile take a set amount of time. During that lapse of time
			the same result will be obteined, and so the same uvs x position
			because we are saying that for each rendered frame during that time
			the uvs x position is EQUAL to its original position PLUS whatever
			output the function gives at a given time.
			
			
			  f(t) -
			       -
			       -
			       -
			       -
			      ---     ...      ...      ...      ...      ...      .
			       -
			       -
			     a -   ...      ...      ...      ...      ...      ...
			       -
			       -
			      ---..      ...      ...      ...      ...      ...
			       -
			transY -
			       -         -        -
			      ------------------------------------------------------
			       -   p     -        -                                t
			              transX
			
			Now the only thing left to do is replacing the elements of
			the function with the apropiated variables:
			
			a = hTiles      - The hTiles uvs travels in a period.
			t = time        - Time variable this function depends on.
			transX = start  - UVs x position should be 0 at start.
			transY = 0      - We want it to be 0 so that the hLoc /
			                  hTiles equals 0 and nothing gets added to
			                  the uvs x position in the first tile of
			                  each horizontal line.
			period = hTiles - Once uvs reaches last tile in each
			                  horizontal line it must go back to the
			                  first tile and start again.
			------------------------------------------------------------------*/
			hLoc = Math.floor(hTiles * (((time - start) / (tileTime
				* hTiles)) - Math.floor((time - start) / (tileTime * hTiles))));
			/*------------------------------------------------------------------
			Following the same reasoning we now do it for the vertical
			position. This time we replace the elements with the next
			variables:
			
			a = vTiles               - The vTiles uvs travels in a period.
			t = time                 - Time variable this function depends on.
			transX = start           - UVs x position should be 0 at start.
			transY = 0               - For the same reason as before.
			period = hTiles * vTiles - Once the uvs reach the very last tile
			                           (upper right corner) they should go
			                           back to the beginning to start all
			                           over again.
			------------------------------------------------------------------*/
			vLoc = Math.floor(vTiles * (((time - start) /
						(tileTime * hTiles * vTiles)) - Math.floor((time -
							start) / (tileTime * hTiles * vTiles))));
			
			// Run through uvs array.
			for (var i = 0; i < currentUvs[0].length; i++) {
				for (var k = 0; k < currentUvs[0][i].length; k++) {
					// Animate uvs only during the set amount of time. Otherwise
					// just reset uvs to their original position and hide model.
					if (time >= start && time <= end) {
						trid.models[modelIndex].visible = true;
						// Deal with the non-trivial case where hTiles = 1.
						// Otherwise, just go right.
						if (hTiles === 1) {
							currentUvs[0][i][k].x = originalUvs[0][i][k].x;
						} else {
							currentUvs[0][i][k].x = originalUvs[0][i][k].x +
								(hLoc / hTiles);
						}
						
						// Deal with the non-trivial case where vTiles = 1.
						// Otherwise, just go up.
						if (vTiles === 1) {
								currentUvs[0][i][k].y = originalUvs[0][i][k].y;
						} else {
							currentUvs[0][i][k].y = originalUvs[0][i][k].y +
								(vLoc / vTiles);
						}
					}
					else {
						trid.models[modelIndex].visible = false;
						currentUvs[0][i][k].x = originalUvs[0][i][k].x;
						currentUvs[0][i][k].y = originalUvs[0][i][k].y;
					}
				}
			}
	},
	/*--------------------------------------------------------------------------
	CHECK LOADING: Check loading progress. Once everything is loaded, run
	a function, for example, "blendTransition" or "setScene".
	--------------------------------------------------------------------------*/
	checkLoading: function (callback) {
		var check = setInterval(function () {
			console.log('textures: ' + trid.texturesLoaded + " / "
				+ trid.texturesToBeLoaded);
			console.log('models: ' + trid.modelsLoaded + " / "
				+ trid.modelsToBeLoaded);
			if (trid.texturesLoaded >= trid.texturesToBeLoaded &&
				trid.modelsLoaded >= trid.modelsToBeLoaded) {
				clearInterval(check);
				console.log('Done!');
				callback();
			}
		}, 1);
	},
	/*--------------------------------------------------------------------------
	LOAD TEXTURE: Loads texture and counts the one to be loaded as well as the
	ones which has been loaded so as to be checked later. Finally, it saves
	texture into a global array.

	HOW TO USE: Just invoke with the url to the texture file as argument.
	
	Parameters:

	url - Url to the texture file to be loaded.
	--------------------------------------------------------------------------*/
	loadTexture: function (url) {
		var texture;
		
		trid.texturesToBeLoaded += 1;
		texture = THREE.ImageUtils.loadTexture(url, undefined, function () {
			trid.texturesLoaded += 1;
		});
		
		trid.textures.push(texture);
	},
	/*--------------------------------------------------------------------------
	ENABLE ANIMATION: enable model animations.
	
	Important: For some reason, Three.js r69 seems to look for "animation"
	(singular) instead of "animations" (plural) in the json file. Therefore, in
	order for Three.js to be able to reach the animation information, both, in
	the json file as well as in the function used to call it must be written as
	"animation" (singular). Also, make sure the json file has been output in the
	right format, which might change with every release of both, the blender
	exporter as well as Three.js itself. Otherwise, it won't work at all.
	--------------------------------------------------------------------------*/
	enableAnimation: function (modelIndex, animationIndex) {
		var mesh; // Just a shortcut for the model.
		
		mesh = trid.models[modelIndex];
		
		// Create an array to store the animations in.
		trid.animations[modelIndex] = new Array ();
		
		// Each animation is its own thing, instead of being a model property.
		// Each one takes the geometry and animation as argumets so we have to
		// save them into a global array to be able to use them later on.
		for (var i = 0; i < mesh.geometry.animation.length; i++) {
			
			// Doing it this way there will be duplicated animations in the
			// animation library but Three.js will take care of it by
			// overwriing them.
			THREE.AnimationHandler.add(mesh.geometry.animation[i]);
			
			animation = new THREE.Animation(mesh,
				// Call the animations by the animation property item "name"
				// instead of them by their names themselves so as to use a
				// generic function.
				mesh.geometry.animation[i].name,
				THREE.AnimationHandler.CATMULLROM);
			
			animation.interpolation
			trid.animations[modelIndex].push(animation);
			
			if (i === animationIndex) {
				animation.play();
			}
		}
	},
	
	/*--------------------------------------------------------------------------
	LOAD UVS: Load original objects uvs to be able to go back to them later.
	--------------------------------------------------------------------------*/
	loadUvs: function (modelIndex) {
		var mesh;
		var geometry;
		var originalFaceVertexUvs = new Array ();
		
		mesh = trid.models[modelIndex];
		geometry = mesh.geometry;
		
		// Create an axact copy or image of the array containing the uvs.
		// The original uvs are saved in the following manner:
		// faceVertexUvs[layer index][face index][vertex index].x and
		// faceVertexUvs[layer index][face index][vertex index].y, since in the
		// vertex array the coordinates are stored as objects.
		originalFaceVertexUvs[0] = new Array ();
		for (var i = 0; i < geometry.faceVertexUvs[0].length; i++) {
			originalFaceVertexUvs[0].push(new Array ());
			for (var k = 0; k < geometry.faceVertexUvs[0][i].length; k++) {
				originalFaceVertexUvs[0][i].push({
					"x": geometry.faceVertexUvs[0][i][k].x,
					"y": geometry.faceVertexUvs[0][i][k].y
				});
			}
		}
		
		trid.uvs.push(originalFaceVertexUvs);
	},
	
	/*--------------------------------------------------------------------------
	RESETUVS: Reset uvs to their original state as in the json file.
	--------------------------------------------------------------------------*/
	resetUvs: function (modelIndex) {
		var mesh;
		var geometry;
		
		mesh = trid.models[modelIndex];
		geometry = mesh.geometry;
		
		for (var i = 0; i < geometry.faceVertexUvs[0].length; i++) {
			for (var k = 0; k < geometry.faceVertexUvs[0][i].length;
				k++) {
					geometry.faceVertexUvs[0][i][k].x =
						trid.uvs[modelIndex][0][i][k].x;
					geometry.faceVertexUvs[0][i][k].y =
						trid.uvs[modelIndex][0][i][k].y;
			}
		}
		
	}
};
/*******************************************************************************
GUI: initializes inner functions when called from outside. Contains all
the code needed to run the gui.

HOW TO USE: Invoke inner function from outside by adding "gui." (whitout
quotes) in front of function.
*******************************************************************************/
var gui = {
	// VARIABLES ("properties", to be accurate)
	
	// DOM elements	
	actionIndex: null,
	actionName: '#actionName',
	characterMenu: '.characterMenu',
	deviceSHeight: null,
	deviceSWidth: null,
	prompt: null,
	FAQLink: '#FAQLink',
	mainMenu: '.mainMenu',
	minWidth: null,
	mouseWheelEvent: null,
	sHeight: null,
	sWidth: null,
	userMessage: null,
	lastInHeight: null,               // lastInnerHeight      - For zooming.
	lastInWidth: null,                // lastInnerWidth       - For zooming.
	lastOutHeight: null,              // lastOuterHeight      - For zooming.
	lastOutWidth: null,               // lastOuterWidth       - For zooming.
	lastDPR: null,                    // lastDevicePixelRatio - For zooming.
	recoveryEmail: null,
	userEmail: null,
	
	// ARRAYS - To be used for linking element to actions by their index number.
	accountPasswordForm: [
		'#accountPasswordInput',                                   // Index: 0
		'#accountRePasswordInput'                                  // Index: 1
	],
	action: [
		"Attack 1",                                                // Index: 0
		"Attack 2",                                                // Index: 1
		"Attack 3",                                                // Index: 2
		"Block",                                                   // Index: 3
		"Death",                                                   // Index: 4
		"Duck",                                                    // Index: 5
		"Idle",                                                    // Index: 6
		"Run",                                                     // Index: 7
		"Victory",                                                 // Index: 8
		"Walk"],                                                   // Index: 9
	activeButton: [
		'#activeAboutButton',                                      // Index: 0
		'#activeNewsButton',                                       // Index: 1
		'#activeCharactersButton',                                 // Index: 2
		'#activeFAQButton',                                        // Index: 3
		'#activeContactButton',                                    // Index: 4
		'#activeLoginButton',                                      // Index: 5
		'.activeHomeButtonImage',                                  // Index: 6
		'#activeAkaneButton',                                      // Index: 7
		'#activeChikaButton',                                      // Index: 8
		'#activeRyuuButton',                                       // Index: 9
		'#activeTaroButton',                                       // Index: 10
		'#activeDownloadButtonImage',                              // Index: 11
		'#activePreviousActionButtonImage',                        // Index: 12
		'#activeNextActionButtonImage',                            // Index: 13
		'.activeDownloadNowButtonImage',                           // Index: 14
		'.activeCancelButtonImage',                                // Index: 15
		'.activeBackToDownloadButtonImage',                        // Index: 16
		'#activeSendButtonImage',                                  // Index: 17
		'#activeEnterButtonImage',                                 // Index: 18
		'#activeRegisterButtonImage',                              // Index: 19
		'#activeLogoutButton',                                     // Index: 20
		'#activeAccountButton',                                    // Index: 21
		'#activeSendRequestButtonImage',                           // Index: 22
		'.activeBackToLoginButtonImage',                           // Index: 23
		'#activeConfirmOwnershipButtonImage',                      // Index: 24
		'.activeBackToStepOneButtonImage',                         // Index: 25
		'#activeResetButtonImage',                                 // Index: 26
		null,                                                      // Index: 27
		null,                                                      // Index: 28
		null,                                                      // Index: 29
		null,                                                      // Index: 30
		null                                                       // Index: 31
	],
	board: [
		'#aboutBoard',                                             // Index: 0
		'#newsBoard',                                              // Index: 1
		'#charactersBoard',                                        // Index: 2
		'#FAQBoard',                                               // Index: 3
		'#contactBoard',                                           // Index: 4
		null,                                                      // Index: 5
		null,                                                      // Index: 6
		null,                                                      // Index: 7
		null,                                                      // Index: 8
		null,                                                      // Index: 9
		null,                                                      // Index: 10
		null,                                                      // Index: 11
		null,                                                      // Index: 12
		null,                                                      // Index: 13
		null,                                                      // Index: 14
		null,                                                      // Index: 15
		null,                                                      // Index: 16
		null,                                                      // Index: 17
		null,                                                      // Index: 18
		null,                                                      // Index: 19
		null,                                                      // Index: 20
		'#accountBoard',                                           // Index: 21
		null,                                                      // Index: 22
		null,                                                      // Index: 23
		null,                                                      // Index: 24
		null,                                                      // Index: 25
		null,                                                      // Index: 26
		null,                                                      // Index: 27
		null,                                                      // Index: 28
		null,                                                      // Index: 29
		'#TOSBoard',                                               // Index: 30
		'#PPBoard'                                                 // Index: 31
	],
	button: [
		'#aboutButton',                                            // Index: 0
		'#newsButton',                                             // Index: 1
		'#charactersButton',                                       // Index: 2
		'#FAQButton',                                              // Index: 3
		'#contactButton',                                          // Index: 4
		'#loginButton',                                            // Index: 5
		'.homeButton',                                             // Index: 6
		'#akaneButton',                                            // Index: 7
		'#chikaButton',                                            // Index: 8
		'#ryuuButton',                                             // Index: 9
		'#taroButton',                                             // Index: 10
		'#downloadButton',                                         // Index: 11
		'#previousActionButton',                                   // Index: 12
		'#nextActionButton',                                       // Index: 13
		'.downloadNowButton',                                      // Index: 14
		'.cancelButton',                                           // Index: 15
		'.backToDownloadButton',                                   // Index: 16
		"#sendButton",                                             // Index: 17
		'#enterButton',                                            // Index: 18
		'#registerButton',                                         // Index: 19
		'#logoutButton',                                           // Index: 20
		'#accountButton',                                          // Index: 21
		'#sendRequestButton',                                      // Index: 22
		'.backToLoginButton',                                      // Index: 23
		'#confirmOwnershipButton',                                 // Index: 24
		'.backToStepOneButton',                                    // Index: 25
		'#resetButton',                                            // Index: 26
		'#accountEmailSubmit',                                     // Index: 27
		'#accountPasswordButton',                                  // Index: 28
		'#downloadFromLibraryButton',                              // Index: 29
		'#TOSbutton',                                              // Index: 30
		'#PPbutton'                                                // Index: 31
	],
	characterName: [
		null,                                                      // Index: 0
		null,                                                      // Index: 1
		null,                                                      // Index: 2
		null,                                                      // Index: 3
		null,                                                      // Index: 4
		null,                                                      // Index: 5
		null,                                                      // Index: 6
		'Akane',                                                   // Index: 7
		'Chika',                                                   // Index: 8
		'Ryuu',                                                    // Index: 9
		'Taro'                                                     // Index: 10
	],
	confirmationForm: [
		'#recoveryCode'                                            // Index: 0
	],
	confirmationErrors: [
		'Wrong!'                                                   // Index: 0
	],
	confirmationKeys: [
		/[A-Z0-9]{6,}$/       // At least six upper case characters and numbers.
	],
	contactForm: [
		'#contactFormEmail',                                       // Index: 0
		'#contactFormSubject',                                     // Index: 1
		'#contactFormTextArea'                                     // Index: 2
	],
	contactKeys: [
		/\w+@+\w+\.+\w/, // Alphanumeric, @, alphanumeric, . and alphanumeric.
		/\w/,            // Alphanumeric (including underscores).
		/\w/             // Alphanumeric (including underscores).
	],
	contactErrors: [
		'That is not a valid e-mail!',                             // Index: 0
		'What can we help you with?',                              // Index: 1
		'Uh? There is no message!'                                 // Index: 3
	],
	faqScroll: [
		'#FAQScrollContainer',                                     // Index: 0
		'#FAQScrollTrack',                                         // Index: 1
		'#FAQScrollThumb',                                         // Index: 2
		'.scrollThumbTopImage',                                    // Index: 3
		'#FAQScrollThumbCenter',                                   // Index: 4
		'.scrollThumbBottomImage',                                 // Index: 5
		'#FAQScrollContent',                                       // Index: 6
		'#FAQContent'                                              // Index: 7
	],
	hoveredButton: [
		'#hoveredAboutButton',                                     // Index: 0
		'#hoveredNewsButton',                                      // Index: 1
		'#hoveredCharactersButton',                                // Index: 2
		'#hoveredFAQButton',                                       // Index: 3
		'#hoveredContactButton',                                   // Index: 4
		'#hoveredLoginButton',                                     // Index: 5
		'.hoveredHomeButtonImage',                                 // Index: 6
		'#hoveredAkaneButton',                                     // Index: 7
		'#hoveredChikaButton',                                     // Index: 8
		'#hoveredRyuuButton',                                      // Index: 9
		'#hoveredTaroButton',                                      // Index: 10
		'#hoveredDownloadButtonImage',                             // Index: 11
		'#hoveredPreviousActionButtonImage',                       // Index: 12
		'#hoveredNextActionButtonImage',                           // Index: 13
		'.hoveredDownloadNowButtonImage',                          // Index: 14
		'.hoveredCancelButtonImage',                               // Index: 15
		'.hoveredBackToDownloadButtonImage',                       // Index: 16
		'#hoveredSendButtonImage',                                 // Index: 17
		'#hoveredEnterButtonImage',                                // Index: 18
		'#hoveredRegisterButtonImage',                             // Index: 19
		'#hoveredLogoutButton',                                    // Index: 20
		'#hoveredAccountButton',                                   // Index: 21
		'#hoveredSendRequestButtonImage',                          // Index: 22
		'.hoveredBackToLoginButtonImage',                          // Index: 23
		'#hoveredConfirmOwnershipButtonImage',                     // Index: 24
		'.hoveredBackToStepOneButtonImage',                        // Index: 25
		'#hoveredResetButtonImage',                                // Index: 26
		null,                                                      // Index: 27
		null,                                                      // Index: 28
		null,                                                      // Index: 29
		null,                                                      // Index: 30
		null                                                       // Index: 31
	],
	isBoardVisible: [
		false,                                                     // Index: 0
		false,                                                     // Index: 1
		false,                                                     // Index: 2
		false,                                                     // Index: 3
		false,                                                     // Index: 4
		false,                                                     // Index: 5
		false,                                                     // Index: 6
		false,                                                     // Index: 7
		false,                                                     // Index: 8
		false,                                                     // Index: 9
		false,                                                     // Index: 10
		false,                                                     // Index: 11
		false,                                                     // Index: 12
		false,                                                     // Index: 13
		false,                                                     // Index: 14
		false,                                                     // Index: 15
		false,                                                     // Index: 16
		false,                                                     // Index: 17
		false,                                                     // Index: 18
		false,                                                     // Index: 19
		false,                                                     // Index: 20
		false,                                                     // Index: 21
		false,                                                     // Index: 22
		false,                                                     // Index: 23
		false,                                                     // Index: 24
		false,                                                     // Index: 25
		false,                                                     // Index: 26
		false,                                                     // Index: 27
		false,                                                     // Index: 28
		false,                                                     // Index: 29
		false,                                                     // Index: 30
		false                                                      // Index: 31
	],
	isButtonActive: [
		false,                                                     // Index: 0
		false,                                                     // Index: 1
		false,                                                     // Index: 2
		false,                                                     // Index: 3
		false,                                                     // Index: 4
		false,                                                     // Index: 5
		false,                                                     // Index: 6
		false,                                                     // Index: 7
		false,                                                     // Index: 8
		false,                                                     // Index: 9
		false,                                                     // Index: 10
		false,                                                     // Index: 11
		false,                                                     // Index: 12
		false,                                                     // Index: 13
		false,                                                     // Index: 14
		false,                                                     // Index: 15
		false,                                                     // Index: 16
		false,                                                     // Index: 17
		false,                                                     // Index: 18
		false,                                                     // Index: 19
		false,                                                     // Index: 20
		false                                                      // Index: 21
	],
	isFormValid: [
		null,      // contactForm.                                 // Index: 0
		null,      // loginForm.                                   // Index: 1
		null,      // registrationForm.                            // Index: 2
		null,      // recoveryForm.                                // Index: 3
		null,      // confirmationForm.                            // Index: 4
		null,      // resetForm.                                   // Index: 5
		null,      // accountFormEmail.                            // Index: 6
		null       // accountPassword.                             // Index: 7
	],
	isProfileVisible: [
		null,                                                      // Index: 0
		null,                                                      // Index: 1
		null,                                                      // Index: 2
		null,                                                      // Index: 3
		null,                                                      // Index: 4
		null,                                                      // Index: 5
		null,                                                      // Index: 6
		false,                                                     // Index: 7
		false,                                                     // Index: 8
		false,                                                     // Index: 9
		false                                                      // Index: 10
	],
	libraryScroll: [
		'#libraryScrollContainer',                                 // Index: 0
		'#libraryScrollTrack',                                     // Index: 1
		'#libraryScrollThumb',                                     // Index: 2
		'.scrollThumbTopImage',                                    // Index: 3
		'#libraryScrollThumbCenter',                               // Index: 5
		'.scrollThumbBottomImage',                                 // Index: 6
		'#libraryScrollContent',                                   // Index: 7
		'#libraryContent'                                          // Index: 8
	],
	loginForm: [
		'#username',                                               // Index: 0
		'#password'                                                // Index: 1
	],
	loginKeys: [
		/[a-zA-Z0-9_]{4,}$/, // At least 8 alphanumerics (equivalent to \w).
		/[a-zA-Z0-9]{8,}$/   // At least 8 alphanumerics (but no underscores).
	],
	loginErrors: [
		'Invalid username!',                                       // Index: 0
		'Invalid password!'                                        // Index: 1
	],
	newsScroll: [
		'#newsScrollContainer',                                    // Index: 0
		'#newsScrollTrack',                                        // Index: 1
		'#newsScrollThumb',                                        // Index: 2
		'.scrollThumbTopImage',                                    // Index: 3
		'#newsScrollThumbCenter',                                  // Index: 4
		'.scrollThumbBottomImage',                                 // Index: 5
		'#newsScrollContent',                                      // Index: 6
		'#newsContent'                                             // Index: 7
	],
	PPScroll: [
		'#PPScrollContainer',                                     // Index: 0
		'#PPScrollTrack',                                         // Index: 1
		'#PPScrollThumb',                                         // Index: 2
		'.scrollThumbTopImage',                                   // Index: 3
		'#PPScrollThumbCenter',                                   // Index: 4
		'.scrollThumbBottomImage',                                // Index: 5
		'#PPScrollContent',                                       // Index: 6
		'#PPContent'                                              // Index: 7
	],
	profile: [
		null,                                                      // Index: 0
		null,                                                      // Index: 1
		null,                                                      // Index: 2
		null,                                                      // Index: 3
		null,                                                      // Index: 4
		null,                                                      // Index: 5
		null,                                                      // Index: 6
		'#akaneProfile',                                           // Index: 7
		'#chikaProfile',                                           // Index: 8
		'#ryuuProfile',                                            // Index: 9
		'#taroProfile'                                             // Index: 10
	],
	registrationForm: [
		'#newUsername',                                            // Index: 0
		'#newEmail',                                               // Index: 1
		'#newPassword',                                            // Index: 2
		'#newPasswordConfirmation'                                 // Index: 3
	],
	recoveryErrors: [
		'That is not a valid e-mail!'                              // Index: 0
	],
	recoveryForm: [
		'#recoveryEamil'                                           // Index: 0
	],
	recoveryKeys: [
		/\w+@+\w+\.+\w/, // Alphanumeric, @, alphanumeric, . and alphanumeric.
	],
	registrationErrors: [
		'Invalid or username already exist!',                      // Index: 0
		'That is not a valid e-mail!',                             // Index: 1
		'Invalid password!',                                       // Index: 2
		"Passwords don't macth!"                                   // Index: 3
	],
	registrationKeys: [
		/[a-zA-Z0-9_]{4,}$/, // At least 8 alphanumerics (equivalent to \w).
		/\w+@+\w+\.+\w/,     // Alphanum., @, alphanum., . and alphanumeric.
		/[a-zA-Z0-9]{8,}$/,  // At least 8 alphanumerics (but no underscores).
		/[a-zA-Z0-9]{8,}$/   // At least 8 alphanumerics (but no underscores).
	],
	resetForm: [
	'#resetPassword',                                              // Index: 0
	'#confirmResetedPassword'                                      // Index: 1
	],
	resetErrors: [
		'Invalid password!',                                       // Index: 0
		"Passwords don't macth!"                                   // Index: 1
	],
	resetKeys: [
		/[a-zA-Z0-9]{8,}$/,  // At least 8 alphanumerics (but no underscores).
		/[a-zA-Z0-9]{8,}$/   // At least 8 alphanumerics (but no underscores).
	],
	TOSScroll: [
		'#TOSScrollContainer',                                     // Index: 0
		'#TOSScrollTrack',                                         // Index: 1
		'#TOSScrollThumb',                                         // Index: 2
		'.scrollThumbTopImage',                                    // Index: 3
		'#TOSScrollThumbCenter',                                   // Index: 4
		'.scrollThumbBottomImage',                                 // Index: 5
		'#TOSScrollContent',                                       // Index: 6
		'#TOSContent'                                              // Index: 7
	],
	updateEmailForm: [
		'#accountFormEmail'                                        // Index: 0
	],
	updateEmailErrors: [
		'That is not a valid e-mail!'                              // Index: 0
	],
	updateEmailKeys: [
		/\w+@+\w+\.+\w/,     // Alphanum., @, alphanum., . and alphanumeric.
	],
	/*--------------------------------------------------------------------------
	CHANGE PASSWORD: Changes user's password when a certain button is clicked by
	submitting the contents of the choosen prompt's input only if the form has
	already been validated by the 'gui.validate()' function. However, remember
	that the real validation is done server-side. 'gui.validate()' is just a
	first layer of protection but it does not provide real protection at all by
	itself.
	
	HOW TO USE: Call the function by passing the index number of the function
	which triggers the action first, the the form where the password and
	password confirmation will be taken from, and lastly, the form's index
	number which will allow us to check if the form has been validated.
	
	Parameters:
	
	buttonIndex - The index button of the button which triggers the action.
	form        - The form where the input will be taken from.
	formIndex   - The form's index number.
	--------------------------------------------------------------------------*/
	changePassword: function (buttonIndex, form, formIndex) {
		var newPassword;
		var newPasswordConfirmation;
		
		$(gui.button[buttonIndex]).click(function() {
			if (gui.isFormValid[formIndex] === true) {
				newPassword = $(form[0]).val();
				newPasswordConfirmation = $(form[1]).val();
				
				$.ajax({
					type:'POST',
					url:'/',
					data: {'id': 'accountRecoveryReset',
						'email': gui.recoveryEmail,
						'password': newPassword,
						'rePassword': newPasswordConfirmation},
					dataType: 'json',
					success: function (json) {
						if (json.newPasswordStatus === 'valid') {
							console.log('Password changed successfully!');
							for (var i = 0; i < form.length; i++) {
								$(form[i]).val('');
							}
							$(gui.prompt).hide();
							gui.prompt = '#accountRecoveryCompleted';
							gui.showPrompt('blink');
							setTimeout(function () {
								gui.showPrompt('close');
								// Also turn "Log in" button off.
								$(gui.activeButton[5]).css('visibility',
									'hidden');
								gui.isButtonActive[5] = false;
							}, 3750);
						} else if (json.newPasswordStatus === 'invalid') {
							console.log('New password is invalid: try again!');
							gui.warnInInput(gui.resetForm[4],
								gui.resetErrors[0]);
						} else if (json.newPasswordStatus ===
							'passwordsDoNotMatch') {
								console.log('Passwords do not match!');
								for (var i = 0; i < form.length; i++) {
									gui.warnInInput(form[i],
										gui.resetErrors[1]);
								}
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log(errorThrown);
					}
				});
			}
		});
	},
	/*--------------------------------------------------------------------------
	CHOOSE PROMPT: Choose what prompt will be shown depending on what the second
	parameter passed in the function is: 'download' or 'login'. If 'download' is
	passed, then choose a prompt depending on whether the active profile is from
	a free or a paid character. If it's paid identify whose character details
	will be displayed. If 'login' is passed as the second parameter, then
	display the "Log In" prompt.
	--------------------------------------------------------------------------*/
	choosePrompt: function (buttonIndex, toBeShown) {
		var isProfileDetected;
		
		$(gui.button[buttonIndex]).click(function() {
			if (toBeShown === 'download') {
				for (var i = 0; i < gui.isProfileVisible.length; i++) {
					if (gui.isProfileVisible[i] === true) {
						isProfileDetected = true;
						break;
					}
				}
				
				if (isProfileDetected === true) {
					$.ajax({
						type:'POST',
						url:'/',
						data: {'id': 'isItFree',
							'characterName': gui.characterName[i]},
						dataType: 'json',
						success: function (json) {
							if (json.isItFree === 'true') {
								gui.prompt = '#freeDownload';
								gui.showPrompt('open');
								$(gui.button[14]).attr('href', json.fileUrl);
								$('#paidDownload>form>input[type=hidden]'
										).remove();
								$('#paidDownload>form').append(json.paypalForm);
							} else {
								// If it's paid content check if the user is
								// logged. Otherwise, ask user to log in.
								console.log(json.isUserLogged);
								if (json.isUserLogged === 'true') {
									// Now check if user already has the item to
									// avoid prompting him/her to buy it again.
									if (json.hasItem === 'false') {
										gui.prompt = '#paidDownload';
										gui.showPrompt('open');
										$('.characterName').empty();
										$('.characterName').append(
											gui.characterName[i]);
										$('#paidDownload>form>input[type=hidden]'
											).remove();
										$('#paidDownload>form').append(
											json.paypalForm);
									} else if (json.hasItem == 'true') {
										gui.prompt = '#ownershipReminder';
										gui.showPrompt('open');
										$(gui.button[14]).attr('href',
											json.fileUrl);
									}
								} else {
									gui.prompt = '#login';
									gui.showPrompt('open');
								}
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							console.log(errorThrown);
						}
					});
				}
			} else if (toBeShown === 'login') {
				gui.prompt = '#login';
			}
		});
	},
	/*--------------------------------------------------------------------------
	CONFIRM OWNERSHIP: Checks whether the user's input matches the recovery code
	it was sent to his e-mail address.
	
	HOW TO USE: Pass the index number of the button which triggers the action
	first and then the form where the recovery code will be taken from. Lastly,
	pass the index number of the validated form.
	
	Parameters:
	
	buttonIndex - The index button of the button which triggers the action.
	form        - The form where the input will be taken from.
	formIndex   - The form's index number.
	--------------------------------------------------------------------------*/
	confirmOwnership: function (buttonIndex, form, formIndex) {
		var recoveryCode; // User's input.
		
		$(gui.button[buttonIndex]).click(function() {
			if (gui.isFormValid[formIndex] === true) {
				recoveryCode = $(form[0]).val();
				
				$.ajax({
					type:'POST',
					url:'/',
					data: {'id': 'accountRecoveryConfirmation',
						'recoveryCode': recoveryCode},
					dataType: 'json',
					success: function (json) {
						if (json.codeStatus === 'valid') {
							console.log('Recovery code matches!');
							$(form[0]).val('');
							$(gui.prompt).hide();
							gui.prompt = '#accountRecoveryReset';
							gui.showPrompt('blink');
						} else if (json.codeStatus === 'invalid') {
							console.log('Recovery code does not match');
							gui.warnInInput(gui.recoveryForm[4],
							gui.confirmationErrors[0]);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log(errorThrown);
					}
				});
			}
		});
	},
	/*--------------------------------------------------------------------------
	DISABLE BUTTONS: Disable buttons when a specified button is clicked after
	a specified amount of time.
		
	HOW TO USE: Pass trigger button's index number first, then the index number
	of the validated form, next, the index numbers of the affected buttons
	and finally, a time delay. If no delay is desired, pass "0"; 
	--------------------------------------------------------------------------*/
	disableButtons: function () {
		var buttonIndex;                   // Trigger button's index number.
		var delay;                         // Time delay to run action. 
		var groupOfButtons = new Array (); // Affected buttons.
		var formIndex;                     // Form's index number.
		buttonIndex = arguments[0];
		formIndex = arguments[1];
		delay = arguments[arguments.length - 1];
		
		for (var i = 2; i < arguments.length - 1; i++) {
			groupOfButtons.push(arguments[i]);
		}
		$(gui.button[buttonIndex]).click(function() {
			if (gui.isFormValid[formIndex] === true) {
				setTimeout(function() {
					for (var i = 0; i < groupOfButtons.length; i++) {
						$(gui.activeButton[i]).css('visibility',
							'hidden');
						gui.isButtonActive[i] = false;
						$(gui.button[i]).hide();
					}
				}, delay);
			}
		});
	},
	/*--------------------------------------------------------------------------
	ENABLE BUTTONS: Enable buttons when a specified button is clicked after
	a specified amount of time.
	
	HOW TO USE: Pass triger button's index number first, then the index
	numbers of the affected buttons and finally, a time delay. If no delay is
	desired, pass "0"; 
	--------------------------------------------------------------------------*/
	enableButtons: function () {
		var buttonIndex;                   // Trigger button's index number.
		var delay;                         // Time delay to run action. 
		var groupOfButtons = new Array (); // Affected buttons.
		buttonIndex = arguments[0];
		delay = arguments[arguments.length - 1];
		
		for (var i = 1; i < arguments.length - 1; i++) {
			groupOfButtons.push(arguments[i]);
		}
		$(gui.button[buttonIndex]).click(function() {
			setTimeout(function() {
				for (var i = 0; i < groupOfButtons.length; i++) {
					$(gui.button[i]).show();
				}
			}, delay);
		});
	},
	/*--------------------------------------------------------------------------
	FLASH BUTTON: Flash button for a determined number of times.
	
	HOW TO USE: Pass the index number of the button to be affected, the duration
	of each step in miliseconds and the number of times the animation should be
	looped.
	
	Parameters:
	
	buttonIndex - Index number of the button to be affected.
	stepInMS    - Duration of each step in miliseconds.
	times       - Number of times to loop the animation.
	--------------------------------------------------------------------------*/
	flashButton: function (buttonIndex, stepInMS, times) {
		var isPlaying = true; // A flag to tell when the animation should be
							  // stopped on mouseover.
		var count = 0;        // The number of times the animations has looped.
		
		// Play the animation loop until it has played the desired number of
		// times.
		$(gui.activeButton[buttonIndex]).css('visibility', 'visible');
		var animation = setInterval(function () {
			if (count < times) {
				$(gui.activeButton[buttonIndex]).fadeTo(stepInMS / 2, 0,
					function () {
						$(this).fadeTo(stepInMS / 2, 1);
						count += 1;
				});
			} else {
				clearInterval(animation);
				$(gui.activeButton[buttonIndex]).fadeTo(stepInMS / 2, 0,
					function () {
						$(this).css('visibility', 'hidden');
						// Return to the original setting.
						$(this).fadeTo(0, 1);
					}
				);
				isPlaying = false;
			}
		}, stepInMS);
		
		// And also stop it if the cursor hovers over the button.
		$(gui.button[buttonIndex]).mouseenter(function () {
			if (isPlaying === true) {
				clearInterval(animation);
				$(gui.activeButton[buttonIndex]).css('visibility', 'hidden');
				// Return to the original setting.
				$(gui.activeButton[buttonIndex]).fadeTo(0, 1);
			}
		});
	},
	/*--------------------------------------------------------------------------
	GO TO FAQ: Go to the FAQ section running all the regular animations as if
	done as usual.
	
	HOW TO USE: Pass the trigger as a parameter.
	
	Parameters:
	
	trigger - The object which will trigger the chain of actions.
	--------------------------------------------------------------------------*/
	goToFAQ: function (trigger) {
		$(trigger).click(function() {
			var boardHeight; // The amount of distance the board moves.
			var characterMenuHeight; // The amount of distance the menu moves.
			var centerX;
			var sWidth;
			var FAQBoardWidth;
			
			FAQBoardWidth = $(gui.board[3]).width();
			sWidth = screen.width;
			centerX = (sWidth - FAQBoardWidth) / 2;
			
			
			$('.blur').hide();
			$('.blur').css('opacity', 0);
			$('.promptContainer').hide();
			$(gui.prompt).hide()
			boardHeight = $('.leftContainer>div>div').height();
			characterMenuHeight = $(gui.characterMenu).height();
			$(gui.board[2]).animate({top: - boardHeight}, 750);
			gui.isBoardVisible[2] = false;
			$(gui.characterMenu).animate({top: - characterMenuHeight}, 750);
			setTimeout(function () {
				$(gui.board[3]).animate({left: centerX});
				$(gui.board[3]).animate({top: 0}, 750);
				gui.isBoardVisible[3] = true;
				$('.leftContainer>div:first-child>div:nth-child(4)>' +
					'div:last-child').hide();
				$('.leftContainer>div:first-child>div:nth-child(4)>' +
					'div:nth-child(4)').show();
			}, 750);
		});
	},	
	/*--------------------------------------------------------------------------
	HIDE BOARD: Hide the current board when an arbitrary button is clicked.
	
	HOW TO USE: Pass the index of the button which will trigger the action 
	first and then all indeces of the boards to be affected by it.
	--------------------------------------------------------------------------*/
	hideBoard: function () {
		var groupOfBoards = new Array (); // Indeces of boards to be affected.
		
		for (var i = 1; i < arguments.length; i++) {
				groupOfBoards.push(arguments[i]);
		}
	
		$(gui.button[arguments[0]]).click(function() {
			for (var i = 0; i < groupOfBoards.length; i++) {
				if (gui.isBoardVisible[groupOfBoards[i]] = true) {
					// Make sure not to store board's height in a variable, so
					// that we can get the current one if browser zoom changes,
					// which in turn changes DOM elements dimensions.
					$(gui.board[groupOfBoards[i]]).animate({top: - 
						$('.leftContainer>div>div').height()}, 750);
					gui.isBoardVisible[groupOfBoards[i]] = false;
				}
			}
		});	
	},	
	/*--------------------------------------------------------------------------
	HIDE CONTACT BOARD: Special function for "Contact" board. Hides the board
	when "send" button is clicked but after some time.
	
	HOW TO USE: Pass the index number of the button which triggers the
	action and the the index number of the board to be affected. Lastly, pass
	the index number of the validated form.
	
	Parameters:
	
	buttonIndex - Index number of the button which triggers the action.
	boardIndex  - Index number of the button to be hidden.
	formIndex   - Form's index number.
	--------------------------------------------------------------------------*/
	hideContactBoard: function (buttonIndex, boardIndex, formIndex) {
		$(gui.button[buttonIndex]).click(function () {
			if (gui.isFormValid[formIndex] === true) {
				setTimeout(function () {
					// Make sure not to store board's height in a variable,
					// so that we can get the current one if browser zoom
					// changes, which in turn changes DOM elements dimensions.
					$(gui.board[boardIndex]).animate({top: -
						$(gui.board[boardIndex]).height()}, 750);
					gui.isBoardVisible[boardIndex] = false;
				}, 3000);
			}
		});
	},
	/*--------------------------------------------------------------------------
	HIDE PRONPT: Hide prompt when an arbitrary button is
	clicked.
	
	HOW TO USE: Pass the index number of the button which triggers the
	action.
	
	Parameters:
	
	buttonIndex - Index number of the button which triggers the action.
	--------------------------------------------------------------------------*/
	hidePrompt: function (buttonIndex) {
		$(gui.button[buttonIndex]).click(function() {
			
			// Hide both, blur and prompt.
			$('.blur').hide();
			$('.blur').css('opacity', 0);
			$('.promptContainer').hide();
			$(gui.prompt).hide();
			
			// Enable clicking on main menu buttons.
			for (var i = 0; i < 22 && i > 13 && i < 16 && i != 18 &&
				i != 19; i++) {
					$(gui.button[i]).css('pointer-events', 'none');
			}
			
			// Enable clicking on TOS and PP buttons.
			for (var i = 30; i < 32; i++) {
				$(gui.button[i]).css('pointer-events', 'auto');
			}
			
			// Change cursor to default for TOS and PP buttons.
			for (var i = 30; i < 32; i++) {
				$(gui.button[i]).css('cursor', 'pointer');
			}
		});
	},
	/*--------------------------------------------------------------------------
	LOAD IMAGES: Loads and assign images to DOM elements depending on screen
	size.
	
	HOW TO USE: Just invoke. 
	--------------------------------------------------------------------------*/
	loadImages: function () {
		var sWidth = screen.width; // Takes screen width on startup.
		var sHeight = screen.height; // Takes screen height on startup.
//		var sWidth = $(window).width(); // Takes Firefox's window width.
//		var sHeight = $(window).height(); // Takes Firefox's window height.
		var scaleRatio = sHeight * 100 / 1600 / 100; // Image scale converter.
		var aRatio = (sWidth / sHeight).toString().substr(0, 4); // Asp. ratio.
		var promptHandleSize; // Calculated according to resolution.
		var promptPaperSize;  // Calculated according to resolution.
		var fontSize;           // Calculated fontSize according to resolution.
		
		// Set images according to aspect ratio.
		// Values for other resolutions:
		// 4:3 = 1.33/1.25, 16:10 = 1.6, 16:9 = 1.77
		switch (aRatio) {
		case "1.25":
		
		case "1.33":
			// Variables: the first two hold rail div and wrapper.
			var lHolder = parseInt(1206 * scaleRatio); 
			var rHolder = parseInt(374 * scaleRatio);
			var lMargin = parseInt((1206 + 120) * scaleRatio - lHolder);
			var rMargin = parseInt((374 + 120) * scaleRatio - rHolder);
			var lrHWidth = lHolder + lMargin + rHolder + rMargin;
			
			// Save the minimal width value in which the gui fit into the window
			// into a global variable to be used later for displaying or not
			// browser's horizontal scrollbar.
			gui.minWidth = lrHWidth;
			
			// Fixed div size assignment (screen resolution dependent).
			$(".floatContainer").css("min-width", lrHWidth);
			$(".backgroundCanvasContainer").css("min-width", lrHWidth);
			$(".leftContainer").css("width", (lHolder + lMargin) * 100 /
				sWidth + "%");
			$(".leftContainer").css("min-width", lHolder + lMargin / 2);
			$(".leftContainer>div").css("width", lHolder);
			$(".leftContainer>div>div").css("width", lHolder);
			$(".rightContainer").css("width", (rHolder + rMargin) * 100 /
				sWidth +　"%");
			$(".rightContainer").css("min-width", rHolder + rMargin / 2);
			$(".rightContainer>div").css("width", rHolder);
			$(".rightContainer>div>div>div").css("width", rHolder);
			
			// Image assignment.
			$("body").attr("background", staticUrl + "art/img/bkg/stars.gif");
			$(".boardChainsImage").attr("src",
				staticUrl + "art/img/dcl/boardChainsDecal_4-3.png");
			$("#aboutBoardImage").attr("src",
				staticUrl + "art/img/dcl/aboutBoardDecal_4-3.png");
			$("#newsBoardImage").attr("src",
				staticUrl + "art/img/dcl/newsBoardDecal_4-3.png");
			$(".charactersBoardImage").attr("src",
				staticUrl + "art/img/dcl/characterProfileDecal_4-3.png");
			$("#FAQBoardImage").attr("src",
				staticUrl + "art/img/dcl/FAQBoardDecal_4-3.png");
			$("#TOSBoardImage").attr("src",
				staticUrl + "art/img/dcl/TOSBoardDecal_4-3.png");
			$("#PPBoardImage").attr("src",
				staticUrl + "art/img/dcl/PPBoardDecal_4-3.png");
			$("#contactBoardImage").attr("src",
				staticUrl + "art/img/dcl/contactBoardDecal_4-3.png");
			$("#accountBoardImage").attr("src",
				staticUrl + "art/img/dcl/accountBoardDecal_4-3.png");
			$(".mainMenuChains").attr("src",
				staticUrl + "art/img/dcl/mainMenuChainsDecal.png");
			$(".characterMenuChains").attr("src",
				staticUrl + "art/img/dcl/characterMenuChainsDecal.png");
			$("#aboutButtonImage").attr("src",
				staticUrl + "art/img/btn/normalAboutButton.png");
			$("#newsButtonImage").attr("src",
				staticUrl + "art/img/btn/normalNewsButton.png");
			$("#charactersButtonImage").attr("src",
				staticUrl + "art/img/btn/normalCharactersButton.png");
			$("#FAQButtonImage").attr("src",
				staticUrl + "art/img/btn/normalFAQButton.png");
			$("#contactButtonImage").attr("src",
				staticUrl + "art/img/btn/normalContactButton.png");
			$("#loginButtonImage").attr("src",
				staticUrl + "art/img/btn/normalLoginButton.png");
			$("#logoutButtonImage").attr("src",
				staticUrl + "art/img/btn/normalLogoutButton.png");
			$("#accountButtonImage").attr("src",
			staticUrl + "art/img/btn/normalAccountButton.png");
			$("#hoveredAboutButton").attr("src",
				staticUrl + "art/img/btn/hoveredAboutButton.png");
			$("#hoveredNewsButton").attr("src",
				staticUrl + "art/img/btn/hoveredNewsButton.png");
			$("#hoveredCharactersButton").attr("src",
				staticUrl + "art/img/btn/hoveredCharactersButton.png");
			$("#hoveredFAQButton").attr("src",
				staticUrl + "art/img/btn/hoveredFAQButton.png");
			$("#hoveredContactButton").attr("src",
				staticUrl + "art/img/btn/hoveredContactButton.png");
			$("#hoveredLoginButton").attr("src",
				staticUrl + "art/img/btn/hoveredLoginButton.png");
			$("#hoveredLogoutButton").attr("src",
				staticUrl + "art/img/btn/hoveredLogoutButton.png");
			$("#hoveredAccountButton").attr("src",
				staticUrl + "art/img/btn/hoveredAccountButton.png");
			$("#activeAboutButton").attr("src",
				staticUrl + "art/img/btn/activeAboutButton.png");
			$("#activeNewsButton").attr("src",
				staticUrl + "art/img/btn/activeNewsButton.png");
			$("#activeCharactersButton").attr("src",
				staticUrl + "art/img/btn/activeCharactersButton.png");
			$("#activeFAQButton").attr("src",
				staticUrl + "art/img/btn/activeFAQButton.png");
			$("#activeContactButton").attr("src",
				staticUrl + "art/img/btn/activeContactButton.png");
			$("#activeLoginButton").attr("src",
				staticUrl + "art/img/btn/activeLoginButton.png");
			$("#activeLogoutButton").attr("src",
				staticUrl + "art/img/btn/activeLogoutButton.png");
			$("#activeAccountButton").attr("src",
				staticUrl + "art/img/btn/activeAccountButton.png");
			$("#normalAkaneButton").attr("src",
				staticUrl + "art/img/btn/normalAkaneButton.png");
			$("#normalChikaButton").attr("src",
				staticUrl + "art/img/btn/normalChikaButton.png");
			$("#normalRyuuButton").attr("src",
				staticUrl + "art/img/btn/normalRyuuButton.png");
			$("#normalTaroButton").attr("src",
				staticUrl + "art/img/btn/normalTaroButton.png");
			$("#hoveredAkaneButton").attr("src",
				staticUrl + "art/img/btn/hoveredAkaneButton.png");
			$("#hoveredChikaButton").attr("src",
				staticUrl + "art/img/btn/hoveredChikaButton.png");
			$("#hoveredRyuuButton").attr("src",
				staticUrl + "art/img/btn/hoveredRyuuButton.png");
			$("#hoveredTaroButton").attr("src",
				staticUrl + "art/img/btn/hoveredTaroButton.png");
			$("#activeAkaneButton").attr("src",
				staticUrl + "art/img/btn/activeAkaneButton.png");
			$("#activeChikaButton").attr("src",
				staticUrl + "art/img/btn/activeChikaButton.png");
			$("#activeRyuuButton").attr("src",
				staticUrl + "art/img/btn/activeRyuuButton.png");
			$("#activeTaroButton").attr("src",
				staticUrl + "art/img/btn/activeTaroButton.png");
			$(".normalHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/normalHomeButton_4-3.png");
			$(".hoveredHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredHomeButton_4-3.png");
			$(".activeHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/activeHomeButton_4-3.png");
			$("#normalPreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/normalPreviousActionButton_4-3.png");
			$("#normalNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/normalNextActionButton_4-3.png");
			$("#hoveredPreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredPreviousActionButton_4-3.png");
			$("#hoveredNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredNextActionButton_4-3.png");
			$("#activePreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/activePreviousActionButton_4-3.png");
			$("#activeNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/activeNextActionButton_4-3.png");
			$(".scrollThumbTopImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbTop.png");
			$(".scrollThumbCenterImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbCenter.png");
			$(".scrollThumbBottomImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbBottom.png");
			$("#libraryScrollThumbTop").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbTop.png");
			$("#libraryScrollThumbCenter").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbCenter.png");
			$("#libraryScrollThumbBottom").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbBottom.png");
			$("#normalDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/normalDownloadButton_4-3.png");
			$("#hoveredDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredDownloadButton_4-3.png");
			$("#activeDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/activeDownloadButton_4-3.png");
			$("#promptPaperImage").attr("src",
				staticUrl + "art/img/dcl/promptPaperDecal.png");
			$(".promptHandleImage").attr("src",
				staticUrl + "art/img/dcl/promptHandleDecal.png");
			$(".normalDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/normalDownloadNowButton.png");
			$(".hoveredDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredDownloadNowButton.png");
			$(".activeDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/activeDownloadNowButton.png");
			$(".normalCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/normalCancelButton.png");
			$(".hoveredCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredCancelButton.png");
			$(".activeCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/activeCancelButton.png");
			$("#normalEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/normalEnterButton.png");
			$("#hoveredEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredEnterButton.png");
			$("#activeEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/activeEnterButton.png");
			$("#normalRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/normalRegisterButton.png");
			$("#hoveredRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredRegisterButton.png");
			$("#activeRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/activeRegisterButton.png");
			$(".normalBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToDownloadButton_4-3.png");
			$(".hoveredBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToDownloadButton_4-3.png");
			$(".activeBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToDownloadButton_4-3.png");
			$("#normalSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/normalSendRequestButton.png");
			$("#hoveredSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredSendRequestButton.png");
			$("#activeSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/activeSendRequestButton.png");
			$(".normalBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToLoginButton.png");
			$(".hoveredBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToLoginButton.png");
			$(".activeBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToLoginButton.png");
			$("#normalSendButtonImage").attr("src",
				staticUrl + "art/img/btn/normalSendButton_4-3.png");
			$("#hoveredSendButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredSendButton_4-3.png");
			$("#activeSendButtonImage").attr("src",
				staticUrl + "art/img/btn/activeSendButton_4-3.png");
			$("#normalConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/normalConfirmOwnershipButton.png");
			$("#hoveredConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredConfirmOwnershipButton.png");
			$("#activeConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/activeConfirmOwnershipButton.png");
			$(".normalBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToStepOneButton.png");
			$(".hoveredBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToStepOneButton.png");
			$(".activeBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToStepOneButton.png");
			$("#normalResetButtonImage").attr("src",
				staticUrl + "art/img/btn/normalResetButton.png");
			$("#hoveredResetButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredResetButton.png");
			$("#activeResetButtonImage").attr("src",
				staticUrl + "art/img/btn/activeResetButton.png");
			$("#glowImage").attr("src", staticUrl + "art/img/bkg/glow.png");
			$("#viewerCanvasContainer").css("background-image",
				"url(" + staticUrl + "art/img/dcl/viewerBackground.png)");
			
			// CSS rules.
			// Font size.
			fontSize = sWidth * 0.01;
			$('html').css('font-size', fontSize);
			
			// Text in "About" board.
			$(".leftContainer>div:first-child>div:nth-child(1)>" +
				"p:nth-child(3)").css("top", "24%");
			$(".leftContainer>div:first-child>div:nth-child(1)>" +
				"p:nth-child(5)").css("top", "68%");
			
			// Scroll track's left property.
			$(".leftContainer>div:first-child>div>.scrollContainer>" +
				"div:first-child").css("left", "84%");
			
			// "Character action" buttons' left property.
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(8)>div:nth-child(8)").css("left", "12%");
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(8)>div:nth-child(9)").css("left", "43%");
			
			// "Download" button' margin-left, left and width properties.
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(9)>div").css("margin-left", "-13%");
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(9)>div").css("left", "79%");
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(9)>div").css("width", "26%");
			break;
		
		case "1.6":
			//Variables: the first two hold rail div and wrapper.
			var lHolder = parseInt(1445 * scaleRatio);
			var rHolder = parseInt(374 * scaleRatio);
			var lMargin = parseInt((1445 + 120) * scaleRatio - lHolder);
			var rMargin = parseInt((374 + 120) * scaleRatio - rHolder);
			var lrHWidth = lHolder + lMargin + rHolder + rMargin + 45;
			
			// Save the minimal width value in which the gui fit into the window
			// into a global variable to be used later for displaying or not
			// browser's horizontal scrollbar.
			gui.minWidth = lrHWidth;
			
			//Fixed size assignment (screen resolution dependent).
			$(".floatContainer").css("min-width", lrHWidth);
			$(".backgroundCanvasContainer").css("min-width", lrHWidth);
			$(".leftContainer").css("width",  (lHolder + lMargin) * 100 /
				sWidth + "%");
			$(".leftContainer").css("min-width", lHolder + lMargin / 2);
			$(".leftContainer>div").css("width", lHolder);
			$(".leftContainer>div>div>div").css("width", lHolder);
			$(".rightContainer").css("width", (rHolder + rMargin) * 100 /
				sWidth + "%");
			$(".rightContainer").css("min-width", rHolder + rMargin / 2);
			$(".rightContainer>div").css("width", rHolder);
			$(".rightContainer>div>div>div").css("width", rHolder);
			
			//Image assignment
			$("body").attr("background", staticUrl + "art/img/bkg/stars.gif");
			$(".boardChainsImage").attr("src",
				staticUrl + "art/img/dcl/boardChainsDecal.png");
			$("#aboutBoardImage").attr("src",
				staticUrl + "art/img/dcl/aboutBoardDecal.png");
			$("#newsBoardImage").attr("src",
				staticUrl + "art/img/dcl/newsBoardDecal.png");
			$(".charactersBoardImage").attr("src",
				staticUrl + "art/img/dcl/characterProfileDecal.png");
			$("#FAQBoardImage").attr("src",
				staticUrl + "art/img/dcl/FAQBoardDecal.png");
			$("#TOSBoardImage").attr("src",
				staticUrl + "art/img/dcl/TOSBoardDecal.png");
			$("#PPBoardImage").attr("src",
				staticUrl + "art/img/dcl/PPBoardDecal.png");
			$("#contactBoardImage").attr("src",
				staticUrl + "art/img/dcl/contactBoardDecal.png");
			$("#accountBoardImage").attr("src",
				staticUrl + "art/img/dcl/accountBoardDecal.png");
			$(".mainMenuChains").attr("src",
				staticUrl + "art/img/dcl/mainMenuChainsDecal.png");
			$(".characterMenuChains").attr("src",
				staticUrl + "art/img/dcl/characterMenuChainsDecal.png");
			$("#aboutButtonImage").attr("src",
				staticUrl + "art/img/btn/normalAboutButton.png");
			$("#newsButtonImage").attr("src",
				staticUrl + "art/img/btn/normalNewsButton.png");
			$("#charactersButtonImage").attr("src",
				staticUrl + "art/img/btn/normalCharactersButton.png");
			$("#FAQButtonImage").attr("src",
				staticUrl + "art/img/btn/normalFAQButton.png");
			$("#contactButtonImage").attr("src",
				staticUrl + "art/img/btn/normalContactButton.png");
			$("#loginButtonImage").attr("src",
				staticUrl + "art/img/btn/normalLoginButton.png");
			$("#logoutButtonImage").attr("src",
				staticUrl + "art/img/btn/normalLogoutButton.png");
			$("#accountButtonImage").attr("src",
				staticUrl + "art/img/btn/normalAccountButton.png");
			$("#imgCharacterMenuChains").attr("src",
				staticUrl + "art/img/dcl/menuChainsDecal.png");
			$("#hoveredAboutButton").attr("src",
				staticUrl + "art/img/btn/hoveredAboutButton.png");
			$("#hoveredNewsButton").attr("src",
				staticUrl + "art/img/btn/hoveredNewsButton.png");
			$("#hoveredCharactersButton").attr("src",
				staticUrl + "art/img/btn/hoveredCharactersButton.png");
			$("#hoveredFAQButton").attr("src",
				staticUrl + "art/img/btn/hoveredFAQButton.png");
			$("#hoveredContactButton").attr("src",
				staticUrl + "art/img/btn/hoveredContactButton.png");
			$("#hoveredLoginButton").attr("src",
				staticUrl + "art/img/btn/hoveredLoginButton.png");
			$("#hoveredLogoutButton").attr("src",
				staticUrl + "art/img/btn/hoveredLogoutButton.png");
			$("#hoveredAccountButton").attr("src",
				staticUrl + "art/img/btn/hoveredAccountButton.png");
			$("#activeAboutButton").attr("src",
				staticUrl + "art/img/btn/activeAboutButton.png");
			$("#activeNewsButton").attr("src",
				staticUrl + "art/img/btn/activeNewsButton.png");
			$("#activeCharactersButton").attr("src",
				staticUrl + "art/img/btn/activeCharactersButton.png");
			$("#activeFAQButton").attr("src",
				staticUrl + "art/img/btn/activeFAQButton.png");
			$("#activeContactButton").attr("src",
				staticUrl + "art/img/btn/activeContactButton.png");
			$("#activeLoginButton").attr("src",
				staticUrl + "art/img/btn/activeLoginButton.png");
			$("#activeLogoutButton").attr("src",
				staticUrl + "art/img/btn/activeLogoutButton.png");
			$("#activeAccountButton").attr("src",
				staticUrl + "art/img/btn/activeAccountButton.png");
			$("#normalAkaneButton").attr("src",
				staticUrl + "art/img/btn/normalAkaneButton.png");
			$("#normalChikaButton").attr("src",
				staticUrl + "art/img/btn/normalChikaButton.png");
			$("#normalRyuuButton").attr("src",
				staticUrl + "art/img/btn/normalRyuuButton.png");
			$("#normalTaroButton").attr("src",
				staticUrl + "art/img/btn/normalTaroButton.png");
			$("#hoveredAkaneButton").attr("src",
				staticUrl + "art/img/btn/hoveredAkaneButton.png");
			$("#hoveredChikaButton").attr("src",
				staticUrl + "art/img/btn/hoveredChikaButton.png");
			$("#hoveredRyuuButton").attr("src",
				staticUrl + "art/img/btn/hoveredRyuuButton.png");
			$("#hoveredTaroButton").attr("src",
				staticUrl + "art/img/btn/hoveredTaroButton.png");
			$("#activeAkaneButton").attr("src",
				staticUrl + "art/img/btn/activeAkaneButton.png");
			$("#activeChikaButton").attr("src",
				staticUrl + "art/img/btn/activeChikaButton.png");
			$("#activeRyuuButton").attr("src",
				staticUrl + "art/img/btn/activeRyuuButton.png");
			$("#activeTaroButton").attr("src",
				staticUrl + "art/img/btn/activeTaroButton.png");
			$(".normalHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/normalHomeButton.png");
			$(".hoveredHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredHomeButton.png");
			$(".activeHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/activeHomeButton.png");
			$("#normalPreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/normalPreviousActionButton.png");
			$("#normalNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/normalNextActionButton.png");
			$("#hoveredPreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredPreviousActionButton.png");
			$("#hoveredNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredNextActionButton.png");
			$("#activePreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/activePreviousActionButton.png");
			$("#activeNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/activeNextActionButton.png");
			$(".scrollThumbTopImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbTop.png");
			$(".scrollThumbCenterImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbCenter.png");
			$(".scrollThumbBottomImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbBottom.png");
			$("#libraryScrollThumbTop").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbTop.png");
			$("#libraryScrollThumbCenter").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbCenter.png");
			$("#libraryScrollThumbBottom").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbBottom.png");
			$("#normalDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/normalDownloadButton.png");
			$("#hoveredDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredDownloadButton.png");
			$("#activeDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/activeDownloadButton.png");
			$("#promptPaperImage").attr("src",
				staticUrl + "art/img/dcl/promptPaperDecal.png");
			$(".promptHandleImage").attr("src",
				staticUrl + "art/img/dcl/promptHandleDecal.png");
			$(".normalDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/normalDownloadNowButton.png");
			$(".hoveredDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredDownloadNowButton.png");
			$(".activeDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/activeDownloadNowButton.png");
			$(".normalCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/normalCancelButton.png");
			$(".hoveredCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredCancelButton.png");
			$(".activeCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/activeCancelButton.png");
			$("#normalEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/normalEnterButton.png");
			$("#hoveredEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredEnterButton.png");
			$("#activeEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/activeEnterButton.png");
			$("#normalRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/normalRegisterButton.png");
			$("#hoveredRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredRegisterButton.png");
			$("#activeRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/activeRegisterButton.png");
			$(".normalBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToDownloadButton.png");
			$(".hoveredBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToDownloadButton.png");
			$(".activeBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToDownloadButton.png");
			$("#normalSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/normalSendRequestButton.png");
			$("#hoveredSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredSendRequestButton.png");
			$("#activeSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/activeSendRequestButton.png");
			$(".normalBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToLoginButton.png");
			$(".hoveredBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToLoginButton.png");
			$(".activeBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToLoginButton.png");
			$("#normalSendButtonImage").attr("src",
				staticUrl + "art/img/btn/normalSendButton.png");
			$("#hoveredSendButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredSendButton.png");
			$("#activeSendButtonImage").attr("src",
				staticUrl + "art/img/btn/activeSendButton.png");
			$("#normalConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/normalConfirmOwnershipButton.png");
			$("#hoveredConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredConfirmOwnershipButton.png");
			$("#activeConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/activeConfirmOwnershipButton.png");
			$(".normalBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToStepOneButton.png");
			$(".hoveredBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToStepOneButton.png");
			$(".activeBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToStepOneButton.png");
			$("#normalResetButtonImage").attr("src",
				staticUrl + "art/img/btn/normalResetButton.png");
			$("#hoveredResetButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredResetButton.png");
			$("#activeResetButtonImage").attr("src",
				staticUrl + "art/img/btn/activeResetButton.png");
			$("#glowImage").attr("src", staticUrl + "art/img/bkg/glow.png");
			$("#viewerCanvasContainer").css("background-image",
				"url(" + staticUrl + "art/img/dcl/viewerBackground.png)");
			
			// No need to modify css rules for this aspect radio. Use the ones
			// already set.		
			// Font size.
			fontSize = sWidth * 0.01;
			$('html').css('font-size', fontSize);
			break;
		
		case "1.77":
			//Variables: the first two hold rail div and wrapper.
			var lHolder = parseInt(1445 * scaleRatio);
			var rHolder = parseInt(374 * scaleRatio);
			var lMargin = parseInt((1445 + 120) * scaleRatio - lHolder);
			var rMargin = parseInt((374 + 120) * scaleRatio - lMargin);
			var lrHWidth = lHolder + lMargin + rHolder + rMargin + 45;
			
			// Save the minimal width value in which the gui fit into the window
			// into a global variable to be used later for displaying or not
			// browser's horizontal scrollbar.
			gui.minWidth = lrHWidth;
			
			//Fixed size assignment (screen resolution dependent).
			$('.floatContainer').css("min-width", lrHWidth);
			$(".backgroundCanvasContainer").css("min-width", lrHWidth);
			$(".leftContainer").css("width", (lHolder + lMargin) * 100 /
				sWidth + "%");
			$(".leftContainer").css("min-width", lHolder + lMargin / 2);
			$(".leftContainer>div").css("width", lHolder);
			$(".leftContainer>div>div>div").css("width", lHolder);
			$(".rightContainer").css("width", (rHolder + rMargin) * 100 /
				sWidth + "%");
			$(".rightContainer").css("min-width", rHolder + rMargin / 2);
			$(".rightContainer>div").css("width", rHolder);
			$(".rightContainer>div>div>div").css("width", rHolder);
			
			// Image assignment.
			$("body").attr("background", staticUrl + "art/img/bkg/stars.gif");
			$(".boardChainsImage").attr("src",
				staticUrl + "art/img/dcl/boardChainsDecal_16-9.png");
			$("#aboutBoardImage").attr("src",
				staticUrl + "art/img/dcl/aboutBoardDecal_16-9.png");
			$("#newsBoardImage").attr("src",
				staticUrl + "art/img/dcl/newsBoardDecal_16-9.png");
			$(".charactersBoardImage").attr("src",
				staticUrl + "art/img/dcl/characterProfileDecal_16-9.png");
			$("#FAQBoardImage").attr("src",
				staticUrl + "art/img/dcl/FAQBoardDecal_16-9.png");
			$("#TOSBoardImage").attr("src",
				staticUrl + "art/img/dcl/TOSBoardDecal_16-9.png");
			$("#PPBoardImage").attr("src",
				staticUrl + "art/img/dcl/PPBoardDecal_16-9.png");
			$("#contactBoardImage").attr("src",
				staticUrl + "art/img/dcl/contactBoardDecal_16-9.png");
			$("#accountBoardImage").attr("src",
				staticUrl + "art/img/dcl/accountBoardDecal_16-9.png");
			$(".mainMenuChains").attr("src",
				staticUrl + "art/img/dcl/mainMenuChainsDecal.png");
			$(".characterMenuChains").attr("src",
				staticUrl + "art/img/dcl/characterMenuChainsDecal.png");
			$("#aboutButtonImage").attr("src",
				staticUrl + "art/img/btn/normalAboutButton.png");
			$("#newsButtonImage").attr("src",
				staticUrl + "art/img/btn/normalNewsButton.png");
			$("#charactersButtonImage").attr("src",
				staticUrl + "art/img/btn/normalCharactersButton.png");
			$("#FAQButtonImage").attr("src",
				staticUrl + "art/img/btn/normalFAQButton.png");
			$("#contactButtonImage").attr("src",
				staticUrl + "art/img/btn/normalContactButton.png");
			$("#loginButtonImage").attr("src",
				staticUrl + "art/img/btn/normalLoginButton.png");
			$("#logoutButtonImage").attr("src",
				staticUrl + "art/img/btn/normalLogoutButton.png");
			$("#accountButtonImage").attr("src",
				staticUrl + "art/img/btn/normalAccountButton.png");
			$("#hoveredAboutButton").attr("src",
				staticUrl + "art/img/btn/hoveredAboutButton.png");
			$("#hoveredNewsButton").attr("src",
				staticUrl + "art/img/btn/hoveredNewsButton.png");
			$("#hoveredCharactersButton").attr("src",
				staticUrl + "art/img/btn/hoveredCharactersButton.png");
			$("#hoveredFAQButton").attr("src",
				staticUrl + "art/img/btn/hoveredFAQButton.png");
			$("#hoveredContactButton").attr("src",
				staticUrl + "art/img/btn/hoveredContactButton.png");
			$("#hoveredLoginButton").attr("src",
				staticUrl + "art/img/btn/hoveredLoginButton.png");
			$("#hoveredLogoutButton").attr("src",
				staticUrl + "art/img/btn/hoveredLogoutButton.png");
			$("#activeAboutButton").attr("src",
				staticUrl + "art/img/btn/activeAboutButton.png");
			$("#activeNewsButton").attr("src",
				staticUrl + "art/img/btn/activeNewsButton.png");
			$("#activeCharactersButton").attr("src",
				staticUrl + "art/img/btn/activeCharactersButton.png");
			$("#activeFAQButton").attr("src",
				staticUrl + "art/img/btn/activeFAQButton.png");
			$("#activeContactButton").attr("src",
				staticUrl + "art/img/btn/activeContactButton.png");
			$("#activeLoginButton").attr("src",
				staticUrl + "art/img/btn/activeLoginButton.png");
			$("#activeLogoutButton").attr("src",
				staticUrl + "art/img/btn/activeLogoutButton.png");
			$("#activeAccountButton").attr("src",
				staticUrl + "art/img/btn/activeAccountButton.png");
			$("#normalAkaneButton").attr("src",
				staticUrl + "art/img/btn/normalAkaneButton.png");
			$("#normalChikaButton").attr("src",
				staticUrl + "art/img/btn/normalChikaButton.png");
			$("#normalRyuuButton").attr("src",
				staticUrl + "art/img/btn/normalRyuuButton.png");
			$("#normalTaroButton").attr("src",
				staticUrl + "art/img/btn/normalTaroButton.png");
			$("#hoveredAkaneButton").attr("src",
				staticUrl + "art/img/btn/hoveredAkaneButton.png");
			$("#hoveredChikaButton").attr("src",
				staticUrl + "art/img/btn/hoveredChikaButton.png");
			$("#hoveredRyuuButton").attr("src",
				staticUrl + "art/img/btn/hoveredRyuuButton.png");
			$("#hoveredTaroButton").attr("src",
				staticUrl + "art/img/btn/hoveredTaroButton.png");
			$("#activeAkaneButton").attr("src",
				staticUrl + "art/img/btn/activeAkaneButton.png");
			$("#activeChikaButton").attr("src",
				staticUrl + "art/img/btn/activeChikaButton.png");
			$("#activeRyuuButton").attr("src",
				staticUrl + "art/img/btn/activeRyuuButton.png");
			$("#activeTaroButton").attr("src",
				staticUrl + "art/img/btn/activeTaroButton.png");
			$(".normalHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/normalHomeButton.png");
			$(".hoveredHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredHomeButton.png");
			$(".activeHomeButtonImage").attr("src",
				staticUrl + "art/img/btn/activeHomeButton.png");
			$("#normalPreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/normalPreviousActionButton.png");
			$("#normalNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/normalNextActionButton.png");
			$("#hoveredPreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredPreviousActionButton.png");
			$("#hoveredNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredNextActionButton.png");
			$("#activePreviousActionButtonImage").attr("src",
				staticUrl + "art/img/btn/activePreviousActionButton.png");
			$("#activeNextActionButtonImage").attr("src",
				staticUrl + "art/img/btn/activeNextActionButton.png");
			$(".scrollThumbTopImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbTop.png");
			$(".scrollThumbCenterImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbCenter.png");
			$(".scrollThumbBottomImage").attr("src",
				staticUrl + "art/img/btn/scrollThumbBottom.png");
			$("#libraryScrollThumbTop").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbTop.png");
			$("#libraryScrollThumbCenter").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbCenter.png");
			$("#libraryScrollThumbBottom").attr("src",
				staticUrl + "art/img/btn/boardScrollThumbBottom.png");
			$("#normalDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/normalDownloadButton.png");
			$("#hoveredDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredDownloadButton.png");
			$("#activeDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/activeDownloadButton.png");
			$("#promptPaperImage").attr("src",
				staticUrl + "art/img/dcl/promptPaperDecal.png");
			$(".promptHandleImage").attr("src",
				staticUrl + "art/img/dcl/promptHandleDecal.png");
			$(".normalDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/normalDownloadNowButton.png");
			$(".hoveredDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredDownloadNowButton.png");
			$(".activeDownloadNowButtonImage").attr("src",
				staticUrl + "art/img/btn/activeDownloadNowButton.png");
			$(".normalCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/normalCancelButton.png");
			$(".hoveredCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredCancelButton.png");
			$(".activeCancelButtonImage").attr("src",
				staticUrl + "art/img/btn/activeCancelButton.png");
			$("#normalEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/normalEnterButton.png");
			$("#hoveredEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredEnterButton.png");
			$("#activeEnterButtonImage").attr("src",
				staticUrl + "art/img/btn/activeEnterButton.png");
			$("#normalRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/normalRegisterButton.png");
			$("#hoveredRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredRegisterButton.png");
			$("#activeRegisterButtonImage").attr("src",
				staticUrl + "art/img/btn/activeRegisterButton.png");
			$(".normalBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToDownloadButton.png");
			$(".hoveredBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToDownloadButton.png");
			$(".activeBackToDownloadButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToDownloadButton.png");
			$("#normalSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/normalSendRequestButton.png");
			$("#hoveredSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredSendRequestButton.png");
			$("#activeSendRequestButtonImage").attr("src",
				staticUrl + "art/img/btn/activeSendRequestButton.png");
			$(".normalBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToLoginButton.png");
			$(".hoveredBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToLoginButton.png");
			$(".activeBackToLoginButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToLoginButton.png");
			$("#normalSendButtonImage").attr("src",
				staticUrl + "art/img/btn/normalSendButton.png");
			$("#hoveredSendButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredSendButton.png");
			$("#activeSendButtonImage").attr("src",
				staticUrl + "art/img/btn/activeSendButton.png");
			$("#normalConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/normalConfirmOwnershipButton.png");
			$("#hoveredConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredConfirmOwnershipButton.png");
			$("#activeConfirmOwnershipButtonImage").attr("src",
				staticUrl + "art/img/btn/activeConfirmOwnershipButton.png");
			$(".normalBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/normalBackToStepOneButton.png");
			$(".hoveredBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredBackToStepOneButton.png");
			$(".activeBackToStepOneButtonImage").attr("src",
				staticUrl + "art/img/btn/activeBackToStepOneButton.png");
			$("#normalResetButtonImage").attr("src",
				staticUrl + "art/img/btn/normalResetButton.png");
			$("#hoveredResetButtonImage").attr("src",
				staticUrl + "art/img/btn/hoveredResetButton.png");
			$("#activeResetButtonImage").attr("src",
				staticUrl + "art/img/btn/activeResetButton.png");
			$("#glowImage").attr("src", staticUrl + "art/img/bkg/glow.png");
			$("#viewerCanvasContainer").css("background-image",
				"url(" + staticUrl + "art/img/dcl/viewerBackground.png)");
			
			// CSS rules.
			// Font size.
			fontSize = sWidth * 0.008;
			$('html').css('font-size', fontSize);
			
			// Text in "About" board.
			$(".leftContainer>div:first-child>div:nth-child(1)>" +
				"p:nth-child(3)").css("top", "24.1%");
			$(".leftContainer>div:first-child>div:nth-child(1)>" +
				"p:nth-child(4)").css("top", "48%");
			$(".leftContainer>div:first-child>div:nth-child(1)>" +
				"p:nth-child(5)").css("top", "68%");
			
			// Scroll container's dimensions.
			$(".leftContainer>div:first-child>div>" +
				".scrollContainer").css("height", "53.3%");
			$(".leftContainer>div:first-child>div>" +
				".scrollContainer").css("top", "32.2%");
			
			// "Character action" buttons' left property.
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(8)>div:nth-child(8)").css("left", "14%");
			$(".leftContainer>div:first-child>div:nth-child(3)>" +
				"div:nth-child(8)>div:nth-child(9)").css("left", "40%");
			break;
		}
		
		// Load and assign loader image according to screen size (but not
		// ratio).
		switch (sWidth) {
		case 640:
		case 800:
		case 1024:
		case 1152:
			// Image assignment.
			$("#loaderImage").attr("src",
				staticUrl + "art/img/bkg/loadingAnimation_small.gif");
			
			// "Loader" size (div containing loader image).
			$(".loader").css("height", "240px");
			$(".loader").css("width", "165px");
			$(".loader").css("margin-top", "-120px");
			$(".loader").css("margin-left", "-82.5px");
			break;
		case 1280:
		case 1360:
		case 1366:
		case 1440:
		case 1600:
		case 1680:
		case 1920:
			// Image assignment.
			$("#loaderImage").attr("src",
				staticUrl + "art/img/bkg/loadingAnimation_regular.gif");
			
			// "Loader" size (div containing loader image).
			$(".loader").css("height", "480px");
			$(".loader").css("width", "313px");
			$(".loader").css("margin-top", "-240px");
			$(".loader").css("margin-left", "-165px");
			break;
		case 2048:
		case 2560:
			// Image assignment.
			$("#loaderImage").attr("src",
				staticUrl + "art/img/bkg/loadingAnimation_big.gif");
			
			// "Loader" size (div containing loader image).
			$(".loader").css("height", "960px");
			$(".loader").css("width", "626px");
			$(".loader").css("margin-top", "-480px");
			$(".loader").css("margin-left", "-313px");
			break;
		}
		
		// Resize prompt: adjust prompt's size according to
		// resolution.
		promptHandleSize = sWidth * 0.52;
		promptPaperSize = sWidth * 0.40;
		$('#promptPaperImage').css("width", promptPaperSize);
		$('.promptHandleImage').css("width", promptHandleSize);
	},
	/*--------------------------------------------------------------------------
	LOG OUT: Sends a signal through AJAX to log out the user.
	
	HOW TO USE: Just call the function.
	--------------------------------------------------------------------------*/
	logout: function (how) {
		$.ajax({
			type: 'POST',
			url: '/',
			data: {'id': 'logout'},
			dataType: 'json',
			success: function (json) {
				var time = 0; // Prompt close animation time.
				if (json.userStatus === 'logged out') {
					console.log('User has logged out successfully.');
					// Only say goodbye when explicitally requested.
					if (how === 'sayingGoodbye') {
						time = 3750;
						gui.prompt = '#goodbye';
						gui.showPrompt('open');
					}
					setTimeout(function () {
						// Close goodbye scroll if it is open.
						if (how === 'sayingGoodbye') {
							gui.showPrompt('close');
						}
						// Replace the "Account" button with the "Log out" one.
						$(gui.activeButton[21]).css('visibility', 'hidden');
						gui.isButtonActive[21] = false;
						$(gui.button[21]).hide();
						$('#accountButtonImage').hide();
						$('#loginButtonImage').show();
						$(gui.button[5]).show();
						gui.flashButton(5, 1000, 5);
						// Erase the username and e-mail on the "Account" board.
						$('#accountUsername').val('');
						$('#accountFormEmail').val('');
						// IMPORTANT!-------------------------------------------
						// Make sure to clean user library as well as its form
						// action's value.
						$('#libraryContent').empty();
						$(downloadFromLibraryForm).attr('action', '');
						//------------------------------------------------------
						gui.userEmail = null;
					}, time);
				} else if (json.userStatus === 'error') {
					console.log('ERROR: unable to log out!');
				} else if (json.userStatus === 'already logged out') {
					console.log('User is already logged out!');
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});
	},
	/*--------------------------------------------------------------------------
	RESET FORM ON CANCEL: Resets form's content when a "cancel" button is
	clicked.
	
	HOW TO USE: Pass the index number of the button which triggers the action
	first, then the form's array, and lastly, the time delay which tells how
	much time should the action wait before running.
	
	Parameters:
	
	buttonIndex - The index number of the button which triggers the action.
	form        - Form's array with all its components.
	time        - How much time the action should wait before actually taking
	              place.
	--------------------------------------------------------------------------*/
	resetFormOnCancel: function (buttonIndex, form, time) {
		var color;
		color = $('form').css('color');
		
		var placeholder = new Array ();
		
		// Reset it on page load.
		for (var i = 0; i < form.length; i++) {
			$(form[i]).val('');
			placeholder.push($(form[i]).attr('placeholder'));
		}
		
		// Reset it when clicking on button.
		$(gui.button[buttonIndex]).click(function() {
			setTimeout(function() {
				for (var i = 0; i < form.length; i++) {
					$(form[i]).val('');
					$(form[i]).attr('placeholder', placeholder[i]);
					$(form[i]).css('color', color);
					
					// Fix for Webkit.
					$(form[i]).removeClass('warning');
				}
			}, time);
		});
	},
	/*--------------------------------------------------------------------------
	RESET FORM ON SEND: Reset the input's content of a form when the specified
	button is clicked and on page load.
	
	HOW TO USE: Pass the index number of the button which triggers the
	action, the form to be reseted, its index number and then the desired time
	delay.
	
	Parameters:
	
	buttonIndex - Index number of the button which triggers the action.
	form        - The form to be reseted.
	formIndex   - The form index number.
	time        - Time delay.
	--------------------------------------------------------------------------*/
	resetFormOnSend: function (buttonIndex, form, formIndex, time) {
		var placeholder = new Array ();
		
		// Reset it on page load.
		for (var i = 0; i < form.length; i++) {
			$(form[i]).val('');
			placeholder.push($(form[i]).attr('placeholder'));
		}
		
		// Reset it when clicking on button.
		$(gui.button[buttonIndex]).click(function() {
			if (gui.isFormValid[formIndex] === true) {
				setTimeout(function() {
					$('#confirmationMessage').hide();
					$('#contactForm').show();
					for (var i = 0; i < form.length; i++) {
						$(form[i]).val('');
						$(form[i]).attr('placeholder', placeholder[i]);
					}
				}, time);
			}
		});
	},
	/*--------------------------------------------------------------------------
	RESET SCROLL: Reset scroll when an arbitrary button is clicked.
	
	HOW TO USE: Use once per scroll. Pass the scroll's array as first
	argument and then all the indices of the buttons which will trigger the
	reset.
	--------------------------------------------------------------------------*/
	resetScroll: function () {
		var array;                         // Scroll's array.
		var textBox;                       // Text box to be affected. 
		var thumb;                         // Scroll thumb to be affected.
		var groupOfButtons = new Array (); // Buttons which trigger the reset.
		array = arguments[0]; // Equal to the function's first argument.
		textBox = 6;          // Equal to text box's index in its array.
		thumb = 2;            // Equal to thumb's index in its array.
		
		// Create an array with the buttons' index numbers.
		for (var i = 1; i < arguments.length; i++) {
			groupOfButtons.push(arguments[i]);
		}
		
		// If one of the is clicked trigger the reset.
		for (var i = 0; i < groupOfButtons.length; i++) {
			$(gui.button[groupOfButtons[i]]).click(function() {
				setTimeout(function() {
					$(array[textBox]).css({'top': 0});
					$(array[thumb]).css({'top': 0});
				}, 750);
			});
		}	
	},
	/*--------------------------------------------------------------------------
	RETURN TO DOWNLOAD: Return to download prompt from "FAQ" board when "Back to
	download" button is clicked.
	
	HOW TO USE: Pass the index number of the button wich triggers the action
	as a parameter.
	
	Parameters:
	
	buttonIndex - Index number of the button which triggers the action.
	--------------------------------------------------------------------------*/
	returnToDownload: function (buttonIndex) {
		$(gui.button[buttonIndex]).click(function() {
			// Make sure not to store board's height in a variable,
			// so that we can get the current one if browser zoom
			// changes, which in turn changes DOM elements dimensions.
			$(gui.board[3]).animate({top: -
				$('.leftContainer>div>div').height()}, 750);
			gui.isBoardVisible[3] = false;
			setTimeout(function () {
				$(gui.characterMenu).animate({top: 0}, 750);
				$(gui.board[3]).css('left', '');
				$(gui.board[2]).animate({top: 0}, 750);
				gui.isBoardVisible[2] = true;
				$('.leftContainer>div:first-child>div:nth-child(4)>' +
					'div:nth-child(4)').hide();
				$('.leftContainer>div:first-child>div:nth-child(4)>' +
					'div:last-child').show();
			}, 750);
		});
	},
	/*--------------------------------------------------------------------------
	SAVE MESSAGE: Save contact message to be sent to the server.
	
	HOW TO USE: Pass the index number of the button which triggers the action
	first and then the form which the input will be taken from. Lastly, pass
	the index number of the validated form.
	
	Parameters:
	
	buttonIndex - Index button of the button which triggers the action.
	form        - Form which the input will be taken from.
	formIndex   - Form's index number.
	--------------------------------------------------------------------------*/
	saveMessage: function (buttonIndex, form, formIndex) {
		
		$(gui.button[buttonIndex]).click(function() {
			if (gui.isFormValid[formIndex] === true) {
				var email = $(form[0]).val();
				var subject = $(form[1]).val();
				var message = $(form[2]).val();
				
				$.ajax({
					type:'POST',
					url:'/',
					data: {'id': 'message', 'email': email, 'subject': subject,
						'message': message},
					dataType: 'json',
					success: function(json)
					{
						// Notify if the server validation found any errors.
						for (property in json.errors) {
							if (property !== '' || property !== null ||
								property !== undefined) {
									console.log(property.charAt(0).toUpperCase()
									+ property.slice(1) + ' ' + 'is required.');
								}
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log(errorThrown);
					}
				});
			}
		});
	},
	/*--------------------------------------------------------------------------
	SCROLL: Gives functionality to website's vertical scrolls by dragging the
	thumb, scrolling with mousewheel, clicking anywhere on track or holding
	mouse button down on it to scroll to an specific point.
	
	HOW TO USE: Pass the scroll's array as a parameter. Call once per scroll.
	
	Parameters:
	
	array                       - The array with the scroll elements.
	trackToContainerHeightRatio - The ratio between track and container heights.
	--------------------------------------------------------------------------*/
	scroll: function(array, trackToContainerHeightRatio) {
		/*Glossary:
		Win-"Windows" coordinates relative to the top left corner of web page.
		Doc-"Document" coordinates relative to the top left corner of browser.
		Pnt-"Parent" coordinates relative to the element's parent.*/
		// Translate index numbers to names to make it human-friendly.
		var container = 0;
		var track = 1;
		var thumb = 2;
		var thumbTop = 3;
		var thumbCenter = 4;
		var thumbBottom = 5;
		var textBox = 6;
		var content = 7;
		var containerHeight;
		// Keep height proportion between container and track.
		var trackHeight = ($(array[container]).height()) *
			trackToContainerHeightRatio;
		$(array[track]).height(trackHeight);
		var contentHeight = $(array[content]).height();
		// Keep height proportion between thumb and track.
		var thumbHeight = (trackHeight / contentHeight) * trackHeight;
		// Set thumb center image to fill the gap in thumb.
		var thumbTopHeight = $(array[thumbTop]).height();
		var thumbBottomHeight = $(array[thumbBottom]).height();
		var thumbCenterHeight = thumbHeight - thumbTopHeight -
			thumbBottomHeight;
		var containerHeight = parseInt($(array[container]).css('height'));
		// If it's smaller, hide thumb and track. Otherwise change its height.
		if (contentHeight < trackHeight) {
			$(array[thumb]).css('display', 'none');
			$(array[thumbCenter]).css('display', 'none');
			$(array[track]).css('display', 'none');
		} else {
			$(array[thumb]).height(thumbHeight);
			$(array[thumbCenter]).height(thumbCenterHeight);
		}
		
		// General function for keeping thumb-content position proportional.
		var moveOnY = function (thumbPntLocY, thumbDeltaY, thumbTime,
				textBoxTime) {
				$(array[thumb]).animate({'top': thumbPntLocY}, thumbTime);
				/* Get thumb's updated position.
				The offset is the difference between thumb new and old location.
				It's added or subtracted to compensate for animation delay. */
				var getThumbPntLocY = parseInt($(array[thumb]).css('top')) +
						thumbDeltaY;
				// Keep content-thumb position proportional to parents.
				var textBoxNewPntLocY =
					(getThumbPntLocY / (trackHeight - thumbHeight)) *
					(contentHeight - containerHeight);
				$(array[textBox]).animate({"top": '-' + textBoxNewPntLocY +
					'px'}, textBoxTime);
		};
		
		// Event 1: scrolling when dragging thumb
		$(array[thumb]).draggable({
			axis: 'y',
			containment: 'parent',
			drag: function() {
				// Make sure to reload contentHeight, trackHeight, thumbHeight
				// and containerHeight to get their changes on browser zoom.
				contentHeight = $(array[content]).height();
				// Keep height proportion between container and track.
				trackHeight = (($(array[container]).height()) * 92.1) / 100;
				// Keep height proportion between thumb and track.
				thumbHeight = (trackHeight / contentHeight) * trackHeight;
				containerHeight = parseInt($(array[container]).css('height'));
				// Get the thumb top css value.
				var thumbPntLocY = parseInt($(this).css('top'));
				// Keep content-thumb position proportional to parents.
				var textBoxNewPntLocY =
					(thumbPntLocY / (trackHeight - thumbHeight)) *
					(contentHeight - containerHeight);
				// Subtract it to scroll content top value.
				$(array[textBox]).css("top", '-' + textBoxNewPntLocY + 'px');
			}
		}); // End of Event 1
		
		// Event 2: scrolling with mouse wheel.
		// Move the thumb when scrolling. jQuery's "bind()" doesn't pass IE
		// wheel event along so "addEventListener" must be used instead.
		document.getElementById(array[container].replace('#',
		'')).addEventListener(gui.mouseWheelEvent, function(e) {
				var thumbPntLocY = parseInt($(array[thumb]).css('top'));
				var multiplier = screen.height / 80; // Set value as see fit.
				var normalizedDeltaY;
				
				/*--------------------------------------------------------------
				Each browser engine uses different names and values for the
				event properties. Find the right name and normalize the
				deltaY property value.
				
				Browser list by render engine:
				******************************
				Gecko   - Firefox.
				Webkit  - Chrome, Chromium, Safari, Opera, Konqueror, Rekonq,
						  etc.
				Trident - Internet Explorer.
				
				"Delta Y" event property name by render engine:
				***********************************************
				'detlaY'     - modern Gecko and Webkit render engines versions.
				'wheelDelta' - modern Trident and older Webkit versions.
				--------------------------------------------------------------*/
				
				// Modern Gecko and Webkit.
				if (e.deltaY) {
					if (e.deltaY > 0) {
						normalizedDeltaY = 3;
					} else if (e.deltaY < 0) {
						normalizedDeltaY = - 3;
					} else {
						normalizedDeltaY = e.deltaY;
					}
				} else if (e.wheelDelta) { // Modern Trident and older Webkit.
						if (e.wheelDelta > 0) {
							normalizedDeltaY = - 3;
						} else if (e.wheelDelta < 0) {
							normalizedDeltaY = 3;
						} else {
							normalizedDeltaY = e.wheelDelta;
						}
				}
				
				thumbPntLocY += normalizedDeltaY * multiplier;
				// Make sure to reload contentHeight, trackHeight, thumbHeight
				// and containerHeight to get their changes on browser zoom.
				contentHeight = $(array[content]).height();
				// Keep height proportion between container and track.
				trackHeight = (($(array[container]).height()) * 92.1) / 100;
				// Keep height proportion between thumb and track.
				thumbHeight = (trackHeight / contentHeight) * trackHeight;
				containerHeight = parseInt($(array[container]).css('height'));
				
				if (thumbPntLocY < 0) {
				$(array[thumb]).css({'top': 0});
					thumbPntLocY = 0;
				}
				
				if (thumbPntLocY > (trackHeight - thumbHeight)) {
					$(array[thumb]).css({'top': trackHeight - thumbHeight});
					thumbPntLocY = trackHeight - thumbHeight;
					var getPositionTop = parseInt($(array[thumb]).css('top'));
					var scrollTopNew =
						(getPositionTop / (trackHeight - thumbHeight)) *
						(contentHeight - containerHeight);
					$(array[textBox]).animate({"top": '-' + scrollTopNew +
							'px'}, 50);
				} else {
					$(array[thumb]).animate({'top': thumbPntLocY}, 50);
					// Use translation value instead of thumb position.
					// Thus avoid delay in thumb position caused by its
					// animation.
					var textBoxNewPntLocY =
						(thumbPntLocY / (trackHeight - thumbHeight)) *
						(contentHeight - containerHeight);
					$(array[textBox]).animate({"top": '-' + textBoxNewPntLocY +
							'px'}, 50);
				}
				
				// Prevent event from notifying any parent handlers and web page
				// from scrolling.
				e.stopPropagation();
				e.preventDefault();
		}); // End of Event 2
		
		// Event 3: scroll by clicking on track and move content by steps.
		$(array[track]).mousedown(function(e) {
			var cursorDocLocY;     // Cursor Document Location on Y axis.
			var direction;         // The movement direction: up or down.
			var getThumbPntLocY;   // Get Thumb Parent Location on Y axis.
			var textBoxNewPntLocY; // Text box New Parent Location on Y axis.
			var thumbNewPntLocY;   // Thumb New Parent Location on Y axis.
			var thumbPntLocY;      // Thumb Parent Location on Y axis.
			var thumbDocLocY;      // Thumb Document Location on Y axis.
			var trackDocLocY;      // Track Document Location on Y axis.
			
			// Make sure to reload contentHeight, trackHeight, thumbHeight and
			// containerHeight to get their changes on browser zoom.
			contentHeight = $(array[content]).height();
			// Keep height proportion between container and track.
			trackHeight = (($(array[container]).height()) * 92.1) / 100;
			// Keep height proportion between thumb and track.
			thumbHeight = (trackHeight / contentHeight) * trackHeight;
			containerHeight = parseInt($(array[container]).css('height'));
			cursorDocLocY = e.clientY + $(document).scrollTop();
			thumbDeltaY = (trackHeight / thumbHeight) * 3;
			thumbPntLocY = parseInt($(array[thumb]).css('top'));
			thumbDocLocY = ($(array[thumb]).offset().top);
			if (cursorDocLocY < thumbDocLocY) {
				direction = -1;
			} else {
				direction = 1;
			}
			thumbNewPntLocY = parseInt($(array[thumb]).css('top')) +
				thumbDeltaY * direction;
			trackDocLocY = $(array[track]).offset().top;
			// Use thumb as reference for direction and prevent it from clicks.
			// Upper limit.
			if (cursorDocLocY < thumbDocLocY) {
				// Snap thumb if close to track top.
				if (thumbPntLocY < thumbDeltaY) {
					moveOnY(0, 0, 0, 0);
				// Or move up.
				} else if ((cursorDocLocY - trackDocLocY) < thumbPntLocY) {
					moveOnY(thumbNewPntLocY, thumbDeltaY * direction, 100, 100);
				}
			} 
			else if ((cursorDocLocY - trackDocLocY) >
					(thumbPntLocY + thumbHeight)) {
				// Snap thumb if close to track bottom.
				if (thumbPntLocY > trackHeight - thumbHeight - thumbDeltaY) {
					moveOnY((trackHeight - thumbHeight), 0, 0, 0);
				// Or move down.
				} else if ((cursorDocLocY - trackDocLocY) >
						(thumbPntLocY + thumbHeight)) {
					moveOnY(thumbNewPntLocY, thumbDeltaY * direction, 100, 100);
				}
			}
			
			/* Event 4: scroll by holding mouse button down on track.
			Thumb and content will move into position or stop on mouse up. */
			// Prevent user from clicking on thumb.
			if (cursorDocLocY < thumbDocLocY || cursorDocLocY > thumbDocLocY +
					thumbHeight) {
				/* Start countdown when user press mouse button on track.
				Let thumb dash if mouse button is not realeased before 0. */
				var countdown;
				var setCountdown = function() {
					countdown = setTimeout(function() {
						// Make sure to reload contentHeight, trackHeight,
						// thumbHeight and containerHeight to get their changes
						// on browser zoom.
						contentHeight = $(array[content]).height();
						// Keep height proportion between container and track.
						trackHeight = (($(array[container]).height()) * 92.1) /
							100;
						// Keep height proportion between thumb and track.
						thumbHeight = (trackHeight / contentHeight) *
							trackHeight;
						containerHeight =
							parseInt($(array[container]).css('height'));
						
						// Find time from velocity formula.
						var dashTime = ((cursorDocLocY - trackDocLocY) -
							parseInt($(array[thumb]).css('top'))) / 0.5;
						if (dashTime < 0) { // Eliminate negative value.
							dashTime = dashTime * (-1);
						} else {
							dashTime = dashTime * 1;
						}
						// Make thumb center match cursor position.
						var thumbNewPntLocY = cursorDocLocY - trackDocLocY -
								(thumbHeight / 2);
						// Snap thumb if close to track top.
						if ((thumbNewPntLocY + (thumbHeight / 2)) <
								thumbHeight) {
							thumbNewPntLocY = 0;
						}
						// Snap thumb if close to track bottom.
						else if ((thumbNewPntLocY + (thumbHeight / 2)) >
								trackHeight - thumbHeight) {
							thumbNewPntLocY = trackHeight - thumbHeight;
						}
						// Keep content-thumb position proportional to parents.
						var textBoxNewPntLocY =
								(thumbNewPntLocY / (trackHeight - thumbHeight))
								* (contentHeight - containerHeight);
						$(array[thumb]).animate({'top': thumbNewPntLocY},
								dashTime);
						$(array[textBox]).animate({"top": '-' +
								textBoxNewPntLocY + 'px'}, dashTime);
					}, 150); // End of countdown.
				}
				setCountdown(); // Run countdown.
			}
			
			// Stop countdown and thumb-content animation on mouse up.
			$(document).mouseup(function() {
				clearTimeout(countdown);
				$(array[thumb]).stop();
				$(array[textBox]).stop();
			}); // End of Event 4
		}); // End of Event 3
	}, // End of scroll.
	/*--------------------------------------------------------------------------
	SCROLL TO: Scrolls to a locator (a chapter or a page in a book) when
	clicking on an index heading (phrases placed in a list).
	
	HOW TO USE: Just pass the array containing the scroll elements.
	
	Parameters:
	
	array    - The array with the scroll elements.
	--------------------------------------------------------------------------*/
	scrollToLocator: function (array) {
		var track = array[1];
		var thumb = array[2];
		var textBox = array[6];
		var content = array[7];			

		$(content + '>.headings').click(function() {
			// Match questions with their answers by their numbers.
			var locatorNumber =
				$(this).attr('class').split(' ')[0].slice(8);
			console.log(locatorNumber);
			// Get the locator's relative top location and add the same
			// offset that the title of the scroll has to make it more
			// readible.
			var locatorPntLocY = $(content + '>.locator_' +
				locatorNumber).offset().top -
					$(textBox).offset().top;
			var offset =
				parseInt($(content + '>br').css('font-size').slice(0,
					-2)) * 3;
			var textBoxNewPntLocY = locatorPntLocY - offset;
			// Get the thumb's new location in relation to how scrolled
			// the content will be.
			var thumbNewPntLocY =  $(track).height() *
				textBoxNewPntLocY / $(content).height();
			// This is an effect that will flash the locator once we get
			// to it.
			var flashText = function () {
				var locator = $(content + '>.locator_' +
					locatorNumber).html();
				$(content).append('<h4>' + locator + '</h4>');
				$(content + '>h4:last-child').css('position',
					'absolute');
				$(content + '>h4:last-child').css('width', '100%');
				$(content + '>h4:last-child').css('top',
					locatorPntLocY);
				$(content + '>h4:last-child').animate({'opacity': 0},
					{queue: false, duration: 600}).hide('scale',
						{percent: 130}, 600, function () {
							$(this).remove()
						});
			};
			// Scroll down to the locator but don't let content go beyond
			// its container.
			if (textBoxNewPntLocY >= $(content).height() - $(textBox).height()) {
				$(textBox).animate({'top': $(textBox).height() -
					$(content).height()}, textBoxNewPntLocY *
						0.5, function () {
							flashText();
						});
			} else {
				$(textBox).animate({'top': - textBoxNewPntLocY}, textBoxNewPntLocY
					* 0.5, function () {
						flashText();
					});
			}
			// Move the thumb accordingly and also constrain it to its track.
			if (thumbNewPntLocY >= $(track).height() - $(thumb).height()) {
				$(thumb).animate({'top': $(track).height() - $(thumb).height()},
					 textBoxNewPntLocY * 0.5);
			} else {
				$(thumb).animate({'top': thumbNewPntLocY}, textBoxNewPntLocY * 0.5);
			}
		});
	},
	/*--------------------------------------------------------------------------
	SEND ACCOUNT RECOVERY REQUEST: Sends the e-mail address the user used for
	resgistration as prompted. A one-time link will be sent to this e-mail
	address where user will be able to reset the account's password.
	
	HOW TO USE: Pass the index number of the button which triggers the action
	first and then the form where the e-mail address will be taken from. Lastly,
	pass the index number of the validated form.
	
	Parameters:
	
	buttonIndex - The index button of the button which triggers the action.
	form        - The form where the input will be taken from.
	formIndex   - The form's index number.
	--------------------------------------------------------------------------*/
	sendAccountRecoveryRequest: function (buttonIndex, form, formIndex) {
		$(gui.button[buttonIndex]).click(function() {
			if (gui.isFormValid[formIndex] === true) {
				gui.recoveryEmail = $(form[0]).val();
				
				$.ajax({
					type:'POST',
					url:'/',
					data: {'id': 'accountRecoveryRequest',
						'email': gui.recoveryEmail},
					dataType: 'json',
					success: function (json) {
						if (json.requestStatus === 'valid') {
							console.log('Request sent successfully!');
							$(gui.prompt).hide();
							gui.prompt = '#accountRecoveryConfirmation';
							gui.showPrompt('blink');
						} else if (json.requestStatus === 'invalid') {
							console.log('Account does not exist!');
							gui.warnInInput(gui.recoveryForm[0],
								gui.recoveryErrors[0]);
								gui.recoveryEamil = null;
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						console.log(errorThrown);
					}
				});
			}
		});
	},
	/*--------------------------------------------------------------------------
	SET INITIAL LOCATION: Set te initial location for selected element.
	
	HOW TO USE: Pass the element you want to move as a parameter and then
	pass an element which its height will be equeal to the distance to move
	the firt one (usually the height of the object itself).
	
	Parameters:
	
	element       - Element to be positioned.
	elementHeight - The element whose height will be used to move the other.
	--------------------------------------------------------------------------*/
	setInitialLocation: function (element, elementHeight) {
		var distance; // Distance to be moved.
		
		// Distance equal to selected element's height.
		distance = $(elementHeight).height();
		
		// Hide the element by moving it up.
		$(element).css('top',  - distance);
	},
	/*--------------------------------------------------------------------------
	SET DEFAULT ACTIVE BUTTON: set a button as active when another arbitrary
	button is clicked.
	
	HOW TO USE: Pass the index number of the button to be clicked first and
	then the index number of the button to be affected.
	
	Parameters:
	
	toBeClickedIndex - Index number of the button which will fire the action.
	toBeAffected     - Index number of the button to be affected.
	--------------------------------------------------------------------------*/
	setDefaultActiveButton: function (toBeClickedIndex, toBeAffectedIndex) {
		$(gui.button[toBeClickedIndex]).click(function() {
			$(gui.activeButton[toBeAffectedIndex]).css('visibility', 'visible');
			gui.isButtonActive[toBeAffectedIndex] = true;
		});
	},
	/*--------------------------------------------------------------------------
	SET DEFAULT ACTIVE PROFILE: set a button as active when another arbitrary
	button is clicked.
	
	HOW TO USE: Pass the index number of the button to be clicked first and
	then the index number of the profile to be affected.
	
	Parameters:
	
	toBeClickedIndex - Index number of the button which will fire the action.
	toBeAffected     - Index number of the profile to be affected.
	--------------------------------------------------------------------------*/
	setDefaultActiveProfile: function (toBeClickedIndex, toBeAffectedIndex) {
		$(gui.button[toBeClickedIndex]).click(function() {
			
			// First hide all the profiles.
			for (var i = 0; i < gui.profile.length; i++) {
				$(gui.profile[i]).css('visibility', 'hidden');
				gui.isProfileVisible[i] = false;
			}
			
			// Then display the chosen one.
			$(gui.profile[toBeAffectedIndex]).css('visibility', 'visible');
			gui.isProfileVisible[toBeAffectedIndex] = true;
		});
	},
	/*--------------------------------------------------------------------------
	SHOW CONFIRMATION MESSAGE: Displays a message when "send" button is
	clicked which replaces the input form.
	
	HOW TO USE: Pass the button's index number as a parameter and the validated
	form's index number.
	
	Parameters:
	
	buttonIndex - The index number of the button which triggers the action.
	formIndex   - Form's index number.
	--------------------------------------------------------------------------*/
	showConfirmationMessage: function (buttonIndex, formIndex) {
		// IMPORTANT: remember to do it after the input has been validated.
		$(gui.button[buttonIndex]).click(function () {
			if (gui.isFormValid[formIndex] === true) {
				$('#contactForm').hide();
				$('#confirmationMessage').show();
			}
		});
	},	
	/*--------------------------------------------------------------------------
	SHOW BUTTON AS ACTIVE: Flash button as active when clicked. Activate it
	when LMB is pressed and deactivated when released.
	
	HOW TO USE: Pass the index number of the button to be affected by this
	effect as argument.
	--------------------------------------------------------------------------*/
	showButtonAsActive: function (buttonIndex) {
		$(gui.button[buttonIndex]).mousedown(function() {
			$(gui.hoveredButton[buttonIndex]).fadeTo(0, 0);
			$(gui.activeButton[buttonIndex]).css('visibility', 'visible');
		});
		$(gui.button[buttonIndex]).mouseup(function() {
			$(gui.activeButton[buttonIndex]).css('visibility', 'hidden');
			$(gui.hoveredButton[buttonIndex]).fadeTo(0, 1);
		});
		$(gui.button[buttonIndex]).mouseleave(function() {
			$(gui.activeButton[buttonIndex]).css('visibility', 'hidden');
		});
	},
	/*--------------------------------------------------------------------------
	SHOW BUTTON AS ACTIVE AND TURN OFF: Show clicked button as active while
	turning off all the others, and then deactivate it after some time.
	
	HOW TO USE: Pass the index number of the button to be clicked and after
	it all the others which will be turned off by it.
	--------------------------------------------------------------------------*/
	showButtonAsActiveAndTurnOff: function () {
		var groupOfButtons = new Array ();
		
		for (var i = 0; i < arguments.length; i++) {
			groupOfButtons.push(arguments[i]);
		}
		
		$(gui.button[groupOfButtons[0]]).click(function() {
			for (var i = 0; i < groupOfButtons.length; i++) {
				$(gui.activeButton[groupOfButtons[i]]).css('visibility',
					'hidden');
				gui.isButtonActive[groupOfButtons[i]] = false;
			}
			$(gui.activeButton[groupOfButtons[0]]).css('visibility',
				'visible');
			gui.isButtonActive[groupOfButtons[0]] = true;
			setTimeout(function() {
				$(gui.activeButton[groupOfButtons[0]]).css('visibility',
					'hidden');
				gui.isButtonActive[groupOfButtons[0]] = false;
			}, 750);
		});
	},
	/*--------------------------------------------------------------------------
	SHOW BUTTON AS HOVERED: Show hover effect when button is hovered.
	
	HOW TO USE: Pass the index number of the button to be affected by this
	effect as argument.
	--------------------------------------------------------------------------*/
	showButtonAsHovered: function (buttonIndex) {
		$(gui.button[buttonIndex]).mouseenter(function() {
			$(gui.hoveredButton[buttonIndex]).css('visibility', 'visible')
			$(gui.hoveredButton[buttonIndex]).fadeTo(75, 1);
		});
		$(gui.button[buttonIndex]).mouseleave(function() {
			$(gui.hoveredButton[buttonIndex]).fadeTo(75, 0, function() {
				$(gui.hoveredButton[buttonIndex]).css('visibility', 'hidden')
			});
		});
	},
	/*--------------------------------------------------------------------------
	SHOW PROMPT: Show prompt while keeping it centered on browser window. It has
	three animations: 'open', 'close' and 'blink'.
	
	HOW TO USE: Just run it by passing the desired animation as a string.
	
	Parameters:
	
	action - The animation to be played as a string: 'open', 'close' and 'blink'
			 (it closes and then opens back again).
	--------------------------------------------------------------------------*/
	showPrompt: function (action) {
		var closedHeight;                 // Container's height when closed.
		var openHeight;                   // Container's height when open.
		var openHeightPercentage;         // Same value but in percentage.
		var containerXLoc;                // Container's location on X axis.
		var containerYLoc;                // Container's location on Y axis.
		var handleHeigth;                 // Handle's height after resizing.
		var handleWidth;                  // Hanlde's width after resizing.
		var paperHeight;                  // Paper's height after resizing.
		var paperWidth;                   // Paper's width after resizing.
		var wHeight = $(window).height(); // Browser window's height. 
		var wWidth = $(window).width();   // Browser window's width.
		
		// Display prompt.
		$('.blur').show();
		$('.blur').css('opacity', 0.5);
		$('.promptContainer').show();

		
		// Disable clicking on main menu buttons.
		for (var i = 0; i < 22 && i > 13 && i < 16 && i != 18 && i != 19; i++) {
			$(gui.button[i]).css('pointer-events', 'none');
		}
		
		// Disable clicking on TOS and PP buttons.
		for (var i = 30; i < 32; i++) {
			$(gui.button[i]).css('pointer-events', 'none');
		}
		
		// Change cursor to default for TOS and PP buttons.
		for (var i = 30; i < 32; i++) {
			$(gui.button[i]).css('cursor', 'default');
		}
		
		// Save window's dimensions.
		wHeight = $(window).height();
		wWidth = $(window).width();
		
		// Save images' height and width.
		handleHeight = $('#scrollTopHandle').height();
		handleWidth = $('#scrollTopHandle').width();
		paperHeight = $('#promptPaperImage').height();
		paperWidth = $('#promptPaperImage').width();
		
		// Calculate container closed height and open heigthm then translate
		// those values to percentage.
		closedHeight = handleHeight * 2;
		openHeight = paperHeight + handleHeight;
		openHeightPercentage = openHeight * 100 / closedHeight;
		closedHeightPercentage = closedHeight * 100 / openHeight;
		
		// Set values that are common to all the states:
		// Set container's intial width.
		$('.promptContainer').width(handleWidth);
		// Center container horizontally on window.
		$('.promptContainer').css("left", (wWidth - handleWidth) / 2);
		// Center paper image wrapper.
		$('.promptContainer>div').css('top', handleHeight / 2);
		// Center paper image horizontally on container.
		$('#promptPaperImage').css("left", (handleWidth - paperWidth) / 2);
		
		if (action === 'open') {
			// Set container's initial height (if the action is 'open', then
			// it's because its initial state is closed).
			$('.promptContainer').height(closedHeight);
			// Center container vertically on window.
			$('.promptContainer').css("top", (wHeight - closedHeight) / 2);
			// The paper image wrapper with "overflow: hidden".
			$('.promptContainer>div').height(handleHeight);
			// The paper image itslef must be centered vertically when
			// closed.
			$('#promptPaperImage').css("top", - paperHeight / 2);
			// Open scroll (scale container vertically).
			$('.promptContainer').effect('scale', {direction: 'vertical',
				percent: openHeightPercentage}, function () {
					$(this).height(openHeight); // Keep it opened.
					// Display content.
					$(gui.prompt).fadeTo(75, 1);
			}, 2000);
			// Paper image and its wrapper also need to be animated
			// accordingly.
			$('.promptContainer>div').animate({height: paperHeight}, 200);
			$('#promptPaperImage').animate({top: 0}, 200);
		} else if (action === 'close') {
			// Since the action is 'close' we will assume it's already open
			// and therefore there is no need to set any dimensions. We will
			// just close it.
			$('.promptContainer').effect('scale', {direction: 'vertical',
				percent: closedHeightPercentage}, function () {
					$(this).height(closedHeight); // Keep it close.
					// Hide prompt.
					$('.blur').hide();
					$('.blur').css('opacity', 0);
					$('.promptContainer').hide();
					$(gui.prompt).hide();
					
					// Enable clicking on main menu buttons.
					for (var i = 0; i < 22 && i > 13 && i < 16 && i != 18 &&
						i != 19; i++) {
							$(gui.button[i]).css('pointer-events', 'none');
					}
					// Enable clicking on TOS and PP buttons.
					for (var i = 30; i < 32; i++) {
						$(gui.button[i]).css('pointer-events', 'auto');
					}
					
					// Change cursor to default for TOS and PP buttons.
					for (var i = 30; i < 32; i++) {
						$(gui.button[i]).css('cursor', 'pointer');
					}
			}, 2000);
			
			// Paper image and its wrapper also need to be animated
			// accordingly.
			$('.promptContainer>div').animate({height: handleHeight}, 400,
				function () {
					$(this).animate({height: handleHeight}, 200);
			});
			
			$('#promptPaperImage').animate({top: - paperHeight / 2}, 400,
				function () {
					$(this).animate({top: - paperHeight / 2}, 200);
			});
			
			$(gui.prompt).fadeTo(75, 0);
		} else if (action === 'blink') {
			$('.promptContainer').effect('scale', {direction: 'vertical',
				percent: closedHeightPercentage}, function () {
					$(this).height(closedHeight);
					$(this).effect('scale', {direction: 'vertical',
						percent: openHeightPercentage}, function () {
							$(this).height(openHeight);
							// Display content.
							$(gui.prompt).fadeTo(75, 1);
					}, 2000);
			}, 2000);
			
			// Paper image and its wrapper also need to be animated
			// accordingly.
			$('.promptContainer>div').animate({height:
				handleHeight}, 400, function () {
					$(this).animate({height: paperHeight}, 200);
				});
			$('#promptPaperImage').animate({top: - paperHeight / 2},
				400, function () {
					$(this).animate({top: 0}, 200);
			});
			
			$(gui.prompt).fadeTo(75, 0);
		}
		
		// Center scroll everytime winow is resized.
		$(window).resize(function() {
			// Make sure to use the current values.
			wHeight = $(window).height();
			wWidth = $(window).width();
			containerXLoc = (wWidth - $('#scrollTopHandle').width()) / 2;
			containerYLoc = (wHeight - $('.promptContainer').height()) / 2;
			handleHeight = $('#scrollTopHandle').height();
			paperHeight = $('#promptPaperImage').height();
			
			$('.promptContainer').css("left", containerXLoc);
			$('.promptContainer').css("top", containerYLoc);
			$('#promptPaperImage').css('top', 0);
			$('.promptContainer>div').css('top', handleHeight / 2);
			$('.promptContainer>div').height(paperHeight);
		});
	},
	/*--------------------------------------------------------------------------
	SHOW ONE BUTTON AS ACTIVE AT A TIME: Shows clicked button as active while
	turning off the previous one. However, it doesn't turn currently active
	button off by clicking on it.
	
	HOW TO USE: Pass the index numbers of the buttons which will be affected
	by this effect.
	--------------------------------------------------------------------------*/
	showOneButtonAsActiveAtATime: function () {
		var clickedButton;      // Button which is clicked.
		var clickedButtonIndex; // Clicked Button Index number.
		var isButtonDetected;   // Tells whether a button is already on or not.
		var groupOfButtons = new Array (); // Index of buttons to be affected.
		
		// First, create an array with the index numbers of the buttons to be
		// affected by this effect.		
		for (var i = 0; i < arguments.length; i++) {
			groupOfButtons.push(arguments[i]);
		}
		
		// Then, if one the button with these index numbers is clicked, get its
		// id and index number.		
		for (var i = 0; i < groupOfButtons.length; i++) {
			$(gui.button[groupOfButtons[i]]).click(function() {
				clickedButton = "#" + $(this).attr('id');
				clickedButtonIndex = $.inArray(clickedButton, gui.button);
				
				// Search for an active button among the button of the group.
				// If found, trigger the alarm and stop searching.
				for (var i = 0; i < groupOfButtons.length; i++) {
					if (gui.isButtonActive[groupOfButtons[i]] === true) {
						isButtonDetected = true;
						break;
					}
				}
				
				if (isButtonDetected === true) {
					$(gui.activeButton[groupOfButtons[i]]).css('visibility',
						'hidden');
					gui.isButtonActive[groupOfButtons[i]] = false;
					$(gui.activeButton[clickedButtonIndex]).css('visibility',
						'visible');
					gui.isButtonActive[clickedButtonIndex] = true;
				} else {
					$(gui.activeButton[clickedButtonIndex]).css('visibility',
						'visible');
					gui.isButtonActive[clickedButtonIndex] = true;
				}
			});
		}
	},
	/*--------------------------------------------------------------------------
	SHOW OR HIDE: Show or hide the boards depending on which button was
	clicked and which board is already displayed (if any at all).
	
	HOW TO USE: Pass the index number of the board to be affected by this
	effect as argument.
	--------------------------------------------------------------------------*/
	showOrHide: function () {
		var clickedButton;      // Button which is clicked.
		var clickedButtonIndex; // Clicked Button Index number.
		var isBoardDetected;    // Tells if a board is already displayed.
		var groupOfBoards = new Array (); // Index of boards to be affected.
		
		// Create an array with the index numbers of the boards to be
		// affected by this effect.
		for (var i = 0; i < arguments.length; i++) {
			groupOfBoards.push(arguments[i]);
		}
		
		// Then, if one the button with these index numbers is clicked, get its
		// id and index number.
		for (var i = 0; i < groupOfBoards.length; i++) {
			$(gui.button[groupOfBoards[i]]).click(function() {
				clickedButton = "#" + $(this).attr('id');
				clickedButtonIndex = $.inArray(clickedButton, gui.button);
				
				// Search for a displayed board among the boards of the group.
				// If found, trigger the alarm and stop searching.
				for (var i = 0; i < groupOfBoards.length; i++) {
					if (gui.isBoardVisible[groupOfBoards[i]] === true) {
						isBoardDetected = true;
						break;
					}
				}
				
				// If the alarm was triggered, that is, an already displayed
				// board was found, then just hide it if it is the case that is 
				// the same which was clicked, or if it was another, hide it and
				// turn on the one which was clicked.
				if (isBoardDetected === true) {
					switch (groupOfBoards[i]) {
					case clickedButtonIndex:
						// Make sure not to store board's height in a variable,
						// so that we can get the current one if browser zoom
						// changes, which in turn changes DOM elements
						// dimensions.
						$(gui.board[clickedButtonIndex]).animate({top:
							- $('.leftContainer>div>div').height()}, 750);
						gui.isBoardVisible[clickedButtonIndex] = false;
						break;
					default:
						$(gui.board[groupOfBoards[i]]).animate({top:
							- $('.leftContainer>div>div').height()}, 750);
						gui.isBoardVisible[groupOfBoards[i]] = false;
						setTimeout(function () {
							$(gui.board[clickedButtonIndex]).animate({top: 0},
								750);
							gui.isBoardVisible[clickedButtonIndex] = true;
						}, 750);
					}
					isBoardDetected = false;
				
				// Else, if no board was found to be visible, just show the one
				// which was selected.
				} else {
					$(gui.board[clickedButtonIndex]).animate({top: 0}, 750);
					gui.isBoardVisible[clickedButtonIndex] = true;
				}
			});
		}
	},
	/*--------------------------------------------------------------------------
	SWITCH ACTION: Change character's action name as the previous and
	next buttons are clicked.
	
	HOW TO USE: Pass the index numbers of both previous and next buttons
	first, then the first and last actions respectively. Finally pass the
	index number of the button which will reset the action to the default.
	
	Parameters:
	
	actionName          - Action name field.
	previousButtonIndex - Previous action button's index number.
	nextButtonIndex     - Next action button's index number.
	firstAction         - The first action that will be displayed.
	lastAction          - The last action that will be displayed.
	defaultActionIndex  - The index number of the default action.
	resetButtonIndex    - Reset button's index number.
	--------------------------------------------------------------------------*/
	switchAction: function (actionName, previousButtonIndex, nextButtonIndex,
		firstAction, lastAction, defaultActionIndex, resetButtonIndex) {
		var currentSlot;
		// Update the current slot.
		currentSlot = gui.actionIndex;
		// Display the action name.
		$(actionName).append(gui.action[currentSlot]);
		// Switch to previous action on click.
		$(gui.button[previousButtonIndex]).click(function() {
			// Update the current slot.
			currentSlot = gui.actionIndex;
			for (var i = currentSlot; i < gui.action.length; i--) {
				if (currentSlot >= i) { // After first action go to last slot.
					if (i === firstAction) {
						currentSlot = lastAction;
					} else { // Otherwise, switch to previous slot.
						currentSlot = i - 1; // Update position.
					}
					break; // Go one slot at a time.
				}
			}
			// Switch action names.
			$(actionName).empty(); // Delete old one.
			$(actionName).append(gui.action[currentSlot]); // Show new one.
			gui.actionIndex = currentSlot;
		});
		// Switch to the next action on click.
		$(gui.button[nextButtonIndex]).click(function() {
			// Update the current slot.
			currentSlot = gui.actionIndex;
			for (var i = currentSlot; i < gui.action.length; i++) {
				if (currentSlot <= i) {
					if (i === lastAction) { // After last action go to start.
						currentSlot = firstAction;
					} else { // Otherwise, switch to next slot.
						currentSlot = i + 1; // Update position.
					}
					break; // Go one slot at a time.
				}	
			}
			// Switch action names.
			$(actionName).empty(); // Delete old one.
			$(actionName).append(gui.action[currentSlot]); // Show new one.
			gui.actionIndex = currentSlot;
		});
		
		$(gui.button[resetButtonIndex]).click(function() {
			setTimeout(function() {
				// Switch action names. Empty field and show default action.
				$(actionName).empty();
				$(actionName).append(gui.action[defaultActionIndex]);
				gui.actionIndex = defaultActionIndex;
			}, 750);
		});
	},
	/*--------------------------------------------------------------------------
	SWITCH MENUS: Hide one menu and show the other when an arbitrary button
	is clicked.
	
	HOW TO USE: Pass index number of the button which triggers the action
	first, then the current menu and lastly, the new menu.
	
	Parameters:
	
	buttonIndex - Index of the button which triggers the action.
	currentMenu - Menu currently being displayed.
	newMenu     - New menu to be displayed.
	--------------------------------------------------------------------------*/
	switchMenus: function (buttonIndex, currentMenu, newMenu) {
		$(gui.button[buttonIndex]).click(function() { // Hide current menu.
			// Make sure not to store current menu's height in a variable,
			// so that we can get the current one if browser zoom changes, which
			// in turn changes DOM elements dimensions.
			$(currentMenu).animate({top: - $(currentMenu).height()}, 750);
			setTimeout(function() { // Wait for current menu to hide.
				$(newMenu).animate({top: 0}, 750);
			}, 750);
		});
	},	
	/*--------------------------------------------------------------------------
	SWITCH PROFILES: switch character profiles as the different character
	buttons are clicked.
	
	HOW TO USE: Pass the index numbers of the profiles (equal to those of
	their respective buttons).
	--------------------------------------------------------------------------*/
	switchProfiles: function () {
		var clickedButton;                  // Button which is clicked.
		var clickedButtonIndex;             // Clicked Button Index number.
		var groupOfProfiles = new Array (); // Indeces of affected profiles.
		var isProfileVisible;               // Tells if a profile is visible.
		var visibleProfileIndex;            // Currently visible profile.
		
		for (var i = 0; i < arguments.length; i++) {
			groupOfProfiles.push(arguments[i]);
		}
		
		for (var i = 0; i < groupOfProfiles.length; i++) {
			$(gui.button[groupOfProfiles[i]]).click(function() {
				clickedButton = "#" + $(this).attr('id');
				clickedButtonIndex = $.inArray(clickedButton, gui.button);
				
				for (var i = 0; i < groupOfProfiles.length; i++) {
					if (gui.isProfileVisible[groupOfProfiles[i]] === true) {
						isProfileVisible = true;
						visibleProfileIndex = groupOfProfiles[i];
						break;
					}
				}
				
				if (isProfileVisible === true) {
					switch (visibleProfileIndex) {
					case clickedButtonIndex:
						$(gui.profile[clickedButtonIndex]).css('visibility',
							'visible');
						gui.isProfileVisible[clickedButtonIndex] = true;
						break;
					default:
						$(gui.profile[visibleProfileIndex]).css('visibility',
							'hidden');
						gui.isProfileVisible[visibleProfileIndex] = false;
						$(gui.profile[clickedButtonIndex]).css('visibility',
							'visible');
						gui.isProfileVisible[clickedButtonIndex] = true;
					}
					isProfileVisible = false;
				}
			});
		}
	},
	/*--------------------------------------------------------------------------
	TOGGLE BUTTON ON AND OFF: Turn on button on click and turn the others
	in its same group (i.e.: in the menu where it belongs) off. Button
	remains on until it's clicked again or another one is.
	
	HOW TO USE: Pass the index numbers of the buttons to be affected by this
	effect as arguments.
	--------------------------------------------------------------------------*/
	toggleButtonOnOff: function () {
		var clickedButton;      // Button which is clicked.
		var clickedButtonIndex; // Clicked Button Index number.
		var isButtonDetected;   // Tells whether a button is already on or not.
		var groupOfButtons = new Array (); // Index of buttons to be affected.
		
		// First, create an array with the index numbers of the buttons to be
		// affected by this effect.
		for (var i = 0; i < arguments.length; i++) {
			groupOfButtons.push(arguments[i]);
		}
		
		// Then, if one the button with these index numbers is clicked, get its
		// id and index number.
		for (var i = 0; i < groupOfButtons.length; i++) {
			$(gui.button[groupOfButtons[i]]).click(function() {
				clickedButton = "#" + $(this).attr('id');
				clickedButtonIndex = $.inArray(clickedButton, gui.button);
				
				// Search for an active button among the buttons of the group.
				// If found, trigger the alarm and stop searching.
				for (var i = 0; i < groupOfButtons.length; i++) {
					if (gui.isButtonActive[groupOfButtons[i]] === true) {
						isButtonDetected = true;
						break;
					}
				}
				
				// If the alarm was triggered, that is, an already active button
				// was found, then just hide it if it is the case that is the
				// same which was clicked, or if it was another, hide it and
				// turn on the one which was clicked.
				if (isButtonDetected === true) {
					switch (groupOfButtons[i]) {
					case clickedButtonIndex:
						// Create variable/s to be used simply as shortcut/s.
						var buttonToHide = gui.activeButton[clickedButtonIndex];
						$(buttonToHide).css('visibility', 'hidden');
						gui.isButtonActive[clickedButtonIndex] = false;
						break;
					default:
						// Create variable/s to be used simply as shortcut/s.
						var buttonToHide = gui.activeButton[groupOfButtons[i]];
						var buttonToShow = gui.activeButton[clickedButtonIndex];
						$(buttonToHide).css('visibility', 'hidden');
						gui.isButtonActive[groupOfButtons[i]] = false;
						$(buttonToShow).css('visibility', 'visible');
						gui.isButtonActive[clickedButtonIndex] = true;
					}
					isButtonDetected = false;
				
				// Else, if no button was found active, just turn the clicked
				// button on.
				} else {
					$(gui.activeButton[clickedButtonIndex]).css('visibility',
						'visible');
					gui.isButtonActive[clickedButtonIndex] = true;
				}
			});
		}
	},
	/*--------------------------------------------------------------------------
	TURN ALL ElEMENTS OFF: Buttons and other elements are on by default
	to avoid using opacity attribute in css file. Run this function to make
	sure they are off as soon as the document is ready. 
		
	HOW TO USE: Just call the function.
	--------------------------------------------------------------------------*/
	turnAllElementsOff: function () {
		for (var i = 0; i < gui.activeButton.length; i++) {
			$(gui.activeButton[i]).css('visibility', 'hidden');
			$(gui.hoveredButton[i]).css('opacity', 0);
			$(gui.profile[i]).css('visibility', 'hidden');
		}
	},
	/*--------------------------------------------------------------------------
	VALIDATE: Validate a form by making sure its input fields are not empty or
	lack the required characters. This validation it's meant to provide just a
	first layer of security and so it should never be used alone. The real
	validation must be done on the server side by Django's built-in method.
	
	HOW TO USE: Pass the index number of the button which triggers the action
	and the form's array. Then pass two other arrays: one with the keys you want
	to check the inputs against and the other one with the messages you want to
	be displayed in the input fields in case of error as strings. Finally, pass
	the variable in which you want to store the resulting boolean into.
	
	Parameters:
	
	buttonIndex - The index number of the button which triggers the action.
	form        - The form's array which contains all its components.
	keys        - An array containing the seach keys used to check the inputs.
	errors      - An array containing the error messages as strings.
	formIndex   - The variable which will store the resulting boolean.
	--------------------------------------------------------------------------*/
	validate: function (buttonIndex, form, keys, errors, formIndex) {
		var isValid = new Array ();
		
		for (var i = 0; i < form.length; i++) {
			isValid.push(false);
		}
		
		$(gui.button[buttonIndex]).click(function() {
			var value;
			
			for (var i = 0; i < form.length; i++) {
				value = $(form[i]).val();
				if ((keys[i]).test(value) === true) {
					isValid[i] = true;
				} 
				else {
					isValid[i] = false;
					gui.warnInInput(form[i], errors[i]);
				}
			}
			
			for (var i = 0; i < isValid.length; i++) {
				if (isValid[i] === false) {
					gui.isFormValid[formIndex] = false;
					break;
				} else if (i === isValid.length - 1) {
					gui.isFormValid[formIndex] = true;
				}
			}
			// Prevent submit buttons from reloading the page.
			return false;
		});
	},
	/*--------------------------------------------------------------------------
	WARN IN INPUT: Display a red warning message in each input field and delete
	it when clicked.
	
	HOW TO USE: Call this function by passing two arrays as parameters: the one
	containing the inputs and then the one which has the messages to be
	displayed on each.
	
	Parameters:
	
	input    - The array containing the input fields.
	messages - The array containing the messages to be displayed in each input
	           field.
	--------------------------------------------------------------------------*/
	warnInInput: function (input, messages) {
		$(input).val('');
		$(input).attr('placeholder', messages);
		$(input).animate({'color':'red'}, 0,
		function() {
			var color;
			var opacity;
			
			// Fix for Webkit.
			$(this).addClass('warning');
			
			// Replace the changed color with the original when user
			// types on input.
			color = $('form').css('color');
			$(this).keydown(function() {
				$(this).css('color', color);
				// Fix for Webkit.
				$(this).removeClass('warning');
			});
		});
	}
};
/*******************************************************************************
RUN: Run the code.
*******************************************************************************/
$(document).ready(function () {
	var csrftoken;
	
	// Hide JavaScript warning.
	$('body>div:last-child').hide();
	
	// Hide glow.
	$('.glow').hide();
	
	// Reset browser's scroll for proper layout display.
	window.scroll(0, 0);
	
	// IMPORT:
	/*--------------------------------------------------------------------------
	CROSS SITE REQUEST FORGERY PROTECTION: functions provided by Django for
	protecting against "Cross Site Request Forgeries". Read more about it at:
	https://docs.djangoproject.com/en/dev/ref/csrf/#ajax
	SECURITY UPDATE: https://www.djangoproject.com/weblog/2011/feb/08/security/
	--------------------------------------------------------------------------*/
	$.ajaxSetup({ 
		beforeSend: function(xhr, settings) {
			function getCookie(name) {
				var cookieValue = null;
				var cookies;
				var cookie;
				if (document.cookie && document.cookie != '') {
					cookies = document.cookie.split(';');
					for (var i = 0; i < cookies.length; i++) {
						cookie = jQuery.trim(cookies[i]);
						// Does this cookie string begin with the name we want?
						if (cookie.substring(0, name.length + 1) ==
							(name + '=')) {
								cookieValue = decodeURIComponent(
									cookie.substring(name.length + 1)
								);
								break;
						}
					}
				}
				return cookieValue;
			}
			if (!(/^http:.*/.test(settings.url) ||
				/^https:.*/.test(settings.url))) {
					// Only send the token to relative URLs i.e. locally.
					xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
			}
		}
	});
	/*---------------------------<< END OF CSRFP >>---------------------------*/
	
	if (!Detector.webgl) {
		Detector.addGetWebGLMessage();
	} else {
		startLoading();
	}
});

var startLoading = function () {
	var artUrl = staticUrl + 'art/img/3d/'; // URL path shorcut.
	
	// Save the correct mouse wheel event name for the browser render engine.
	// 'wheel': All modern browsers render engines (Gecko, Webkit, Trident).
	// 'mousewheel': Trident and older Webkit versions.
	// 'DOMMouseScroll': Older Gecko versions.
	gui.mouseWheelEvent = 'onwheel' in window ? 'wheel' :
		document.onmousewheel !== undefined ? 'mousewheel':
		'DOMMouseScroll';
	
	// Make the loader text twinkle while loading the backgroundScene.
	var flashLoadingMessage = setInterval(function () {
		$('#loaderText').fadeIn("slow", function () {
			$(this).fadeOut("slow");
		});
	}, 800);
	
	// Load models: scene.
	trid.loadModel(artUrl + 'obj/sky.json');                       // Index: 0
	trid.loadModel(artUrl + 'obj/valley.json');                    // Index: 1
	trid.loadModel(artUrl + 'obj/river.json');                     // Index: 2
	trid.loadModel(artUrl + 'obj/foam.json');                      // Index: 3
	trid.loadModel(artUrl + 'obj/drops.json');                     // Index: 4
	trid.loadModel(artUrl + 'obj/smokeLeft.json');                 // Index: 5
	trid.loadModel(artUrl + 'obj/smokeRight.json');                // Index: 6
	trid.loadModel(artUrl + 'obj/watermill.json');                 // Index: 7
	trid.loadModel(artUrl + 'obj/watermillPlatform.json');         // Index: 8
	trid.loadModel(artUrl + 'obj/windmill.json');                  // Index: 9
	trid.loadModel(artUrl + 'obj/sheep.01.json');                  // Index: 10
	trid.loadModel(artUrl + 'obj/sheep.02.json');                  // Index: 11
	trid.loadModel(artUrl + 'obj/cloud.01.json');                  // Index: 12
	trid.loadModel(artUrl + 'obj/cloud.02.json');                  // Index: 13
	trid.loadModel(artUrl + 'obj/cloud.03.json');                  // Index: 14
	trid.loadModel(artUrl + 'obj/cloud.04.json');                  // Index: 15
	trid.loadModel(artUrl + 'obj/cloud.05.json');                  // Index: 16
	trid.loadModel(artUrl + 'obj/cloud.06.json');                  // Index: 17
	trid.loadModel(artUrl + 'obj/cloud.07.json');                  // Index: 18
	trid.loadModel(artUrl + 'obj/cloud.08.json');                  // Index: 19
	trid.loadModel(artUrl + 'obj/cloud.09.json');                  // Index: 20
	trid.loadModel(artUrl + 'obj/cloud.10.json');                  // Index: 21
	trid.loadModel(artUrl + 'obj/cloud.11.json');                  // Index: 22
	trid.loadModel(artUrl + 'obj/cloud.12.json');                  // Index: 23
	trid.loadModel(artUrl + 'obj/cloud.13.json');                  // Index: 24
	trid.loadModel(artUrl + 'obj/TATLogoTop.json');                // Index: 25
	trid.loadModel(artUrl + 'obj/TATLogoBannerLeft.json');         // Index: 26
	trid.loadModel(artUrl + 'obj/TATLogoBannerRight.json');        // Index: 27
	trid.loadModel(artUrl + 'obj/TATLogoBottom.json');             // Index: 28
	trid.loadModel(artUrl + 'obj/TATLogoTopHighlight.json');       // Index: 29
	trid.loadModel(artUrl + 'obj/TATLogoBottomHighlight.json');    // Index: 30
	trid.loadModel(artUrl + 'obj/tree.json');                      // Index: 31
	trid.loadModel(artUrl + 'obj/cliff.json');                     // Index: 32
	trid.loadModel(artUrl + 'obj/grassAndFlowers.01.json');        // Index: 33
	trid.loadModel(artUrl + 'obj/grassAndFlowers.02.json');        // Index: 34
	trid.loadModel(artUrl + 'obj/grassAndFlowers.03.json');        // Index: 35
	trid.loadModel(artUrl + 'obj/grassAndFlowers.04.json');        // Index: 36
	
	// Load models: characters and weapons.
	trid.loadModel(artUrl + 'chr/akaneOTL.json');                  // Index: 37
	trid.loadModel(artUrl + 'chr/akaneStaffOTL.json');             // Index: 38
	trid.loadModel(artUrl + 'chr/ryuuOTL.json');                   // Index: 39
	trid.loadModel(artUrl + 'chr/ryuuSwordOTL.json');              // Index: 40
	trid.loadModel(artUrl + 'chr/taroOTL.json');                   // Index: 41
	trid.loadModel(artUrl + 'chr/taroHammerOTL.json');             // Index: 42
	trid.loadModel(artUrl + 'chr/chikaOTL.json');                  // Index: 43
	trid.loadModel(artUrl + 'chr/chikaCrossbowOTL.json');          // Index: 44
	
	// Load textures: scene.
	trid.loadTexture(artUrl + 'tex/sky.png');                      // Index: 0
	trid.loadTexture(artUrl + 'tex/valley.png');                   // Index: 1
	trid.loadTexture(artUrl + 'tex/river.png');                    // Index: 2            
	trid.loadTexture(artUrl + 'tex/foam.png');                     // Index: 3
	trid.loadTexture(artUrl + 'tex/drops.png');                    // Index: 4
	trid.loadTexture(artUrl + 'tex/smokeLeft.png');                // Index: 5
	trid.loadTexture(artUrl + 'tex/smokeRight.png');               // Index: 6
	trid.loadTexture(artUrl + 'tex/sheep1.png');                   // Index: 7
	trid.loadTexture(artUrl + 'tex/sheep2.png');                   // Index: 8
	trid.loadTexture(artUrl + 'tex/clouds.png');                   // Index: 9
	trid.loadTexture(artUrl + 'tex/TATLogoTopAndBottom.png');      // Index: 10
	trid.loadTexture(artUrl + 'tex/TATLogoLeftBanner.png');        // Index: 11
	trid.loadTexture(artUrl + 'tex/TATLogoRightBanner.png');       // Index: 12
	trid.loadTexture(artUrl + 'tex/TATLogoTopHighlight.png');      // Index: 13
	trid.loadTexture(artUrl + 'tex/TATLogoBottomHighlight.png');   // Index: 14
	trid.loadTexture(artUrl + 'tex/tree.png');                     // Index: 15
	trid.loadTexture(artUrl + 'tex/cliff.png');                    // Index: 16
	trid.loadTexture(artUrl + 'tex/grassLight.png');               // Index: 17
	trid.loadTexture(artUrl + 'tex/grassPenumbra.png');            // Index: 18
	trid.loadTexture(artUrl + 'tex/grassShadow.png');              // Index: 19
	trid.loadTexture(artUrl + 'tex/margaritaWhite.png');           // Index: 20
	trid.loadTexture(artUrl + 'tex/margaritaYellow.png');          // Index: 21
	trid.loadTexture(artUrl + 'tex/tulipOldpink.png');             // Index: 22
	
	// Load textures: characters and weapons.
	trid.loadTexture(artUrl + 'tex/akane.png');                    // Index: 23
	trid.loadTexture(artUrl + 'tex/akaneStaff.png');               // Index: 24
	trid.loadTexture(artUrl + 'tex/ryuu.png');                     // Index: 25
	trid.loadTexture(artUrl + 'tex/ryuuSword.png');                // Index: 26
	trid.loadTexture(artUrl + 'tex/taro.png');                     // Index: 27
	trid.loadTexture(artUrl + 'tex/taroHammer.png');               // Index: 28
	trid.loadTexture(artUrl + 'tex/chika.png');                    // Index: 29
	trid.loadTexture(artUrl + 'tex/chikaCrossbow.png');            // Index: 30
	
	// Check loading progress after some time. Once everything was loaded, laod
	// the models to be used for the character viewer and then once those are
	// loaded run the transition to the backgroundScene. Modes must be loaded
	// again because the clone () property doesn't seem to work when moving such
	// clone into another canvas. Also, to mantain proper index order, we have
	// make sure these last ones are loaded AFTER the first ones were because
	// being loaded from the same files means that they take the same time to
	// load and as consequence, the code that is supposed to work on one model,
	// affects the other, and vice versa.
	setTimeout(function () {
		trid.checkLoading(function () {
			
			// Load models: characters and weapons.
			trid.loadModel(artUrl + 'chr/akane.json');             // Index: 45
			trid.loadModel(artUrl + 'chr/akaneStaff.json');        // Index: 46
			trid.loadModel(artUrl + 'chr/chika.json');             // Index: 47
			trid.loadModel(artUrl + 'chr/chikaCrossbow.json');     // Index: 48
			trid.loadModel(artUrl + 'chr/ryuu.json');              // Index: 49
			trid.loadModel(artUrl + 'chr/ryuuSword.json');         // Index: 50
			trid.loadModel(artUrl + 'chr/taro.json');              // Index: 51
			trid.loadModel(artUrl + 'chr/taroHammer.json');        // Index: 52
			
			// Load models: effects.
			trid.loadModel(artUrl + 'chr/chargeCoil.json');        // Index: 53
			trid.loadModel(artUrl + 'chr/energyCharge.json');      // Index: 54
			trid.loadModel(artUrl + 'chr/energyBeam.json');        // Index: 55
			trid.loadModel(artUrl + 'chr/energyBlast.json');       // Index: 56
			trid.loadModel(artUrl + 'chr/energyBolt.json');        // Index: 57
			trid.loadModel(artUrl + 'chr/energyBoltGlow.json');    // Index: 58
			trid.loadModel(artUrl + 'chr/energyShield.json');      // Index: 59
			trid.loadModel(artUrl + 'chr/iceBolt.json');           // Index: 60
			
			// Load textures: characters and weapons.
			trid.loadTexture(artUrl + 'tex/akane.png');            // Index: 31
			trid.loadTexture(artUrl + 'tex/akaneStaff.png');       // Index: 32
			trid.loadTexture(artUrl + 'tex/chika.png');            // Index: 33
			trid.loadTexture(artUrl + 'tex/chikaCrossbow.png');    // Index: 34
			trid.loadTexture(artUrl + 'tex/ryuu.png');             // Index: 35
			trid.loadTexture(artUrl + 'tex/ryuuSword.png');        // Index: 36
			trid.loadTexture(artUrl + 'tex/taro.png');             // Index: 37
			trid.loadTexture(artUrl + 'tex/taroHammer.png');       // Index: 38
			
			// Load textures: effects.
			trid.loadTexture(artUrl + 'tex/chargeCoil.png');       // Index: 39
			trid.loadTexture(artUrl + 'tex/energyCharge.png');     // Index: 40
			trid.loadTexture(artUrl + 'tex/energyBeam.png');       // Index: 41
			trid.loadTexture(artUrl + 'tex/energyBlast.png');      // Index: 42
			trid.loadTexture(artUrl + 'tex/energyBolt.png');       // Index: 43
			trid.loadTexture(artUrl + 'tex/energyBoltGlow.png');   // Index: 44
			trid.loadTexture(artUrl + 'tex/energyShield.png');     // Index: 45
			trid.loadTexture(artUrl + 'tex/iceBolt.png');          // Index: 46
			
			// Once everything is loaded, run the transition.
			trid.checkLoading(function () {
				// Stop flashing the "Loading" message.
				clearInterval(flashLoadingMessage);
				
				blendTransition('.loader', '.flash', 1000, 3000);
			
				$('.glow').show();
				$('.glow').fadeTo(1, 1);
				$('.glow').effect('scale', {percent: 5000}, 1000);
			});
		});
	}, 5000);
	
	// Load and assign gui images according to screen size.
	gui.loadImages();
	
	// Hide GUI: set boards and menus' initial position once images are loaded.	
	$(window).load(function () {
		gui.setInitialLocation('.leftContainer>div>div',
			'.leftContainer>div>div');
		gui.setInitialLocation('.rightContainer>div>div', '.characterMenu');
	});
};

// Blend the transition between the loading stage and the loaded backgroundScene
// with a white screen. In the middle of it, hide loading screen and run the
// backgroundScene as well as the GUI.
var blendTransition = function (toBeHidden, flash, fadeInMS, fadeToMS) {
	$(flash).fadeTo(fadeInMS, 1, function () {
		$(toBeHidden).hide();
		$(flash).fadeTo(fadeToMS, 0);
		setScene();
	});
};

// Run this once everything was loaded and in the middle of the transition.
var setScene = function () {
	
	// CREATE THE SCENE
	trid.backgroundScene = new THREE.Scene();
	
	// CREATE A CAMERA: zoom it out from the model a bit. The aspect ratio
	// of the camera should use the canvas dimensions, that is, the horizontal
	// space available in the browser window divided by canvas containers'
	// height.
	trid.backgroundCamera = new THREE.PerspectiveCamera(49.134,
	screen.width / screen.height, 0.1, 20000);
	switch ((screen.width / screen.height).toString().substr(0, 4)) {
	case "1.25":
	case "1.33":
		trid.backgroundCamera.setLens(35, 21.5);
		break;
	case "1.6":
		trid.backgroundCamera.setLens(35, 19);
		break;
	case "1.77":
		trid.backgroundCamera.setLens(35, 20.2);
		break;
	}
	
	// Move the backgroundCamera away from origin only, not its target.
	trid.backgroundCamera.position.x = 0;
	trid.backgroundCamera.position.y = 71; // Final position is 11.74.
	trid.backgroundCamera.position.z = 34.75;
	
	// CREATE AMBIENT LIGHT.
	trid.backgroundAmbientLight = new THREE.AmbientLight(0xA6AF90);
	trid.backgroundScene.add(trid.backgroundAmbientLight);
	
	// CREATE SPOT LIGHT: for lighting watermill and platform only.
	
	trid.spotLights[0] = new THREE.SpotLight(0x797C6F,  0.4, 0, Math.PI/3,
		10.0);
	
	// Position both light source and target.
	trid.spotLights[0].position.set(-17.44143, 24.52101, -80.22354);
	trid.spotLights[0].target.position.set(-4.44072, 11.34973, -85.46973);

	// Add directional light source to the backgroundScene.
	trid.backgroundScene.add(trid.spotLights[0]);

	// CREATE SPOT LIGHT: for lightning windmill only.
	trid.spotLights[1] = new THREE.SpotLight(0xC2C2C2,  0.1, 0, Math.PI/10,
		10.0);
	
	// Position both light source and target.
	trid.spotLights[1].position.set(-10, 33.88198, -62.7576);
	trid.spotLights[1].target.position.set(-16, 16.05454, -83.39265);
	
	// Add directional light source to the backgroundScene.
	trid.backgroundScene.add(trid.spotLights[1]);
			
	// LOAD MODELS UVS: all of them.
	for (var i = 0; i < trid.models.length; i++) {
		trid.loadUvs(i);
	}
	
	// APPLY MATERIALS TO MODELS: background, river, foam, drops, smokeLeft
	// and smokeRight.
	for (var i = 0; i < 7; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i]});
		trid.models[i].material.alphaTest = 0.8;
		trid.models[i].material.side = THREE.DoubleSide;
	}
	
	// APPLY MATERIALS TO MODELS: watermill and platform.
	for (var i = 7; i < 9; i++) {
		trid.models[i].material = new THREE.MeshLambertMaterial();
	}
	
	// APPLY MATERIALS TO MODELS: windmill.
	trid.models[9].material = new THREE.MeshFaceMaterial(new Array (
		new THREE.MeshLambertMaterial(),
		new THREE.MeshBasicMaterial()
	));
		
	// Configure materials: windmill.
	trid.models[9].material.materials[0].shading = THREE.FlatShading;
	trid.models[9].material.materials[0].ambient = new THREE.Color(0.8, 0.8,
		0.8);
	trid.models[9].material.materials[0].color = new THREE.Color(1, 1, 1);
	trid.models[9].material.materials[1].ambient = new THREE.Color(1, 1, 1);
	trid.models[9].material.materials[1].color = new THREE.Color(0.8, 0.8, 0.8);
	
	// APPLY MATERIALS TO MODELS: sheep.
	for (var i = 10; i < 12; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 3]});
		trid.models[i].material.alphaTest = 0.8;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.skinning = true;
	}
	
	// APPLY MATERIALS TO MODELS: clouds.
	for (var i = 12; i < 25; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[9]});
		trid.models[i].material.alphaTest = 0.9;
		trid.models[i].material.side = THREE.DoubleSide;
	}
	
	// APPLY MATERIALS TO MODELS: TATLogoTop and TATLogoBottom.
	for (var i = 25; i < 29; i += 3) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[10]});
		trid.models[i].material.alphaTest = 0;
		trid.models[i].material.transparent = true;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.opacity = 0;
	}
		
	// APPLY MATERIALS TO MODELS: TATLogoBannerLeft and TATLogoBannerRight.
	for (var i = 26; i < 28; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 15]});
		trid.models[i].material.alphaTest = 0;
		trid.models[i].material.transparent = true;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.opacity = 0;
	}
	
	// APPLY MATERIALS TO MODELS: TAT logo top and bottom highlights.
	for (var i = 29; i < 31; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 16]});
		trid.models[i].material.alphaTest = 0;
		trid.models[i].material.transparent = true;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.opacity = 0;
	}
	
	// APPLY MATERIALS TO MODELS: tree and cliff.
	for (var i = 31; i < 33; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 16]});
		trid.models[i].material.alphaTest = 0.8;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.skinning = true;
	}
	
	// APPLY MATERIALS TO MODELS: grassAndFlowers_01.
	trid.models[33].material = new THREE.MeshFaceMaterial(new Array (
		new THREE.MeshBasicMaterial({map: trid.textures[21]}),
		new THREE.MeshBasicMaterial({map: trid.textures[17]}),
		new THREE.MeshBasicMaterial({map: trid.textures[19]}),
		new THREE.MeshBasicMaterial({map: trid.textures[18]}),
		new THREE.MeshBasicMaterial({map: trid.textures[20]})
	));
	
	// Configure materials: grassAndFlowers_01.
	for (var i = 0; i < 5; i++) {
		trid.models[33].material.materials[i].alphaTest = 0.8;
		trid.models[33].material.materials[i].side = THREE.DoubleSide;
		trid.models[33].material.materials[i].skinning = true;
	}
	
	// APPLY MATERIALS TO MODELS: grassAndFlowers_02.
	trid.models[34].material = new THREE.MeshFaceMaterial(new Array (
		new THREE.MeshBasicMaterial({map: trid.textures[22]}),
		new THREE.MeshBasicMaterial({map: trid.textures[18]}),
		new THREE.MeshBasicMaterial({map: trid.textures[17]}),
		new THREE.MeshBasicMaterial({map: trid.textures[19]}),
		new THREE.MeshBasicMaterial({map: trid.textures[20]}),
		new THREE.MeshBasicMaterial({map: trid.textures[21]})
	));
	
	// Configure materials: grassAndFlowers_02.
	for (var i = 0; i < 6; i++) {
		trid.models[34].material.materials[i].alphaTest = 0.8;
		trid.models[34].material.materials[i].side = THREE.DoubleSide;
		trid.models[34].material.materials[i].skinning = true;
	}
	
	// APPLY MATERIALS TO MODELS: grassAndFlowers_03 and grassAndFlowers_04.
	for (var i = 35; i < 37; i++) {
		trid.models[i].material = new THREE.MeshFaceMaterial(new Array (
			new THREE.MeshBasicMaterial({map: trid.textures[22]}),
			new THREE.MeshBasicMaterial({map: trid.textures[17]}),
			new THREE.MeshBasicMaterial({map: trid.textures[18]}),
			new THREE.MeshBasicMaterial({map: trid.textures[19]}),
			new THREE.MeshBasicMaterial({map: trid.textures[20]}),
			new THREE.MeshBasicMaterial({map: trid.textures[21]})
		));
		
		// Configure materials: grassAndFlowers_03 and grassAndFlowers_04.
		for (var j = 0; j < 6; j++) {
			trid.models[i].material.materials[j].alphaTest = 0.8;
			trid.models[i].material.materials[j].side = THREE.DoubleSide;
			trid.models[i].material.materials[j].skinning = true;
		}
	}
	
	// APPLY MATERIALS TO MODELS: characters and weapons.
	for (var i = 37; i < 45; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 14]});
		trid.models[i].material.alphaTest = 0.4;
		trid.models[i].material.transparent = true;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.skinning = true;
	}	
	
	// ADD MODELS TO THE SCENE.
	for (var i = 0; i < 45; i++) {
		trid.backgroundScene.add(trid.models[i]);
	}
	
	// ENABLE MODEL ANIMATIONS: sheeps.
	for (var i = 10; i < 12; i++) {
		trid.enableAnimation(i, 0);
	}
	
	// ENABLE MODEL ANIMATIONS: three, cliff, all grassAndFlowers, characters
	// and their weapons.
	trid.enableAnimation(31, 0);
	trid.enableAnimation(32, 0);
	trid.enableAnimation(33, 0);
	trid.enableAnimation(34, 0);
	trid.enableAnimation(35, 0);
	trid.enableAnimation(36, 0);
	trid.enableAnimation(37, 0);
	trid.enableAnimation(38, 0);
	trid.enableAnimation(39, 0);
	trid.enableAnimation(40, 0);
	trid.enableAnimation(41, 0);
	trid.enableAnimation(42, 0);
	trid.enableAnimation(43, 0);
	trid.enableAnimation(44, 0);
	
	// PLACE MODELS INTO POSITION. 
	
	// Create empty helpers and save them into array to be used for making
	// watermill, platform and windmill always look at the backgroundCamera.
	trid.emptyHelpers.push(new THREE.Object3D());
	trid.emptyHelpers.push(new THREE.Object3D());
	
	// Set empty helpers into teir positions.
	trid.emptyHelpers[0].position.set(-4.44072, 11.34973, -85.46973);
	trid.emptyHelpers[1].position.set(-16.05454, 16.48871, -83.39265);
	
	// Add empty helpers to the backgroundScene.
	trid.backgroundScene.add(trid.emptyHelpers[0]);
	trid.backgroundScene.add(trid.emptyHelpers[1]);
	
	// Link watermill, platform and windmill to empty helpers.
	setTimeout(function () {
		trid.emptyHelpers[0].add(trid.models[7]);
		trid.emptyHelpers[0].add(trid.models[8]);
		trid.emptyHelpers[1].add(trid.models[9]);
	}, 0);
	
	// Set sky and valley into their position.
	trid.models[0].position.set(2.09423, 39.02464, -87.67120);
	trid.models[1].position.set(2.09423, 4.88841, -87.47120);
	
	// Set watermill, platform and windmill into their positions.
	trid.models[7].rotation.x = 0 * (Math.PI / 180);
	trid.models[7].rotation.y = 193.038 * (Math.PI / 180);
	trid.models[7].rotation.z = 90 * (Math.PI / 180);
	trid.models[8].rotation.x = 0 * (Math.PI / 180);
	trid.models[8].rotation.y = 13.037 * (Math.PI / 180);
	trid.models[8].rotation.z = 0 * (Math.PI / 180);
	trid.models[9].rotation.x = 0 * (Math.PI / 180);
	trid.models[9].rotation.y = 45 * (Math.PI / 180);
	trid.models[9].rotation.z = 0 * (Math.PI / 180);
	
	// Set clouds into their positions and create vector to get their global
	// coordinates during animation.
	trid.models[12].position.set(-28.32144, 36.72701, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[13].position.set(28.533, 33.27302, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[14].position.set(30.32374, 56.78553, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[15].position.set(18.5147, 78.93318, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[16].position.set(-29.90762, 70.31664, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[17].position.set(-90, 35, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[18].position.set(-90, 30, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[19].position.set(80, 24, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[20].position.set(-70, 23, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[21].position.set(-40, 22, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[22].position.set(60, 18, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[23].position.set(34, 18, -87.6);
	trid.locators.push(new THREE.Vector3());
	trid.models[24].position.set(-20, 18, -87.6);
	trid.locators.push(new THREE.Vector3());
	
	// Set logo into position.
	trid.models[25].position.set(0, 85.86136, -86);
	trid.models[26].position.set(-16.34769, 73, -85.5);
	trid.models[27].position.set(16.34769, 73, -85.5);
	trid.models[28].position.set(0, 76.60574, -85);
	trid.models[29].position.set(0, 83.2, -85.5);
	trid.models[30].position.set(0, 75.9, -84.5);	
	
	// Set characters and their weapons into position.	
	trid.models[37].position.set(-4.02979, 8.77296, 2.03149);
	trid.models[38].position.set(-4.02979, 8.77296, 2.03149);
	trid.models[39].position.set(-9.258, 8.408, -0.378);
	trid.models[40].position.set(-9.258, 8.408, -0.378);
	trid.models[41].position.set(-0.456, 7.723, -2.728);
	trid.models[42].position.set(-0.456, 7.723, -2.728);
	trid.models[43].position.set(-0.6793, 11.6967, -3.0679);
	trid.models[44].position.set(-0.6793, 11.6967, -3.0679);
	
	// SET RENDERER.
	// Set the canvas width to be the same as the available space in the browser
	// window and its height equal to the canvas container's.
	trid.backgroundRenderer = new THREE.WebGLRenderer({antialias:true});
	trid.backgroundRenderer.setSize(screen.width,
		screen.height);
	
	// Append rendered to document body.
	var backgroundCanvasContainer =
		document.getElementById('backgroundCanvasContainer');
	backgroundCanvasContainer.appendChild(trid.backgroundRenderer.domElement);
	
	// SET ORBIT CONTROLS.
	trid.backgroundControls = new THREE.OrbitControls(trid.backgroundCamera,
		trid.backgroundRenderer.domElement);
	trid.backgroundControls.noKeys = true;
	trid.backgroundControls.target.set(0,71,0);
	trid.backgroundScene.add(trid.backgroundCamera);
	
	// SET BACKGROUND COLOR.
	trid.backgroundRenderer.setClearColor(0x333F47, 1);
	
	// SET CLOCKS: for timing animations.
	
	// Add a locator for the backgroundCamera.
	trid.locators.push(new THREE.Vector3());
	
	// Start backgroundCamera animation.
	setTimeout(function () {
		trid.cameraTimer.start();
	}, 8000);
	
	// Start title fade in.
	setTimeout(function () {
		trid.titleFadeInTimer.start();
	}, 2000);
	
	// start title fade out.
	setTimeout(function () {
		trid.titleFadeOutTimer.start();
	}, 8500);
	
	// RUN ANIMATION.
	animateBackground();
	
	// TOGGLE BROWSER' SCROLLBARS ON AND OFF.
	
	// Control the scrollbars behaivor by changing the html body's overflow css
	// property when comparing the window's dimensions to the minimal required
	// for optimal navigation.
	var toggleBrowserScrollbarsOnOff = function () {
		// Show browser's horizontal scrollbar if the window is too narrow for
		// the main menu to be visible. Do this by toggling html body's overflow
		// property on and off. Also, make sure it only happens when
		// the page is not scrolled horizontally.
		if (parseInt(window.innerWidth) < parseInt(gui.minWidth)) {
			$('body').css({'overflow-x': 'visible'});
		} else if (window.pageXOffset === 0) {
			$('body').css({'overflow-x': 'hidden'});
		}
		
		// Do the same for the vertical scrollbar, but this time taking the
		// "main menu" height as reference. Also, make sure it only happens when
		// the page is not scrolled vertically.
		if (parseInt(window.innerHeight) < parseInt($('.mainMenu').height())) {
			$('body').css({'overflow-y': 'visible'});
		} else if (window.pageYOffset === 0) {
			$('body').css({'overflow-y': 'hidden'});
		}
	}
	
	// Make a first comparition as soon as the background scene renders. Later
	// we invoke this function again on window resize.
	toggleBrowserScrollbarsOnOff();
	
	// Hide the verticall scroll whenever the browser window's height is larger
	// than the "Main Menu" height and the page is not scrolled. Also hide the
	// horizontal scrollbar when window's width is larger than the minimum width
	// and the page is not scrolled.
	window.addEventListener('scroll', function () {
		if (parseInt(window.innerHeight) > parseInt($('.mainMenu').height())) {
			if (window.pageYOffset === 0) {
				$('body').css({'overflow-y': 'hidden'});
			}
		}
		
		if (parseInt(window.innerWidth) > parseInt(gui.minWidth)) {
			if (window.pageXOffset === 0) {
				$('body').css({'overflow-x': 'hidden'});
			}
		}
	}, false);
	
	// RESIZE GUI AND CANVAS ON BROWSER WINDOW ZOOM AND RESIZE.
	
	// Define a function which will adjust the gui and canvas according to the
	// browser's zoom and resize.
	var onWindowResize = function () {
		
		// VARIABLES:
		var scaleRatio;				 // Image scale converter.
		var aRatio;					 // Asp. ratio.
		var lHolder;
		var rHolder;
		var lMargin;
		var rMargin;
		var lrHWidth;
		var fontSize;
		
		// Gecko.
		if (screen.height !== gui.deviceSHeight && screen.width !==
			gui.deviceSWidth && window.devicePixelRatio !== gui.lastDPR) {
				gui.sHeight = screen.height;
				gui.sWidth = screen.width;
		}
		
		// Modern Webkit.
		if (screen.height === gui.deviceSHeight  && screen.width ===
			gui.deviceSWidth && window.devicePixelRatio !== gui.lastDPR) {
				gui.sHeight = screen.height / window.devicePixelRatio;
				gui.sWidth = screen.width / window.devicePixelRatio;
		}
		
		// Old Webkit.
		if (screen.height === gui.deviceSHeight && screen.width ===
			gui.deviceSWidth && window.devicePixelRatio === gui.lastDPR) {
			// Only when zooming, not when rezising the window. 
			if (window.outerHeight === gui.lastOutHeight &&
				window.outerWidth === gui.lastOutWidth) {
				
				// Avoid this from happening when console (or "dev tools" is
				// opened or moved to the size or bottom of the browser).
				if (Math.round(window.innerWidth / window.innerHeight) ===
					Math.round(gui.lastInWidth / gui.lastInHeight)) {
						gui.sHeight = screen.height * (window.innerWidth /
							window.outerWidth);
						gui.sWidth = screen.width * (window.innerWidth /
							window.outerWidth);
				}
			}
		}
		
		// FOR OLD WEBKIT ONLY: Update values so that they can be used on next
		// resize for comparing. It's important to remember that they must be
		// updated after they where used since they will represent the previous
		// values before the last resize.
		gui.lastInHeight = window.innerHeight;
		gui.lastInWidth = window.innerWidth;
		gui.lastOutHeight = window.outerHeight;
		gui.lastOutWidth = window.outerWidth;
		gui.lastDPR = window.devicePixelRatio;
		
		scaleRatio = gui.sHeight * 100 / 1600 / 100;
		aRatio = (gui.sWidth / gui.sHeight).toString().substr(0, 4);
		
		// Since "aRatio" result isn't always accurate, make it match for cases
		// in which it's +-0.01 the desired result.
		if (aRatio === '1.24' || aRatio === '1.26') {
			aRatio = '1.26';
		}
		
		if (aRatio === '1.32' || aRatio === '1.34') {
			aRatio = '1.33';
		}
		
		if (aRatio === '1.59' || aRatio === '1.60' || aRatio === '1.61') {
			aRatio = '1.6';
		}
		
		if (aRatio === '1.76' || aRatio === '1.78') {
			aRatio = '1.77';
		}
		
		// Define values depending on screen aspect ratio for each time
		// browser window zooms in and out.
		switch (aRatio) {
		case '1.25':
		
		case '1.33':
			// Variables: the first two hold rail div and wrapper.
			// These set values come from the original layout.
			lHolder = parseInt(1206 * scaleRatio); 
			rHolder = parseInt(374 * scaleRatio);
			lMargin = parseInt((1206 + 120) * scaleRatio - lHolder);
			rMargin = parseInt((374 + 120) * scaleRatio - rHolder);
			lrHWidth = lHolder + lMargin + rHolder + rMargin;
			// Make sure to update the window's minimal width so that its
			// scrollbars can be toggled just the same way when there is no
			// zoom.
			gui.minWidth = lrHWidth;
			// From the design itself we know that the font size is 1/100 of
			// a 1680 wide layout.
			fontSize = (1680 * 0.01) * gui.sWidth / 1680;
			break;
		
		case '1.6':
			// Variables: the first two hold rail div and wrapper.
			// These set values come from the original layout.
			lHolder = parseInt(1445 * scaleRatio);
			rHolder = parseInt(374 * scaleRatio);
			lMargin = parseInt((1445 + 120) * scaleRatio - lHolder);
			rMargin = parseInt((374 + 120) * scaleRatio - rHolder);
			lrHWidth = lHolder + lMargin + rHolder + rMargin + 45;
			// Make sure to update the window's minimal width so that its
			// scrollbars can be toggled just the same way when there is no
			// zoom.
			gui.minWidth = lrHWidth;
			// From the design itself we know that the font size is 1/100 of
			// a 1680 wide layout.
			fontSize = (1680 * 0.01) * gui.sWidth / 1680;
			break;
		
		case '1.77':
			// Variables: the first two hold rail div and wrapper.
			// These set values come from the original layout.
			lHolder = parseInt(1445 * scaleRatio);
			rHolder = parseInt(374 * scaleRatio);
			lMargin = parseInt((1445 + 120) * scaleRatio - lHolder);
			rMargin = parseInt((374 + 120) * scaleRatio - lMargin);
			lrHWidth = lHolder + lMargin + rHolder + rMargin + 45;
			// Make sure to update the window's minimal width so that its
			// scrollbars can be toggled just the same way when there is no
			// zoom.
			gui.minWidth = lrHWidth;
			// From the design itself we know that the font size is 1/100 of
			// a 1680 wide layout.
			fontSize = (1680 * 0.008) * gui.sWidth / 1680;
			break;
		}
		
		// Assign values to DOM elements to adjust the layout according to
		// each zoom level.
		$('body>div>canvas').height(gui.sHeight);
		$('body>div>canvas').width(gui.sWidth);
		$('.floatContainer>div:last-child').width(gui.sWidth);
		$('.floatContainer>div:last-child').css('top', 900 * gui.sHeight /
			1050);
		$('#viewerCanvasContainer').css('background-size', '100%' + ' '
			+ '100%');
		$('#viewerCanvasContainer>canvas').css('width', '100%');
		$('.flash').hide();
		
		$('.floatContainer').css('min-width', lrHWidth);
		$('.backgroundCanvasContainer').css('min-width', lrHWidth);
		$('.leftContainer').css('width', (lHolder + lMargin) * 100 /
			gui.sWidth + '%');
		$('.leftContainer').css('min-width', lHolder + lMargin / 2);
		$('.leftContainer>div').css('width', lHolder);
		$('.leftContainer>div>div>div').css('width', lHolder);
		$('.rightContainer').css('width', (rHolder + rMargin) * 100 /
			gui.sWidth + '%');
		$('.rightContainer').css('min-width', rHolder + rMargin / 2);
		$('.rightContainer>div').width(rHolder);
		$('.rightContainer>div>div>div').width(rHolder);
		
		$('html').css('font-size', fontSize);
		
		// Resize prompt's images.
		$('.promptHandleImage').height(120 * gui.sHeight / 1600);
		$('.promptHandleImage').width(1336 * gui.sWidth / 2560);
		$('#promptPaperImage').height(1090 * gui.sHeight / 1600);
		$('#promptPaperImage').width(1016 * gui.sWidth / 2560);
		
		// Resize prompt's container.
		$('.promptContainer').height((1090 * gui.sHeight / 1600) +
			(120 * gui.sHeight / 1600));
		$('.promptContainer').width(1336 * gui.sWidth / 2560);
		
		// Center paper image on container.
		$('#promptPaperImage').css('left', ((1336 * gui.sWidth / 2560) -
			(1016 * gui.sWidth / 2560))
				/ 2);
		$('#promptPaperImage').css('top', (120 * gui.sHeight / 1600)
			/ 2);
		
		// Resize Flattr image.
		$('.promptContainer>div>div:nth-child(2)>a:nth-child(11)>' +
			'img').height(20 * gui.sHeight / 1050);
		$('.promptContainer>div>div:nth-child(2)>a:nth-child(11)>' +
			'img').width(93 * gui.sWidth / 1680);
		
		// Resize Paypal image.
		$('.promptContainer>div>div:nth-child(2)>form:nth-child(13)>' +
			'input[type=image]').height(47 * gui.sHeight / 1050);
		$('.promptContainer>div>div:nth-child(2)>form:nth-child(13)>' +
			'input[type=image]').width(147 * gui.sWidth / 1680);
		
		// Resize "Download Now" button's image.
		$('.normalDownloadNowButtonImage').height(38 * gui.sHeight /
			1600);
		$('.normalDownloadNowButtonImage').width(801 * gui.sWidth / 2560);
		
		// Resize prompt's "Cancel" button. Only one needs to be resized,
		// the other will resize accordingly.
		$('.normalCancelButtonImage').height(30 * gui.sHeight/ 1600);
		$('.normalCancelButtonImage').width(801 * gui.sWidth/ 2560);
		
		// Do the same for "Enter" and "Register" buttons.
		$('#normalEnterButtonImage').height(38 * gui.sHeight/ 1600);
		$('#normalEnterButtonImage').width(801 * gui.sWidth/ 2560);
		$('#normalRegisterButtonImage').height(47 * gui.sHeight/ 1600);
		$('#normalRegisterButtonImage').width(801 * gui.sWidth/ 2560);
		
		// Create a function for redifining DOM elements initial location
		// each time their sizes changes as the browser zooms in and out.
		var resetInitialLocation = function (element, heightOf) {
			var elementTop = parseInt($(element).css('top'));
			var isAnimated = $(element).is(':animated');
			
			// Make sure to only make this change when element is hidden in
			// its initial location and is not beign animated at the moment.
			if (elementTop < 0 && isAnimated === false) {
				gui.setInitialLocation(element, heightOf);
			}
		}
		
		// Redefine boards initial location (when they are hidden away above
		// the browser' window) each time brwoser zooms in/out.
		for (var i = 0; i < gui.board.length; i++) {
			resetInitialLocation(gui.board[i], gui.board[i]);
		}
		
		// Do the same for the menues.
		resetInitialLocation(gui.characterMenu, gui.characterMenu);
		resetInitialLocation(gui.mainMenu, gui.mainMenu);
		
		// Recalculate and relocate scrolls content according to changes in
		// the scroll thumb location (which itself depends on the
		// container's height). Content's new location = its current
		// location * thumb's new location / thumb's old location. This
		// must be calculated before calculate and relocate the scroll thumb
		// so we can get the original relationship between their locations.
		$(gui.newsScroll[6]).css('top',
			parseInt($(gui.newsScroll[6]).css('top')) *
				parseInt($(gui.newsScroll[2]).css('top')) *
					((($(gui.newsScroll[0]).height()) * 92.1) / 100)
						/ $(gui.newsScroll[1]).height()
							/ parseInt($(gui.newsScroll[2]).css('top')));
		
		$(gui.faqScroll[6]).css('top',
			parseInt($(gui.faqScroll[6]).css('top')) *
				parseInt($(gui.faqScroll[2]).css('top')) *
					((($(gui.faqScroll[0]).height()) * 92.1) / 100)
						/ $(gui.faqScroll[1]).height()
							/ parseInt($(gui.faqScroll[2]).css('top')));
		
		$(gui.libraryScroll[6]).css('top',
			parseInt($(gui.libraryScroll[6]).css('top')) *
				parseInt($(gui.libraryScroll[2]).css('top')) *
					((($(gui.libraryScroll[0]).height()) * 92.1) / 100)
						/ $(gui.libraryScroll[1]).height()
							/ parseInt($(gui.libraryScroll[2]).css('top')));
		
		$(gui.TOSScroll[6]).css('top',
			parseInt($(gui.TOSScroll[6]).css('top')) *
				parseInt($(gui.TOSScroll[2]).css('top')) *
					((($(gui.TOSScroll[0]).height()) * 92.1) / 100)
						/ $(gui.TOSScroll[1]).height()
							/ parseInt($(gui.TOSScroll[2]).css('top')));
		
		$(gui.PPScroll[6]).css('top',
			parseInt($(gui.PPScroll[6]).css('top')) *
				parseInt($(gui.PPScroll[2]).css('top')) *
					((($(gui.PPScroll[0]).height()) * 92.1) / 100)
						/ $(gui.PPScroll[1]).height()
							/ parseInt($(gui.PPScroll[2]).css('top')));
		
		// Recalculate and relocate thumb according to changes in the
		// scroll container's height. Thumb's new location = its current
		// location * new container's height (which will be the same as the
		// new track's height will assign next) / the current track's height
		// (which will be the old one after we change it next). Thumb's new
		// location must be calculated before changing track's height to
		// be able to get the originall proportion between thumb's height
		// and track's height.
		$(gui.newsScroll[2]).css('top',
			parseInt($(gui.newsScroll[2]).css('top')) *
				((($(gui.newsScroll[0]).height()) * 92.1) / 100)
					/ $(gui.newsScroll[1]).height());
		
		$(gui.faqScroll[2]).css('top',
			parseInt($(gui.faqScroll[2]).css('top')) *
				((($(gui.faqScroll[0]).height()) * 92.1) / 100)
					/ $(gui.faqScroll[1]).height());
		
		$(gui.libraryScroll[2]).css('top',
			parseInt($(gui.libraryScroll[2]).css('top')) *
				((($(gui.libraryScroll[0]).height()) * 100) / 100)
					/ $(gui.libraryScroll[1]).height());
		
		$(gui.TOSScroll[2]).css('top',
			parseInt($(gui.TOSScroll[2]).css('top')) *
				((($(gui.TOSScroll[0]).height()) * 100) / 100)
					/ $(gui.TOSScroll[1]).height());
		
		$(gui.PPScroll[2]).css('top',
			parseInt($(gui.PPScroll[2]).css('top')) *
				((($(gui.PPScroll[0]).height()) * 100) / 100)
					/ $(gui.PPScroll[1]).height());
		
		// Resize scrolls track's height. We use the same formula as
		// in the "gui.scroll" function. Basically, the thumb is 92.1%
		// smaller in height than the scroll container.
		$(gui.newsScroll[1]).height((($(gui.newsScroll[0]).height())
			* 92.1) / 100);
		
		$(gui.faqScroll[1]).height((($(gui.faqScroll[0]).height())
			* 92.1) / 100);
		
		$(gui.libraryScroll[1]).height((($(gui.libraryScroll[0]).height())
			* 100) / 100);
		
		$(gui.TOSScroll[1]).height((($(gui.TOSScroll[0]).height())
			* 100) / 100);
		
		$(gui.PPScroll[1]).height((($(gui.PPScroll[0]).height())
			* 100) / 100);
		
		// Resize scrolls thumb's height. We use the same formula as
		// in the "gui.scroll" function. This way we keep height the
		// proportion between thumb and track. New thumb's height =
		// (track's height / content's height) * track's height.
		$(gui.newsScroll[2]).height($(gui.newsScroll[1]).height() /
			$(gui.newsScroll[7]).height() * $(gui.newsScroll[1]).height());
		
		$(gui.faqScroll[2]).height($(gui.faqScroll[1]).height() /
			$(gui.faqScroll[7]).height() * $(gui.faqScroll[1]).height());
		
		$(gui.libraryScroll[2]).height($(gui.libraryScroll[1]).height() /
			$(gui.libraryScroll[7]).height() *
				$(gui.libraryScroll[1]).height());
		
		$(gui.TOSScroll[2]).height($(gui.TOSScroll[1]).height() /
			$(gui.TOSScroll[7]).height() *
				$(gui.TOSScroll[1]).height());
		
		$(gui.PPScroll[2]).height($(gui.PPScroll[1]).height() /
			$(gui.PPScroll[7]).height() *
				$(gui.PPScroll[1]).height());
		
		// Resize the area in between the top and bottom of the thumb (the
		// thumb is actually a div with three images inside). Thumb center's
		// height = thumb's height - thumb top's height - thumb bottom's
		// height.
		$(gui.newsScroll[4]).height($(gui.newsScroll[2]).height() -
			$(gui.newsScroll[3]).height() - $(gui.newsScroll[5]).height());
		
		$(gui.faqScroll[4]).height($(gui.faqScroll[2]).height() -
			$(gui.faqScroll[3]).height() - $(gui.faqScroll[5]).height());
		
		$(gui.libraryScroll[4]).height($(gui.libraryScroll[2]).height() -
			$(gui.libraryScroll[3]).height() -
				$(gui.libraryScroll[5]).height());
		
		$(gui.TOSScroll[4]).height($(gui.TOSScroll[2]).height() -
			$(gui.TOSScroll[3]).height() - $(gui.TOSScroll[5]).height());
		
		$(gui.PPScroll[4]).height($(gui.PPScroll[2]).height() -
			$(gui.PPScroll[3]).height() - $(gui.PPScroll[5]).height());
		
		// Center "Faq" board only when coming from "Download" prompt. The
		// only thing to distinguish this from when the main menu's "FAQ"
		// button is clicked is the "Back to Download" button, so we use its
		// position to know when to center the board. 
		if ($('.backToDownloadButton').offset().top > 0) {
			$(gui.board[3]).css('left', (gui.sWidth -
				$(gui.board[3]).width()) / 2);
		}
		
		// Keep the canvas container's size the same unless the
		// browser's height gets longer than the screen height or its width
		// get longer than the screen's. If the height gets heigher, keep the
		// width with the same proportion as it was originally. If the width is
		// the one who gets longer, then keep the height with the same
		// proportion as before.
		if (window.innerHeight < gui.sHeight && window.innerWidth <
			gui.sWidth) {
			$('body>div>canvas').height(gui.sHeight);
			$('body>div>canvas').width(gui.sWidth);
		} else if (window.innerHeight > gui.sHeight) {
			$('body>div>canvas').height(window.innerHeight);
			$('body>div>canvas').width(gui.sWidth * window.innerHeight /
				gui.sHeight);	
		} else if (window.innerWidth > gui.sWidth) {
			$('body>div>canvas').height(gui.sHeight * window.innerWidth /
				gui.sWidth);
			$('body>div>canvas').width(window.innerWidth);
		}
		
		// Check the browser window's dimensions again to see if scrollbars are
		// needed.
		toggleBrowserScrollbarsOnOff();
	}
	
	var resizeLayout = function () {
		gui.deviceSHeight = screen.height;
		gui.deviceSWidth = screen.width;
		
		// Set values for these variables which will be used for comparing with
		// the current ones on browser window resize. Here will give them their
		// first value and then these wil also be updated after being used for
		// comparing.
		gui.sHeight = screen.height;
		gui.sWidth = screen.width;
		gui.lastInHeight = window.innerHeight;
		gui.lastInWidth = window.innerWidth;
		gui.lastOutHeight = window.outerHeight;
		gui.lastOutWidth = window.outerWidth;
		gui.lastDPR = window.devicePixelRatio;
		
		// Set this functionality to take place on windows resize.
		window.addEventListener('resize', onWindowResize, false);
	};
	
	resizeLayout();
};

var animateBackground = function () {
	// Measure the time it takes to go from one frame to the next.
	var delta = trid.clock.getDelta() * 3; // Make animations update faster by
										   // multiplying it by something.
	
	// Draw both the background and viewer canvases.
	requestAnimationFrame(animateBackground);
	
	// Render the backgroundScene.
	trid.backgroundRenderer.render(trid.backgroundScene, trid.backgroundCamera);
	
	// Update animations except characters'.
	for (var i = 0; i < 37; i++) {
		if (trid.animations[i] !== null) {
			for (var k = 0; k < trid.animations[i].length; k++) {
				trid.animations[i][k].update(delta);
			}
		}
	}
	
	// Update character animations as long as character board is not visible.
	// The idea is to optimize by avoiding running needles animations that won't
	// be seen after all.
	if (gui.isBoardVisible[2] == false) {
		for (var i = 37; i < 45; i++) {
			if (trid.animations[i] !== null) {
				for (var k = 0; k < trid.animations[i].length; k++) {
					trid.animations[i][k].update(delta);
				}
			}
		}
	}
	
	// ANIMATE MODELS UVS: river, foam, drops, smokeLeft, smokeRight,
	// TATLogoBannerLeft, TATLogoBannerRight, TATLogoTopHighlight and
	// TATLogoBottomHighlight.
	trid.animateUvs(2, 2, 8, trid.clock.getElapsedTime(), 0.2, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(3, 2, 8, trid.clock.getElapsedTime(), 0.2, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(4, 2, 8, trid.clock.getElapsedTime(), 0.1, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(5, 2, 8, trid.clock.getElapsedTime(), 0.2, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(6, 2, 8, trid.clock.getElapsedTime(), 0.2, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(26, 9, 2, trid.clock.getElapsedTime(), 0.1, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(27, 9, 2, trid.clock.getElapsedTime(), 0.1, 0,
		trid.clock.getElapsedTime() * 2);
	trid.animateUvs(29, 2, 4, trid.clock.getElapsedTime(), 0.075, 6, 6.6);
	trid.animateUvs(30, 4, 8, trid.clock.getElapsedTime(), 0.075, 3.5, 5.9);
	
	// Update uvs.
	for (var i = 0; i < 46; i++) {
		trid.models[i].geometry.uvsNeedUpdate = true;
	}
	
	// Animate models.
	trid.emptyHelpers[0].lookAt(trid.backgroundCamera.position);
	trid.emptyHelpers[1].lookAt(trid.backgroundCamera.position);
	trid.models[7].rotateY(Math.PI * (-0.05) * delta);
	trid.models[9].rotateZ(Math.PI * (-0.05) * delta);
	
	// Fade in title.
	if (trid.titleFadeInTimer.running === true) {
		for (var i = 25; i <= 30; i++) {
			trid.models[i].material.opacity += 0.05 + delta;
		}
		if (trid.models[25].material.opacity >= 1) {
			for (var i = 25; i <= 30; i++) {
				trid.models[i].material.opacity = 1;
				trid.titleFadeInTimer.stop();
			}
		}
	}
	
	// Animate backgroundCamera panning it downwards after title has been shown.
	if (trid.cameraTimer.running === true) {
		// Use a sinusoidal graph to properly animate backgroundCamera and
		// target. Movement starts from the crest of the curve and ends at the
		// valley. The domain of this function should be the time the
		// backgroundCamera and target take to go from their tridal position to
		// their final destination. The amplitude is the distance they travel
		// vertically. The general function is f(t)= a.sin(b(t + k)) + h,
		// where "a" is the amplitude (a = (ti - tf) / 2), b=2PI/T (T being the
		// domain), "k" is the horizontal displacement and "h" is the vertical
		// displacement (h = a + tf).
		trid.backgroundCamera.position.y = (29.63 * Math.sin(0.523333 *
			(trid.cameraTimer.getElapsedTime() + Math.PI)) + 41.37);
		trid.backgroundControls.target.y = trid.backgroundCamera.position.y;
		
		// Set locator vector's position to be the same as the backgroundCamera
		// so as to know its position. When the locator vector reaches the final
		// destination, stop the animation.
		trid.locators[5].setFromMatrixPosition(
			trid.backgroundCamera.matrixWorld
		);
		
		// Stop the function once the camera has reached the valley (that is,
		// the end position). In order to do this, find whether the graph goes
		// up or down for each frame by subtracting the next y value minus the
		// current one. In other words, f(t + 1) - f(t). Stop the animation once
		// the result is greater or equal to 0. We will take arbitrarily as the
		// next point the current time plus 1 millisecond.
		if (((29.63 * Math.sin(0.523333 *
			(trid.cameraTimer.getElapsedTime() + 0.001 + Math.PI)) + 41.37) -
				(29.63 * Math.sin(0.523333 * (trid.cameraTimer.getElapsedTime()
					+ Math.PI)) + 41.37)) >= 0) {
			trid.cameraTimer.stop();
			
			// Run GUI and character viewer.
			setGUI();
			setViewer();
		}
	};
	
	// Animate the viewer once it's flagged as "ready".
	if (trid.isViewerReady === true) {
		animateViewer();
	}
	
	// Fade out title.
	if (trid.titleFadeOutTimer.running === true) {
		for (var i = 25; i <= 30; i++) {
			trid.models[i].material.opacity -= 0.03 + delta;
		}
		if (trid.models[25].material.opacity <= 0) {
			trid.titleFadeOutTimer.stop();
		}
	}
	
	// Move bottom left cloud to the right.
	trid.models[12].position.x += 0.3 * delta;
	trid.locators[0].setFromMatrixPosition(trid.models[12].matrixWorld);
	
	if (trid.locators[0].x > 90) {
		trid.models[12].position.set(-90,
			36.72701 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move bottom right cloud to the right.
	trid.models[13].position.x += 0.3 * delta;
	trid.locators[1].setFromMatrixPosition(trid.models[13].matrixWorld);
	
	if (trid.locators[1].x > 90) {
		trid.models[13].position.set(-90,
		33.27302 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move center right cloud to the right.
	trid.models[14].position.x += 0.1 * delta;
	trid.locators[2].setFromMatrixPosition(trid.models[14].matrixWorld);
	
	// Reset clouds positions by placing them on the left. Add a little bit of
	// randomness to their locations on Y-axis.
	if (trid.locators[2].x > 90) {
		trid.models[14].position.set(-90,
			56.78553 + Math.round(Math.random() * 2 - 1) * 3, -87.6);
	}trid.backgroundCamera.position.y
	
	// Move top right cloud to the right.
	trid.models[15].position.x += 0.3 * delta;
	trid.locators[3].setFromMatrixPosition(trid.models[15].matrixWorld);
	
	if (trid.locators[3].x > 90) {
		trid.models[15].position.set(-90,
			78.93318 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
		
	// Move top left cloud to the right.
	trid.models[16].position.x += 0.6 * delta;
	trid.locators[4].setFromMatrixPosition(trid.models[16].matrixWorld);
	
	if (trid.locators[4].x > 90) {
		trid.models[16].position.set(-90,
			70.31664 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move second bottom left cloud to the right.
	trid.models[17].position.x += 0.3 * delta;
	trid.locators[5].setFromMatrixPosition(trid.models[17].matrixWorld);
	
	if (trid.locators[5].x > 90) {
		trid.models[17].position.set(-90,
			35 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move third bottom left cloud to the right.
	trid.models[18].position.x += 0.26 * delta;
	trid.locators[6].setFromMatrixPosition(trid.models[18].matrixWorld);
	
	if (trid.locators[6].x > 90) {
		trid.models[18].position.set(-90,
			30 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move fourth bottom left cloud to the right.
	trid.models[19].position.x += 0.25 * delta;
	trid.locators[7].setFromMatrixPosition(trid.models[19].matrixWorld);
	
	if (trid.locators[7].x > 90) {
		trid.models[19].position.set(-90,
			24 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move fifth bottom left cloud to the right.
	trid.models[20].position.x += 0.2 * delta;
	trid.locators[8].setFromMatrixPosition(trid.models[20].matrixWorld);
	
	if (trid.locators[8].x > 90) {
		trid.models[20].position.set(-90,
			23 + Math.round(Math.random() * 2 - 1) * 2, -87.6);
	}
	
	// Move sixth bottom left cloud to the right.
	trid.models[21].position.x += 0.2 * delta;
	trid.locators[9].setFromMatrixPosition(trid.models[21].matrixWorld);
	
	if (trid.locators[9].x > 90) {
		trid.models[21].position.set(-90,
			22 + Math.round(Math.random() * 2 - 1), -87.6);
	}
	
	// Move seventh bottom left cloud to the right.
	trid.models[22].position.x += 0.1 * delta;
	trid.locators[10].setFromMatrixPosition(trid.models[22].matrixWorld);
	
	if (trid.locators[10].x > 90) {
		trid.models[22].position.set(-90,
			18 + Math.round(Math.random() * 2 - 1), -87.6);
	}
	
	// Move eighth bottom left cloud to the right.
	trid.models[23].position.x += 0.1 * delta;
	trid.locators[11].setFromMatrixPosition(trid.models[23].matrixWorld);
	
	if (trid.locators[11].x > 90) {
		trid.models[23].position.set(-90,
			18 + Math.round(Math.random() * 2 - 1), -87.6);
	}
	
	// Move ninth bottom left cloud to the right.
	trid.models[24].position.x += 0.1 * delta;
	trid.locators[12].setFromMatrixPosition(trid.models[24].matrixWorld);
	
	if (trid.locators[12].x > 90) {
		trid.models[24].position.set(-90,
			18 + Math.round(Math.random() * 2 - 1), -87.6);
	}
	
	trid.backgroundControls.update();
	
};

var setGUI = function () {
	// Display GUI.
	$('.floatContainer').css('visibility', 'visible');
	// Display main menu.
	$(gui.mainMenu).animate({top: 0}, 750, 'swing'); // Move main menu down.
	// Set "Download" prompt ready.
	gui.choosePrompt(11, 'download');
	gui.choosePrompt(16, 'download');
	// Set "Log in" prompt ready.
	gui.choosePrompt(5, 'login');
	$(gui.button[5]).click(function () {
		gui.showPrompt('open');
	});
	gui.goToFAQ(gui.FAQLink);
	// Turn off all buttons effects by default.
	gui.turnAllElementsOff();
	// Enable hiding boards when "Home" button is clicked.
	gui.hideBoard(6, 0, 1, 2, 3, 4, 5, 21, 30, 31);
	// Enable hiding boards when logging in.
	gui.hideBoard(5, 0, 1, 2, 3, 4, 30, 31);
	// Enable hiding boards when logging out.
	gui.hideBoard(20, 0, 1, 2, 3, 4, 5, 21, 30, 31);
	// Validate forms.
	gui.validate(17, gui.contactForm, gui.contactKeys, gui.contactErrors, 0);
	gui.validate(18, gui.loginForm, gui.loginKeys, gui.loginErrors, 1);
	gui.validate(19, gui.registrationForm, gui.registrationKeys,
		gui.registrationErrors, 2);
	gui.validate(22, gui.recoveryForm, gui.recoveryKeys, gui.recoveryErrors, 3);
	gui.validate(24, gui.confirmationForm, gui. confirmationKeys,
		gui.confirmationErrors, 4);
	gui.validate(26, gui.resetForm, gui.resetKeys, gui.resetErrors, 5);
	gui.validate(27, gui.updateEmailForm, gui.updateEmailKeys,
		gui.updateEmailErrors, 6);
	gui.validate(28, gui.accountPasswordForm, gui.resetKeys, gui.resetErrors,
		7);
	gui.changePassword(26, gui.resetForm, 5);
	// Set "Contact" board ready to be hidden.
	gui.hideContactBoard(17, 4, 0);
	// Set buttons ready to be disabled.
	gui.disableButtons(17, 0, 0, 1, 2, 3, 4, 5, 6, 21, 0);
	// Set buttons ready to be enabled.
	gui.enableButtons(17, 0, 1, 2, 3, 4, 5, 6, 21, 3750);
	// Set e-mail confirmation ready.
	gui.showConfirmationMessage(17, 0);
	// Set which buttons can be toggled.
	gui.toggleButtonOnOff(0, 1, 3, 4, 5, 21);
	// Enable reseting forms' inputs.
	gui.resetFormOnSend(17, gui.contactForm, 0, 3750);
	gui.resetFormOnSend(18, gui.loginForm, 1, 3750);
	gui.resetFormOnSend(18, gui.registrationForm, 1, 3750);
	gui.resetFormOnSend(22, gui.recoveryForm, 3, 3750);
	gui.resetFormOnCancel(4, gui.contactForm, 750);
	gui.resetFormOnCancel(6, gui.contactForm, 750);
	gui.resetFormOnCancel(15, gui.loginForm, 750);
	gui.resetFormOnCancel(15, gui.registrationForm, 750);
	gui.resetFormOnCancel(23, gui.recoveryForm, 0);
	gui.resetFormOnCancel(15, gui.confirmationForm, 0);
	gui.resetFormOnCancel(15, gui.resetForm, 0);
	gui.resetFormOnCancel(6, gui.updateEmailForm, 750);
	gui.resetFormOnCancel(6, gui.accountPasswordForm, 750);
	// Show the user's e-mail in the "Account" board "Login info" input field.  
	$(gui.button[21]).click(function () {
		// Only when "Account" board isn't visible;
		if (gui.isBoardVisible[21] === false) {
			$('#accountFormEmail').val(gui.userEmail);
		}
	});
	// Enable reseting scroll when specific buttons are clicked.
	gui.resetScroll(gui.newsScroll, 0, 1, 2, 3, 4, 5, 6);
	gui.resetScroll(gui.faqScroll, 0, 1, 2, 3, 4, 5, 6, 16);
	gui.resetScroll(gui.TOSScroll, 0, 1, 2, 3, 4, 5, 6, 30, 31);
	gui.resetScroll(gui.PPScroll, 0, 1, 2, 3, 4, 5, 6, 30, 31);
	// Set message ready to be saved.
	gui.saveMessage(17, gui.contactForm, 0);
	// Set account recovery request ready.
	gui.sendAccountRecoveryRequest(22, gui.recoveryForm, 3);
	// Set account recovery confirmation ready.
	gui.confirmOwnership(24, gui.confirmationForm, 4);
	// Set which buttons whill be active by default.
	gui.setDefaultActiveButton(2, 7);
	gui.setDefaultActiveProfile(2, 7);
	// Set "Back to download" button ready.
	gui.returnToDownload(16);
	// Enable turning a specific button on while hiding the rest.
	gui.showButtonAsActiveAndTurnOff(2, 0, 1, 3, 4, 5, 21);
	gui.showButtonAsActiveAndTurnOff(6, 0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 21);
	// Enable hover effect on buttons.
	gui.showButtonAsHovered(0);
	gui.showButtonAsHovered(1);
	gui.showButtonAsHovered(2);
	gui.showButtonAsHovered(3);
	gui.showButtonAsHovered(4);
	gui.showButtonAsHovered(5);
	gui.showButtonAsHovered(6);
	gui.showButtonAsHovered(7);
	gui.showButtonAsHovered(8);
	gui.showButtonAsHovered(9);
	gui.showButtonAsHovered(10);
	gui.showButtonAsHovered(11);
	gui.showButtonAsHovered(12);
	gui.showButtonAsHovered(13);
	gui.showButtonAsHovered(14);
	gui.showButtonAsHovered(15);
	gui.showButtonAsHovered(16);
	gui.showButtonAsHovered(17);
	gui.showButtonAsHovered(18);
	gui.showButtonAsHovered(19);
	gui.showButtonAsHovered(20);
	gui.showButtonAsHovered(21);
	gui.showButtonAsHovered(22);
	gui.showButtonAsHovered(23);
	gui.showButtonAsHovered(24);
	gui.showButtonAsHovered(25);
	gui.showButtonAsHovered(26);
	// Set active button effect ready.
	gui.showButtonAsActive(11);
	gui.showButtonAsActive(12);
	gui.showButtonAsActive(13);
	gui.showButtonAsActive(14);
	gui.showButtonAsActive(15);
	gui.showButtonAsActive(16);
	gui.showButtonAsActive(17);
	gui.showButtonAsActive(18);
	gui.showButtonAsActive(19);
	gui.showButtonAsActive(20);
	gui.showButtonAsActive(22);
	gui.showButtonAsActive(23);
	gui.showButtonAsActive(24);
	gui.showButtonAsActive(25);
	gui.showButtonAsActive(26);
	// Set boards ready.
	gui.showOrHide(0, 1, 2, 3, 4, 21, 30, 31);
	// Set character menu's buttons ready.
	gui.showOneButtonAsActiveAtATime(7, 8, 9, 10);
	// Set scrolls ready.
	gui.scroll(gui.newsScroll, 0.921);
	gui.scroll(gui.faqScroll, 0.921);
	gui.scroll(gui.TOSScroll, 0.921);
	gui.scroll(gui.PPScroll, 0.921);
	// Set the action to start with.
	gui.actionIndex = 6;
	gui.switchAction(gui.actionName, 12, 13, 0, 9, 6, 6);
	// Set menus ready.
	gui.switchMenus(2, gui.mainMenu, gui.characterMenu);
	gui.switchMenus(6, gui.characterMenu, gui.mainMenu);
	gui.switchMenus(30, gui.characterMenu, gui.mainMenu);
	gui.switchMenus(31, gui.characterMenu, gui.mainMenu);
	// Set profiles ready.
	gui.switchProfiles(7, 8, 9, 10);
	// Enable prompt hiding.
	gui.hidePrompt(15);
	// Scroll down to locator automatically when clicking in a News and FAQ
	// headings.
	gui.scrollToLocator(gui.faqScroll);
	gui.scrollToLocator(gui.newsScroll);
	// Turn "Log In" button off on cancel.
	$('.promptContainer>div>div>div:last-child>div').click(function() {
		$(gui.activeButton[5]).css('visibility', 'hidden');
		gui.isButtonActive[5] = false;
	});
	// Send to Account Recovery when clicking on "Can't log in?".
	$('#resetLink').click(function() {
		$('#login').hide();
		gui.prompt = '#accountRecoveryRequest';
		gui.showPrompt('blink');
	});
	// Send back to log in when clicking on "Back to login" button
	$(gui.button[23]).click(function () {
		$(gui.prompt).hide();
		gui.prompt = '#login';
		gui.showPrompt('blink');
	});
	// Send back to "Account Recovery: Step One" when clicking on "Back to step
	// one" button.
	$(gui.button[25]).click(function () {
		$(gui.confirmationForm[0]).val('');
		$(gui.prompt).hide();
		gui.prompt = '#accountRecoveryRequest';
		gui.showPrompt('blink');
	});
	
	// Register user.
	$(gui.button[19]).click(function() {
		var activeItem;
		var email;
		var password;
		var passwordConfirmation;
		var username;
		var form = gui.registrationForm;
		
		if (gui.isFormValid[2] === true) {
			username = $(form[0]).val();
			email = $(form[1]).val();
			password = $(form[2]).val();
			passwordConfirmation = $(form[3]).val();
			
			$.ajax({
				type:'POST',
				url:'/',
				data: {'id': 'registration',
					'username': username,
					'email': email,
					'password': password,
					'rePassword': passwordConfirmation},
				dataType: 'json',
				success: function (json) {
					if (json.registrationStatus === 'valid') {
						console.log('User registered successfully!');
						for (var i = 0; i < form.length; i++) {
							$(form[i]).val('');
						}
						$(gui.prompt).hide();
						gui.prompt = '#registrationCompleted';
						gui.showPrompt('blink');
						// Close this message after a brief time and replace
						// the "Log in" button with the "Account" one, then
						// make it flash four times to let the user know
						// about the change.
						setTimeout(function () {
							gui.showPrompt('close');
							$(gui.activeButton[5]).css('visibility',
								'hidden');
							gui.isButtonActive[5] = false;
							$(gui.button[5]).hide();
							$('#loginButtonImage').hide();
							$('#accountButtonImage').show();
							$(gui.button[21]).show();
							gui.flashButton(21, 1000, 5);
						}, 3750);
						// Show the username and e-mail on the "Account"
						// board.
						$('#accountUsername').val('');
						$('#accountUsername').val(username);
						$('#accountFormEmail').val(json.email);
						gui.userEmail = json.email;
						// Display user's library items in the "Account"
						// board.
						$.each(json.userLibrary, function(key, value) {
							$('#libraryContent').append(
								'<div id=' + value.name.toLowerCase() +
								' class="item"><table><tbody><tr>' +
								'<td>' +
								value.name +
								'</td><td>' +
								value.type +
								'</td><td>' +
								value.license +
								'</td><td>' +
								value.price +
								'</td>' +
								'</tr></tbody></table></div>'
							);
						});
						// Highlight items when hovered.
						$('.item').mouseover('click', function (e) {
							$(e.target).css('background-color', '#f0ca64');
							$(activeItem).css('background-color',
								'#ffc308');
							$(e.target).mouseleave(function () {
								$(this).css('background-color',
								'transparent');
								$(activeItem).css('background-color',
									'#ffc308')
							});
						});
						// Display selected item as active on click.
						$('.item').on('click', function (e) {
							$('.item').css('background-color',
								'transparent');
							$(e.target).css('background-color', '#ffc308');
							activeItem = e.target;
						});
						// Deselect choosen option from "Library" when
						// clicking "Home" and "Account" buttons.
						$(gui.button[6]).click(function () {
							setTimeout(function () {
								$('.item').css('background-color',
								'transparent');
							}, 750);
							activeItem = undefined;
							$(downloadFromLibraryForm).attr('action', '');
						});
						$(gui.button[21]).click(function () {
							if (gui.isBoardVisible[21] === false) {
								setTimeout(function () {
									$('.item').css('background-color',
									'transparent');
								}, 750);
								activeItem = undefined;
								$(downloadFromLibraryForm).attr('action', '');
							}
						});
						// Make sure to reset the url the download form points
						// to.
						$(downloadFromLibraryForm).attr('action', '');
						// Set the url the download form points to.
						$(downloadFromLibraryForm).click(function () {
							if (activeItem !== undefined) {
								var item = json.userLibrary[activeItem.id];
								var fileUrl = item['fileUrl'];
								$(downloadFromLibraryForm).attr('action',
									fileUrl);
							} else {
								return false;
							}
						});
						/* Enable "Library" scroll functionality. */
						gui.scroll(gui.libraryScroll, 1);
					} else if (json.registrationStatus === 'invalid') {
						console.log('Invalid input: try again!');
						for (var i = 0; i < form.length; i++) {
							gui.warnInInput(form[i],
								gui.registrationErrors[i])
						}
					} else if (json.registrationStatus === 'taken') {
						console.log('The username is already taken');
						gui.warnInInput(form[0],
							gui.registrationErrors[0]);
					} else if (json.registrationStatus ===
						'passwordsDoNotMatch') {
							console.log('Passwords do not match!');
							gui.warnInInput(form[2],
								gui.registrationErrors[3]);
							gui.warnInInput(form[3],
								gui.registrationErrors[3]);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	// Log in user.
	$(gui.button[18]).click(function() {
		var activeItem;
		var password;
		var username;
		var form = gui.loginForm;
		
		if (gui.isFormValid[1] === true) {
			username = $(form[0]).val();
			password = $(form[1]).val();
			
			$.ajax({
				type:'POST',
				url:'/',
				data: {'id': 'login', 'username': username,
					'password': password},
				dataType: 'json',
				success: function (json) {
					// Check if the user is authenticated by the server.
					if (json.userStatus === 'active') {
						console.log('User has logged in successfully.');
						// Replace the log in form with a welcoming message.
						$(gui.prompt).hide();
						gui.prompt = '#welcome';
						gui.showPrompt('blink');
						// Close this message after a brief time and replace
						// the "Log in" button with the "Account" one, then
						// make it flash four times to let the user know
						// about the change.
						setTimeout(function () {
							gui.showPrompt('close');
							$(gui.activeButton[5]).css('visibility',
								'hidden');
							gui.isButtonActive[5] = false;
							$(gui.button[5]).hide();
							$('#loginButtonImage').hide();
							$('#accountButtonImage').show();
							$(gui.button[21]).show();
							gui.flashButton(21, 1000, 5);
						}, 3750);
						// Show the username and e-mail on the "Account"
						// board.
						$('#accountUsername').val('');
						$('#accountUsername').val(username);
						$('#accountFormEmail').val(json.email);
						gui.userEmail = json.email;
						// Display user's library items in the "Account"
						// board.
						$.each(json.userLibrary, function(key, value) {
							$('#libraryContent').append(
								'<div id=' + value.name.toLowerCase() +
								' class="item"><table><tbody><tr>' +
								'<td>' +
								value.name +
								'</td><td>' +
								value.type +
								'</td><td>' +
								value.license +
								'</td><td>' +
								value.price +
								'</td>' +
								'</tr></tbody></table></div>'
							);
						});
						// Highlight items when hovered.
						$('.item').mouseover('click', function (e) {
							$(e.target).css('background-color', '#f0ca64');
							$(activeItem).css('background-color',
								'#ffc308');
							$(e.target).mouseleave(function () {
								$(this).css('background-color',
								'transparent');
								$(activeItem).css('background-color',
									'#ffc308')
							});
						});
						// Display selected item as active on click.
						$('.item').on('click', function (e) {
							$('.item').css('background-color',
								'transparent');
							$(e.target).css('background-color', '#ffc308');
							activeItem = e.target;
						});
						// Deselect choosen option from "Library" when
						// clicking "Home" and "Account" buttons.
						$(gui.button[6]).click(function () {
							setTimeout(function () {
								$('.item').css('background-color',
								'transparent');
							}, 750);
							activeItem = undefined;
							$(downloadFromLibraryForm).attr('action', '');
						});
						$(gui.button[21]).click(function () {
							if (gui.isBoardVisible[21] === false) {
								setTimeout(function () {
									$('.item').css('background-color',
									'transparent');
								}, 750);
								activeItem = undefined;
								$(downloadFromLibraryForm).attr('action', '');
							}
						});
						// Make sure to reset the url the download form points
						// to.
						$(downloadFromLibraryForm).attr('action', '');
						// Set the url the download form points to.
						$(downloadFromLibraryForm).click(function () {
							if (activeItem !== undefined) {
								var item = json.userLibrary[activeItem.id];
								var fileUrl = item['fileUrl'];
								$(downloadFromLibraryForm).attr('action',
									fileUrl);
							} else {
								return false;
							}
						});
						// Enable "Library" scroll functionality. //
						gui.scroll(gui.libraryScroll, 1);
					} else if (json.userStatus === 'disabled') {
						console.log('The password is valid, but the' +
							' account has been disabled!');
					} else if (json.userStatus === 'invalid') {
						console.log('The username and password were' +
							' incorrect.');
						gui.warnInInput(form[0], gui.loginErrors[0]);
						gui.warnInInput(form[1], gui.loginErrors[1]);
					} else if (json.userStatus === 'error') {
						console.log('ERROR: unable to log in!');
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	// Change password from the "Account" board.
	$(gui.button[28]).click(function() {
		var password;
		var rePassword;
		var form = gui.accountPasswordForm; // Just a shortcut.
		
		if (gui.isFormValid[7] === true) {
			password = $(form[0]).val();
			rePassword = $(form[1]).val();
			
			$.ajax({
				type:'POST',
				url:'/',
				data: {'id': 'changePasswordFromAccountBoard',
					'password': password,
					'rePassword': rePassword},
				dataType: 'json',
				success: function (json) {
					if (json.newPasswordStatus === 'valid') {
						console.log('Password changed successfully!');
						for (var i = 0; i < form.length; i++) {
							$(form[i]).val('');
						}
						$(gui.prompt).hide();
							gui.prompt = '#passwordChanged';
							gui.showPrompt('open');
							// Close this message after a brief time.
							setTimeout(function () {
								gui.showPrompt('close');
							}, 3750);
					} else if (json.newPasswordStatus === 'invalid') {
						console.log('New password is invalid: try again!');
						gui.warnInInput(gui.resetForm[4],
							gui.resetErrors[0]);
					} else if (json.newPasswordStatus ===
						'passwordsDoNotMatch') {
							console.log('Passwords do not match!');
							for (var i = 0; i < form.length; i++) {
								gui.warnInInput(form[i],
									gui.resetErrors[1]);
							}
					} else if (json.newPasswordStatus === 'userIsNotLoggedIn') {
						console.log('User is not logged in! Redirecting to ' +
							'login prompt...');
						// Clean input fields.
						for (var i = 0; i < form.length; i++) {
							$(form[i]).val('');
						}
						// Hide "Account" board.
						if (gui.isBoardVisible[21] === true) {
							$(gui.board[21]).animate({top: -
								$(gui.board[21]).height()}, 750);
							gui.isBoardVisible[21] = false;
						}
						// Replace the "Account" button with the "Log out" one.
						$(gui.activeButton[21]).css('visibility', 'hidden');
						gui.isButtonActive[21] = false;
						$(gui.button[21]).hide();
						$('#accountButtonImage').hide();
						$('#loginButtonImage').show();
						$(gui.button[5]).show();
						gui.flashButton(5, 1000, 5);
						// Erase the username on the "Account" board.
						$('#accountUsername').val('');
						// Display "Login" prompt.
						gui.prompt = '#login';
						gui.showPrompt('open');
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	// Change e-mail.
	$(gui.button[27]).click(function() {
		var email;
		var form = gui.updateEmailForm; // Just a shorcut.
		
		if (gui.isFormValid[6] === true) {
			email = $(form[0]).val();
			
			$.ajax({
				type:'POST',
				url:'/',
				data: {'id': 'changeEmail', 'email': email},
				dataType: 'json',
				success: function (json) {
					if (json.newEmailStatus === 'valid') {
						console.log('E-mail address changed successfully!');
						for (var i = 0; i < form.length; i++) {
							$(form[i]).val('');
						}
						$(gui.prompt).hide();
							gui.prompt = '#emailChanged';
							gui.showPrompt('open');
							// Close this message after a brief time.
							setTimeout(function () {
								gui.showPrompt('close');
							}, 3750);
					} else if (json.newEmailStatus === 'invalid') {
						console.log('New email is invalid: try again!');
						gui.warnInInput(gui.updateEmailForm[0],
							gui.updateEmailErrors[0]);
					} else if (json.newEmailStatus === 'userIsNotLoggedIn') {
						console.log('User is not logged in! Redirecting to ' +
							'login prompt...');
						// Clean input fields.
						for (var i = 0; i < form.length; i++) {
							$(form[i]).val('');
						}
						// Hide "Account" board.
						if (gui.isBoardVisible[21] === true) {
							$(gui.board[21]).animate({top: -
								$(gui.board[21]).height()}, 750);
							gui.isBoardVisible[21] = false;
						}
						// Replace the "Account" button with the "Log out" one.
						$(gui.activeButton[21]).css('visibility', 'hidden');
						gui.isButtonActive[21] = false;
						$(gui.button[21]).hide();
						$('#accountButtonImage').hide();
						$('#loginButtonImage').show();
						$(gui.button[5]).show();
						gui.flashButton(5, 1000, 5);
						// Erase the username on the "Account" board.
						$('#accountUsername').val('');
						// Display "Login" prompt.
						gui.prompt = '#login';
						gui.showPrompt('open');
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	});
	// Make the "Log out" button slide behind the "Account" button on hover and
	// hide when mouse leaves both of their areas.
	var slideButton = function () {
		// Show button when hovering over "Account" button.
		$(gui.button[21]).mouseenter(function() {
			$('.rightContainer>div:nth-child(1)>div:nth-child(1)>' +
				'div:nth-child(12)').animate({'width': '150%'}, 300);
			$('.rightContainer>div:nth-child(1)>div:nth-child(1)>' +
				'div:nth-child(12)').css('right', '0');
			$(gui.button[21]).width('44%');
			$(gui.button[21]).css('margin-left', '-6%');
			$(gui.button[20]).width('44%');
			$(gui.button[20]).css('margin-left', '-50%');
			$('#logoutButtonImage').animate({'left': '-66%'}, 300);
			$(gui.hoveredButton[20]).animate({'left': '-66%'}, 300);
			$(gui.activeButton[20]).animate({'left': '-66%'}, 300);
		});
		
		// Define a function to hide the button.
		var hideButton = function () {
			$('.rightContainer>div:nth-child(1)>div:nth-child(1)>' +
					'div:nth-child(12)').animate({'width': '100%'}, 300);
			$(gui.button[21]).animate({'margin-left': '-33%'}, 300);
			$(gui.button[21]).width('66%');
			$(gui.button[21]).css('margin-left', '-33%');
			$(gui.button[20]).width('66%');
			$(gui.button[20]).css('margin-left', '-33%');
			$('#logoutButtonImage').animate({'left': '0'}, 300);
			$(gui.hoveredButton[20]).animate({'left': '0'}, 300);
			$(gui.activeButton[20]).animate({'left': '0'}, 300);
		} 
		// Hide button when leaving both areas.
		$('.rightContainer>div:nth-child(1)>div:nth-child(1)>' +
			'div:nth-child(12)').mouseleave(function() {
				hideButton();
		});
		// Hide when clicking the "Log out" button.
		$(gui.button[20]).click(function () {
			hideButton();
		});
	};
	
	slideButton();
	// Turn all main menu buttons off when logging out.
	$(gui.button[20]).click(function () {
		var mainMenuButtons = [0, 1, 2, 3, 4, 21];
		for (var i = 0; i < mainMenuButtons.length; i++) {
			$(gui.activeButton[mainMenuButtons[i]]).css('visibility', 'hidden');
			gui.isButtonActive[mainMenuButtons[i]] = false;
		}
	});
	
	$(gui.button[20]).click(function () {
		gui.logout('sayingGoodbye');
	});
	
	$(window).unload(function () {
		gui.logout();
		return 'Bye!';
	});
};

var setViewer = function () {
	
	// CREATE THE SCENE
	trid.viewerScene = new THREE.Scene();
	
	// CREATE A CAMERA: zoom it out from the model a bit.
	trid.viewerCamera = new THREE.PerspectiveCamera(45,
		$('#viewerCanvasContainer').width() /
		$('#viewerCanvasContainer').height(), 1, 1000);
	
	// Move the camera away from origin only, not its target.
	trid.viewerCamera.position.x = 0;
	trid.viewerCamera.position.y = 3.5;
	trid.viewerCamera.position.z = 12;
	
	// PLACE MODELS INTO THE SCENE: characters, weapons and most effects.
	for (var i = 45; i < 56; i++) {
		// APPLY MATERIALS TO MODELS
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 14]});
		trid.models[i].material.alphaTest = 0.4;
		trid.models[i].material.transparent = true;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.skinning = true;
		trid.models[i].visible = false;
		
		// ENABLE MODEL ANIMATIONS: characters and weapons.
		// The action that will appear is set in the gui section of the code,
		// just before running "gui.switchAction" function.
		// Enable animation only if it exist in the json file.
		if (trid.models[i].geometry.animation[gui.actionIndex] !== undefined) {
			trid.enableAnimation(i, gui.actionIndex);
		}
		
		// ENABLE MODEL ANIMATIONS: effects.
		if (i > 52 && i < 59) {
			trid.enableAnimation(i, 0);
		}
		
		// ADD MODELS TO THE SCENE: characters and weapons.
		trid.viewerScene.add(trid.models[i]);
	}
	
	// PLACE MODELS INTO THE SCENE: "energyBlast.js".
	trid.models[56].material = new THREE.MeshBasicMaterial({map:
		trid.textures[42]});
	trid.models[56].material.alphaTest = 0.4;
	trid.models[56].material.transparent = true;
	trid.models[56].material.side = THREE.DoubleSide;
	trid.models[56].visible = false; 

	trid.viewerScene.add(trid.models[56]);
	
	// PLACE MODELS INTO THE SCENE: "energyBolt.js" and "energyBoltGlow".
	for (var i = 57; i < 59; i++) {
		trid.models[i].material = new THREE.MeshBasicMaterial({map:
			trid.textures[i - 14]});
		trid.models[i].material.alphaTest = 0.4;
		trid.models[i].material.transparent = true;
		trid.models[i].material.side = THREE.DoubleSide;
		trid.models[i].material.skinning = true;
		trid.models[i].visible = false;
	
		trid.enableAnimation(i, 0);
		trid.viewerScene.add(trid.models[i]);
	}
	
	// APPLY MATERIALS TO MODEL: "energyShield.js".
	trid.models[59].material = new THREE.MeshBasicMaterial({map:
			trid.textures[45]});
		trid.models[59].material.alphaTest = 0.4;
		trid.models[59].material.transparent = true;
		trid.models[59].material.side = THREE.DoubleSide;
		trid.models[59].visible = false;
	// PLACE MODELS INTO THE SCENE: "energyShield.js".
	trid.viewerScene.add(trid.models[59]);
	
	// APPLY MATERIALS TO MODEL: "iceBolt.js".
	trid.models[60].material = new THREE.MeshBasicMaterial({map:
		trid.textures[46]});
	trid.models[60].material.alphaTest = 0.4;
	trid.models[60].material.transparent = true;
	trid.models[60].material.side = THREE.DoubleSide;
	trid.models[60].visible = false;	
	
	trid.viewerScene.add(trid.models[60]);
	
	// PLACE MODELS INTO POSITION: Ice Bolt.
	trid.models[60].position.set(-1.075, 4.13913, 9.0153);
	trid.models[60].rotation.x = 90 * (Math.PI / 180);
	
	// SHOW DEFAULT CHARACTER: Show default character when character button is
	// clicked.
	$(gui.button[2]).click(function () {
		for (var i = 45; i < trid.models.length; i++) {
			trid.models[i].visible = false;
		}
		trid.models[45].visible = true;
		trid.models[46].visible = true;
	}); 
	
	// SWITCH CHARACTERS: Switch between them when their buttons are clicked.
	var switchSelectedCharacter = function (buttonIndex, characterIndex,
		weaponIndex) {
			$(gui.button[buttonIndex]).click(function () {
				for (var i = 45; i < trid.models.length; i++) {
					trid.models[i].visible = false;
				}
				trid.models[characterIndex].visible = true;
				trid.models[weaponIndex].visible = true;
			});
	};
	
	switchSelectedCharacter(7, 45, 46);
	switchSelectedCharacter(8, 47, 48);
	switchSelectedCharacter(9, 49, 50);
	switchSelectedCharacter(10, 51, 52);
	
	// HIDE CHARACTERS: hide characters and weapons when cancel, TOS or PP
	// buttons are clicked.
	$(gui.button[6]).click(function () {
		setTimeout(function () {
			for (var i = 45; i < trid.models.length; i++) {
				trid.models[i].visible = false;
			}
		}, 750);
	});
	$(gui.button[30]).click(function () {
		setTimeout(function () {
			for (var i = 45; i < trid.models.length; i++) {
				trid.models[i].visible = false;
			}
		}, 750);
	});
	$(gui.button[31]).click(function () {
		setTimeout(function () {
			for (var i = 45; i < trid.models.length; i++) {
				trid.models[i].visible = false;
			}
		}, 750);
	});
	
	// SWITCH ACTIONS: when clicking "previous action" button.
	$(gui.button[12]).click(function () {
		for (var i = 45; i < trid.animations.length; i++) {
			if (trid.animations[i] !== null) {
				for (var k = 0; k < trid.animations[i].length; k++) {
					trid.animations[i][k].stop();
					if (trid.animations[i][gui.actionIndex] !== undefined) {
						trid.animations[i][gui.actionIndex].play();
					}
				}
			}
		}
	});
	
	// SWITCH ACTIONS: when clicking "next action" button.
	$(gui.button[13]).click(function () {
		for (var i = 45; i < trid.animations.length; i++) {
			if (trid.animations[i] !== null) {
				for (var k = 0; k < trid.animations[i].length; k++) {
					trid.animations[i][k].stop();
					if (trid.animations[i][gui.actionIndex] !== undefined) {
						trid.animations[i][gui.actionIndex].play();
					}
				}
			}
		}
	});
	
	//	RESTART EFFECTS ANIMTIONS: restart effects animations when clicking
	// "previous action" button.
	$(gui.button[12]).click(function () {
		for (var i = 53; i < trid.models.length; i++) {
			if (trid.animations[i] !== null) {
				trid.animations[i][0].stop();
				trid.animations[i][0].play();
			}
		}
	});

	//	RESTART EFFECTS ANIMTIONS: restart effects animations when clicking
	// "next action" button.
	$(gui.button[13]).click(function () {
		for (var i = 53; i < trid.models.length; i++) {
			if (trid.animations[i] !== null) {
				trid.animations[i][0].stop();
				trid.animations[i][0].play();
			}
		}
	});
	
	// RESET ACTION: when clicking "cancel" button.
	$(gui.button[6]).click(function () {
		setTimeout (function () {
			for (var i = 45; i < trid.animations.length; i++) {
				if (trid.animations[i] !== null) {
					for (var k = 0; k < trid.animations[i].length; k++) {
						trid.animations[i][k].stop();
						if (trid.animations[i][gui.actionIndex] !== undefined) {
							trid.animations[i][gui.actionIndex].play();
						}
					}
				}
			}
		}, 750);
	});
	
	// SET RENDERER.
	trid.viewerRenderer = new THREE.WebGLRenderer({antialias:true, alpha:true});
	trid.viewerRenderer.setSize($('#viewerCanvasContainer').width(),
		$('#viewerCanvasContainer').height());
	
	// Append rendered to document body.
	var viewerCanvasContainer =
		document.getElementById('viewerCanvasContainer');
	viewerCanvasContainer.appendChild(trid.viewerRenderer.domElement);
	
	// SET ORBIT CONTROLS.
	trid.viewerControls = new THREE.OrbitControls(trid.viewerCamera,
		trid.viewerRenderer.domElement);
	trid.viewerControls.noKeys = true;
	trid.viewerControls.target.set(0, 3.5, 0);
	trid.viewerScene.add(trid.viewerCamera);
	
	// RESET VIEWER CAMERA: Reset camera settings when cancel button is clicked.
	$(gui.button[6]).click(function () {
		setTimeout(function() {
			trid.viewerCamera.position.set(0, 3.5, 12);
			trid.viewerControls.target.set(0, 3.5, 0);
		}, 750);
	});
	
	// Flag the viewer as ready to be animated.
	trid.isViewerReady = true;
};

var animateViewer = function () {
	// Use the same delta as background canvas.
	var delta;
	delta = trid.viewerClock.getDelta();
	
	// Render the viewerScene.
	trid.viewerRenderer.render(trid.viewerScene, trid.viewerCamera);
	
	// Update animations only for the action currently choosen and as long as
	// character board is visible. The idea is to optimize by avoiding running
	// needles animations that won't be seen after all.
	if (gui.isBoardVisible[2] == true) {
		for (var i = 45; i < 53; i++) {
			if (trid.animations[i] !== null) {
				if (trid.animations[i][gui.actionIndex] !== undefined) {
					trid.animations[i][gui.actionIndex].update(delta);
				}
			}
		}
		
		// ANIMATE FX MODELS: charge coil, energy charge, energy beam, energy
		// bolt and energy bolt glow. Note that both energy blast and energy
		// shield do not have model animations.
		switch (gui.actionIndex) {
		case 0:
			trid.animations[53][0].update(delta); // charge coil.
			break;
		case 1:
			trid.animations[54][0].update(delta); // energy charge.
			trid.animations[55][0].update(delta); // energy beam.
			break;
		case 2:
			trid.animations[57][0].update(delta); // energy bolt.
			trid.animations[58][0].update(delta); // energy bolt glow.
			break;
		}
		
		// ANIMATE FX UVS: charge coil.
		trid.animateFxUvs(53, 45, 0, function () {
			trid.animateUvs(53, 4, 2, trid.animations[45][0].currentTime,
				0.041666667, 0.541666667, 0.875);
		});
		// ANIMATE FX UVS: energy charge.
		trid.animateFxUvs(54, 45, 1, function () {
			trid.animateUvs(54, 8, 8, trid.animations[45][1].currentTime,
				0.041666667, 0.583333333, 3.291666667);
		});
		// ANIMATE FX UVS: energy beam.
		trid.animateFxUvs(55, 45, 1, function () {
			trid.animateUvs(55, 8, 2, trid.animations[45][1].currentTime,
				0.041666667, 2.791666667, 3.5);
		});
		// ANIMATE FX UVS: energy blast.
		trid.animateFxUvs(56, 45, 1, function () {
			trid.animateUvs(56, 4, 4, trid.animations[45][1].currentTime,
				0.041666667, 2.958333333, 3.666666667);
		});
		// ANIMATE FX UVS: energy bolt.
		trid.animateFxUvs(57, 45, 2, function () {
			trid.animateUvs(57, 10, 1, trid.animations[45][2].currentTime,
				0.041666667, 0.583333333, 1.791666667);
		});
		// ANIMATE FX UVS: energy bolt glow.
		trid.animateFxUvs(58, 45, 2, function () {
			trid.animateUvs(58, 1, 2, trid.animations[45][2].currentTime,
				0.041666667, 0.583333333, 1.791666667);
		});
		// ANIMATE FX UVS: energy shield.
		trid.animateFxUvs(59, 45, 3, function () {
			trid.animateUvs(59, 4, 8, trid.animations[45][3].currentTime,
				0.041666667, 0.375, 1.75);
		});
		
		// Animate ice bolt.
		if (trid.models[45].visible === true) {
			if (gui.actionIndex === 0 && (trid.animations[45][0].currentTime * 24)
				>= 17) {
				trid.models[60].visible = true;
				trid.models[60].position.z += 6;
			} else {
				trid.models[60].visible = false;
				trid.models[60].position.set(-1.075, 4.13913, 9.0153);
			}
		}
		
		// Update uvs.
		for (var i = 53; i < trid.models.length; i++) {
			trid.models[i].geometry.uvsNeedUpdate = true;
		}
	}
	
	
	
	trid.viewerControls.update();
};

// Log user out before leaving the page.
$(window).on('beforeunload', function () {
	gui.logout();
}); 
