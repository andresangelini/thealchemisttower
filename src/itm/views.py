# Copyright (C) 2016 Andres Angelini
#
# This file is part of The Alchemist's Tower.
#
# The Achemist's Tower is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The Alchemist's Tower is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Website.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from itm.models import Item, Library
import os
import re

# Create your views here.
def download_item(request):
    # The file to be sent is identified by the url.
    def send_file():
        abs_path = os.path.join(\
            settings.BASE_DIR,\
            re.split('/', request.path[1:])[0],\
            re.split('/', request.path[1:])[1])
        item = open(abs_path, 'r')
        response = HttpResponse(item, content_type='application/x-compressed-tar')
        response['Content-Disposition'] = \
            'attachment; filename=' + '"' + request.path[5:] + '"'
        return response
    
    # If the item is free, its file can be downloaded by clicking "Download Now"
    # or by entering its url. Otherwise, the user must be logged in and must
    # have the item in his library in order to be able to download its file.
    try:
        Item.objects.get(file_url=request.path[1:])
        price = Item.objects.get(file_url=request.path[1:]).price
        if price == 0:
            return send_file()
        else:
            if request.user.is_authenticated() == True:
                item_name = Item.objects.get(file_url=request.path[1:]).name
                user_library = Library.objects.get(owner=request.user)
                try:
                    user_library.items.get(name=item_name)
                    print user_library.items.get(name=item_name)
                    return send_file()
                except ObjectDoesNotExist:
                    return render(request , 'unauthorized.html')
            else:
                return render(request , 'unauthorized.html')
    except ObjectDoesNotExist:
        return render(request , 'unauthorized.html')
