# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('itm', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='file_url',
            field=models.FilePathField(),
            preserve_default=True,
        ),
    ]
