# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('itm', '0002_auto_20150617_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='library',
            name='price',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
