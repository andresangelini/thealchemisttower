# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('itm', '0003_library_price'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='library',
            name='price',
        ),
    ]
