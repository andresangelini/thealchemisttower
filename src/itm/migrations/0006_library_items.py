# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('itm', '0005_remove_library_items'),
    ]

    operations = [
        migrations.AddField(
            model_name='library',
            name='items',
            field=models.ManyToManyField(to='itm.Item'),
        ),
    ]
