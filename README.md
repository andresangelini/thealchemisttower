# The Alchemist's Tower #

This is the source code for our website at www.thealchemisttower.com. We have made our website
free software because we truly believe in the Free Software Pholosophy. Besides, not only does this lets our visitors check for themselves the integrity of our code but hopefully it may also be of help to those learning about web development.

## Instructions: ##

Since instructions may vary depending on your operative system, you will have to read the official documantation for each of the tools needed.

### Cloning ###

1. Open your terminal and navigate to the folder you want to clone it into. In our case, this folder is called "Projects".

        $ cd Projects

2. The type:

        $ git clone git@gitlab.com:andresangelini/thealchemisttower.git

3. And you are all done!

### Installing dependencies ###

Now that we have the project cloned in our local machine, we need to install the tools required to start working.

###### Python ######

[Python][python] is the programming language the server-side code is written in, so it is essential we make sure it is installed in our system.

###### Python Pip ######

[pip][pip] is the recommended tool to install python packages. It already comes bundled with python in versions > 2.

###### Python Virtual Enviroment ######

[virtualenv][virtualenv] is a tool to create isolated Python environments. virtualenv creates a folder which contains all the necessary executables to use the packages that a Python project would need.

###### Django ######

[Django][django] is a framework written in python which facilitates writing server-side code.

###### Django-PayPal ######

[django-paypal][django-paypal] is a pluggable Django application for integrating PayPal Payments Standard or Payments Pro.

###### M2Crypto ######

[M2Crypto][m2crypto] is a Python crypto and SSL toolkit.

###### Gunicorn ######

[Gunicorn][gunicorn] is a python WSGI HTTP Server for UNIX. Basically, it is the connection between Django and Nginx.

###### Nginx ######

[Nginx][nginx] is a web server. It can act as a reverse proxy server for HTTP, HTTPS, SMTP, POP3, and IMAP protocols, as well as a load balancer and an HTTP cache.

### Configuring Django ###

Most of the Django configuration is ready for deployment but there are some settings that for obvious security reasons must be kept secret, and so you need to add them on your own (and also
keep them secret yourself!).

To configure Django, you could just open the file "settings.py" found inside the "tat" folder and then edit it. However, that would be extremely insecure if you then decide to share this file in a repository just like we do here. So, instead, to keep your secrets, well... secret, create a python file inside the "tat" folder, where "settings.py" is, with all the secret settings you don't want leaked out so that you can point to it later while excluding it from git. ALternately, you may also place these settings in your system enviroment as variables.



###### SECRET\_KEY ######

A secret key for a particular Django installation. This is used to provide cryptographic signing, and should be set to a unique, unpredictable value. Please note that running Django with a known SECRET_KEY defeats many of Django’s security protections, and can lead to privilege escalation and remote code execution vulnerabilities.

When you create a new project, Django automatically generates one for you by default, however, you now need to generate one by yourself.

###### ALLOWED\_HOSTS ######

A list of strings representing the host/domain names that this Django site can serve. This is a security measure to prevent an attacker from poisoning caches and triggering password reset emails with links to malicious hosts by submitting requests with a fake HTTP Host header, which is possible even under many seemingly-safe web server configurations.

Add the default hosts in the list.

###### PAYPAL\_RECEIVER\_EMAIL ######

Add the e-mail address you want to receive notification from PayPal.

###### PAYPAL\_PRIVATE\_CERT ######

Add the private certificate you used for PayPal.

###### PAYPAL\_PUBLIC\_CERT ######

Add the public certificate you used for PayPal.

###### PAYPAL\_CERT ######

Add the certificate you receive from PayPal.

###### PAYPAL\_CERT\_ID ######

Add your own PayPal certificate id.

### Development Vs Production settings ###

Django "settings.py" file has different configurations depending on whether you are developing or deploying the site into production. Each block of code is clearly identified. Just comment and uncomment the different blocks accordingly. Also remember to set DEBUG and TEMPLATE\_DEBUG variables properly.

### Configuring Gunicorn ###

Having Django properly configured lets you run and test the site as long as you are developing it. However, once that's done and you want to deploy it, you will need a real web server, and Django it's simply not. It's has been pretending to be one up until now by serving your static files just as a convenience. However, once you set DEBUG and TEMPLATE\_DEBUG both to False (as you should when going live!), Django will refuse to serve your files. So, what you need to do is setting up Nginx. But there is the problem; Nginx can't talk with Django, it needs something to let them both communicate with each other, and that's exactly where Gunicorn comes into play.

What we are going to is to create a simple bash script that runs gunicorn with our own defined settings at system startup.

1. Create a bash file called "gunicorn\_start.sh" inside your "Project" folder (or however you named the folder which contains "thealchemisttower" you cloned).

        $ touch Projects/gunicorn_start.sh

2. Open it with your preferred text editor and copy and paste the following:

        #!/bin/bash

        NAME="tat" # Name of the application.
        DJANGODIR=/home/user/Projects/thealchemisttower/src # Django project directory.
        SOCKFILE=/home/user/Projects/thealchemisttower/run/gunicorn.sock # Our link.
        USER=nginx     # The user to run as.
        GROUP=webdata  # The group to run as.
        NUM_WORKERS=1  # How many worker processes should Gunicorn spawn.
        DJANGO_SETTINGS_MODULE=tat.settings # Which settings file should Django use.
        DJANGO_WSGI_MODULE=tat.wsgi # WSGI module name.
        
        echo "Starting $NAME as `whoami`"
        
        # Activate the virtual environment
        cd $DJANGODIR
        source /home/user/Projects/thealchemisttower/bin/activate
        export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
        export PYTHONPATH=$DJANGODIR:$PYTHONPATH
        
        # Create the run directory if it doesn't exist
        RUNDIR=$(dirname $SOCKFILE)
        test -d $RUNDIR || mkdir -p $RUNDIR
        
        # Start your Django Unicorn
        # Programs meant to be run under supervisor should not daemonize
        # themselves (do not use --daemon).
        exec /home/user/Projects/thealchemisttower/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
          --name $NAME \
          --workers $NUM_WORKERS \
        #  --user $USER \
          --bind=unix:$SOCKFILE

3. Although we are still missing Nginx, you can already test if it's working.

        $ bash Projects/gunicorn_start.sh

   You shouls see something like this:

        [2016-02-25 20:24:58 +0000] [18135] [INFO] Starting gunicorn 19.3.0
        [2016-02-25 20:24:58 +0000] [18135] [INFO] Listening at: http://127.0.0.1:8000 (18135)
        [2016-02-25 20:24:58 +0000] [18135] [INFO] Using worker: sync
        [2016-02-25 20:24:58 +0000] [18146] [INFO] Booting worker with pid: 18146

   To stop it while it's still running in the terminal just press CTRL+C. If it's running in the background (because you closed the terminal), you will first have find its PID (Process ID) and to then kill it.

   1. Report a snapshot of the current processes using BSD syntax and grep to print only
      the gunicorn process in just one command.

           $ ps ax | grep gunicorn

      You should see something like this:

           17971 ?        Sl     0:03 gedit /home/user/Projects/thealchemisttower/gunicorn_start.sh
           18456 pts/2    S+     0:00 /home/user/Projects/thealchemisttower/bin/python /home/user/Projects/thealchemisttower/bin/gunicorn tat.wsgi:application --name tat --workers 1
           18467 pts/2    S+     0:00 /home/user/Projects/thealchemisttower/bin/python /home/user/Projects/thealchemisttower/bin/gunicorn tat.wsgi:application --name tat --workers 1
           18471 pts/6    R+     0:00 grep --color=auto gunicorn

   2. Its PID is the first numbers at the top right. Ivoke the kill command to kill it.
           $ kill 17971

### Configuring Nginx ###

Once Nginx has been properly installed, there are mainly three files you will be concerned about:

###### nginx.conf ######

This is the main file where you will have to tell Nginx which user has the right to access the project folder as well as where it should look for the "gunicorn.sock" file to communicate with gunicorn.

1. Open "nginx.conf" with using your favorite text editor using sudo if you didn't assigned privilage rights to "nginx" folder before.

        $ sudo nano ../../etc/nginx/nginx.conf

2. At the very top you should the user setting. Change it to be the same user as the one using gunicorn.

        user www-data;              # Replace with the proper user.
        
        worker_processes 4;
        pid /run/nginx.pid;
        
        events {
            worker_connections 768;
            # multi_accept on;
        }

###### sites-available ######

The next file you need to take care of is the one living inside the "sites-available" folder.

1. Delete the file named "default" inside the "sites-available" folder. You may want to back it up somewhere else before doing so.

        $ sudo rm ../../etc/nginx/sites-available/default

2. Create a new file and name however you like. It would be a good idea to call it the same as your project.

        $ sudo touch ../../etc/nginx/sites-available/myproject

3. Then open it with your preferred text editor.

       $ sudo nano ../../etc/nginx/sites-available/myproject

4. Here you will have to define your server settings. To do so, create a server block like this:

        server {
            # Here you will add your settings.
        }

5. Start by defining the port to listen to, which by default is 80, and then the name of your server.

        Server {
            listen 80;                # Make sure you to use the right port.
            server_name localhost;
            
            client_max_body_size 4G;  # Add also this which is the default.
        }

6. Now you need to point to your Django templates folder and tell Nginx which file to look for. You can indicate many possible file names.

        Server {
            listen 80;                # Make sure you to use the right port.
            server_name localhost;
            
            client_max_body_size 4G;  # Add also this which is the default.
            
            # Django project folder. In our example, our project is inside a folder
            # called "Projects" in our home.
            root home/user/Projects/thealchemisttower/src/tpl;
            index index.html index.htm;
        }

7. Then, you also have indicate where is you static folder.

        Server {
            listen 80;                # Make sure you to use the right port.
            server_name localhost;
            
            client_max_body_size 4G;  # Add also this which is the default.
            
            # Django project folder. In our example, our project is inside a folder
            # called "Projects" in our home.
            root home/user/Projects/thealchemisttower/src/tpl;
            index index.html index.htm;
            
            # Static files.
            location /static/ {
                autoindex on;
                # This is the path according to our example.
                alias   /home/user/Projects/thealchemisttower/src/static/;
            }
        }

8. Last, but not least, you will tell Nginx to forward "localhost" to the actual address.

        Server {
            listen 80;                # Make sure you to use the right port.
            server_name localhost;
            
            client_max_body_size 4G;  # Add also this which is the default.
            
            # Django project folder. In our example, our project is inside a folder
            # called "Projects" in our home.
            root home/user/Projects/thealchemisttower/src/tpl;
            index index.html index.htm;
            
            # Static files.
            location /static/ {
                autoindex on;
                # This is the path according to our example.
                alias   /home/user/Projects/thealchemisttower/src/static/;
            }
            
            # Forward "localhost" to "http://127.0.0.1:8000".
            location / {
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_pass http://127.0.0.1:8000;
            }
        }

9. Save and close the file.

###### sites-enabled ######

1. Now that this "site" is configutated and "available", you need to "enable" it by going to the "sites-enabled" folder and creating a symbolic link to the file we just created to actually enable this configuration.

        $ sudo ln ../../etc/nginx/sites-available/myproject ../../etc/nginx/sites-enabled/myproject

2. With this, Nginx is ready to go with a basic configuration. It's highly recommendable though, that you go through Nginx [documentation][nginx] to learn how to properly configure it to make it more secure.

### Running Nginx ###

With everything already set up the only left thing to do is to start up Nginx and Gunicorn.

1. Execute our Gunicorn bash file we created earlier (if it's not already running).

        $ bash Projects/thealchemisttower/gunicorn_start.sh

2. Start up Nginx (with sudo privilages).

        $ sudo service nginx start

   You can stop it anytime with:

        $ sudo service nginx stop


[python]: https://www.python.org/ "Python official website"
[pip]: https://pypi.python.org/pypi/pip "Pip official documentation"
[virtualenv]: https://pypi.python.org/pypi/virtualenv "virtualenv official website"
[django]: https://www.djangoproject.com/ "Django official website"
[django-paypal]: https://pypi.python.org/pypi/django-paypal "Django-PayPal official website"
[m2crypto]: https://pypi.python.org/pypi/M2Crypto/ "M2Crypto official website"
[gunicorn]: http://gunicorn.org/ "Gunicorn official website"
[nginx]: https://www.nginx.com/ "Nginx official website"